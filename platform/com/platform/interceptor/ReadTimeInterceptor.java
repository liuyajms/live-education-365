package com.platform.interceptor;

import com.application.feedback.Feedback;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;

import java.util.Date;

/**
 * 评论阅读拦截器
 */
public class ReadTimeInterceptor implements Interceptor {

    private static final Log log = Log.getLog(ReadTimeInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		inv.invoke();
		String uri = inv.getActionKey();
		String sql = "update {T} set read_time = ? where read_time is null";
		if(uri.contains("comment")){
		} else if(uri.contains("feedback")){
			Db.update(sql.replace("{T}", Feedback.table_name), new Date());
		}
	}



}

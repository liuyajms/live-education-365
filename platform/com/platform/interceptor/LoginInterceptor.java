package com.platform.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.log.Log;
import com.platform.config.EscapeXSSHttpServletRequestWrapper;
import com.platform.constant.ConstantAuth;
import com.platform.constant.ConstantWebContext;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.user.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登陆拦截器
 */
public class LoginInterceptor implements Interceptor {

    private static final Log log = Log.getLog(LoginInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
        BaseController contro = (BaseController) inv.getController();
        HttpServletRequest request = contro.getRequest();
        HttpServletResponse response = contro.getResponse();
		User user = AuthInterceptor.getCurrentUser(request, response, true);

        if(user == null){
            if(log.isDebugEnabled()) log.debug("未登录校验，拒绝访问！");
            AuthInterceptor.toView(contro, ConstantAuth.auth_no_login, "");
            return;
        }
		inv.invoke();
	}



}

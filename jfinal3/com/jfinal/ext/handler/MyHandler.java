package com.jfinal.ext.handler;

import com.jfinal.handler.Handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyHandler extends Handler {
    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Headers",
                "authmark, Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
        response.addHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
        response.addHeader("X-Powered-By","Jetty");
        next.handle(target, request, response, isHandled);
        System.out.println("==============");
    }
}

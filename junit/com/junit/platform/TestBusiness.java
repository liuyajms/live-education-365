package com.junit.platform;

import com.alibaba.fastjson.JSON;
import com.application.job.TeacherIncomeReportJob;
import com.application.model.Order;
import com.application.module.livecourse.LiveCourse;
import com.application.module.livecourse.LiveCourseService;
import com.application.pay.AbstractPayNotify;
import com.application.pay.WxPayNotify;
import com.junit.TestBase;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Map;

public class TestBusiness extends TestBase {
    @Test
    public void test() {
        String str = "{\"transaction_id\":\"4200000995202102174805630236\",\"nonce_str\":\"545f5bc52fa24e5e865ec384ffc05871\",\"bank_type\":\"CCB_CREDIT\",\"openid\":\"o_CqH6OphBX5D46MvHrSZ3LBYDF8\",\"sign\":\"DC177562FCEDF019746DB62CF4DED660\",\"fee_type\":\"CNY\",\"mch_id\":\"1605917606\",\"cash_fee\":\"10\",\"out_trade_no\":\"p202102212124462513\",\"appid\":\"wx579632282b9ab612\",\"total_fee\":\"10\",\"trade_type\":\"JSAPI\",\"result_code\":\"SUCCESS\",\"attach\":\"充值高级会员\",\"time_end\":\"20210217235251\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}";
        Map<String,String> map = JSON.parseObject(str,Map.class);
//        map.put("out_trade_no", "c202102221818593812");
//        map.put("out_trade_no", "v202102221859143231");
        map.put("out_trade_no", "p202102212124462513");
        map.put("time_end", "20210223235251");
        map.put("total_fee", "10");
        String fee = map.get("total_fee");
        AbstractPayNotify notify = new WxPayNotify(map.get("time_end"), WxPayNotify.getFormatMoney(fee),
                Order.PAY_WX, map.get("appid"), map.get("out_trade_no"));
        notify.run();
    }

    @Test
    public void testRebate() {
        LiveCourse course = new LiveCourse();
        course.setStream_name("course1");
        course.setLive_starttime(new Timestamp(System.currentTimeMillis()));
        new LiveCourseService().createOrUpdate(course);
    }

    @Test
    public void testReport() {
        new TeacherIncomeReportJob().start();

    }
}

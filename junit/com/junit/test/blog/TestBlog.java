package com.junit.test.blog;

import com.alibaba.fastjson.JSONObject;
import com.application.common.utils.wx.WxUtils;
import com.application.model.Category;
import com.application.module.withdraw.WithdrawService;
import com.jfinal.ext.kit.excel.PoiImporter;
import com.jfinal.ext.kit.excel.Rule;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.junit.TestBase;
import com.platform.mvc.base.BaseService;
import com.platform.mvc.operator.Operator;
import com.platform.plugin.ServicePlugin;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 博客单元测试类
 * @author 董华健  dongcb678@163.com
 */
public class TestBlog extends TestBase {
	String session_key;
	String iv;
	String encryptedData;
	String openid;

	@Test
    public void testt() {
	    WithdrawService.transferMoney("xxxxx", new BigDecimal(100), "xdfdfdfdfdf");
    }

	@Test
	public void testWx() {
		encryptedData = "rUVMCKD0gz8D/zUGPWUxTck+n+kYt1l9bOWJ3KvgLvAjKfBXS62jo/R0l20cIw4HnywAYJM8P9+fSv3jyWT" +
				"+qjqYJz6iKQJuQQnK63q6eUup5Y47zoRZVL2lcfEevaCoHzpNpkEFlqpYMf7K0Zn7U+5Pb2WP5a103p1LEzV2qpV2nUfJ9U0aJlKtaUKh7rd9tKrcICOQtxfNjr8MhMH+4w==";
		session_key = "0JrsZ3NGPjD\\/G74DAnV6ZA==";
		iv = "cPMSF63ac86iJzTgWS17cg==";
		openid = "oHH-25OKOCktqvdyd_RCTblQc-pE";

//		2
		session_key = "T2aO2jl4Hu4OhSUJSbxfVw==";
		encryptedData = "UCO6KvnBmDApgb6/WeyKykcnZRk9GI0DGVMzyZbSgkk2Ei9wBbX48/O+H81hC/0a/Xrg8qHIyMraNf5hv4BDPVE+OShIOd7hxWyleq0bnLnKtQdm53RAanpaPAcAtPfjK7EEZ+ZFynoFjIaqUQOua7kvYck/1xhz6a4YO0qrD6mzDFjjvKkR1+/AcLfWiHOzr6ZENqR2tliq01LWntdnzA==";
		iv = "4XEQEksyyBLbinmjmWEQpA==";

		test();
	}

	private void test() {
		Map<String, Object> map = new HashMap<>();

		String result = WxUtils.wxDecrypt(encryptedData, session_key, iv);
		System.out.println(result);
		JSONObject json = JSONObject.parseObject(result);
		if (json.containsKey("phoneNumber")) {
			String phone = json.getString("phoneNumber");
			if (StringUtils.isNoneBlank(phone)) {
				map.put("code", 0);
				map.put("msg", "成功");
				map.put("data", phone);
			} else {
				map.put("code", 4001);
				map.put("msg", "失败！用户未绑定手机号");
				map.put("data", "");
			}
		} else {
			map.put("code", 4001);
			map.put("msg", "获取失败！");
			map.put("data", "");
		}
	}
	
	@Test
    public void delete() throws SQLException{
		BaseService bService = (BaseService) ServicePlugin.getService(BaseService.baseServiceName);
		bService.baseDelete("test_blog", "ids001,ids002");
    }

    /*
    insert into t_product_sku(sku_name,product_id,sku_image,sku_price,sku_stock,sku_weight
,sku_ord) select product_name,id,cover_image,price,stock,weight, 1 from t_product
where create_time > '2019-06-21'
     */
    @Test
	public void importImage() throws IOException {
		File dir = new File("D:\\Mine\\宜生鲜\\第二批水果\\detail");
		for (File file : dir.listFiles()) {
			String name = file.getName();
			String pNo = name.split("_")[0];
			String type = name.split("_")[1];

		}
	}

	@Test
	public void importCatImage() throws IOException {
		File dir = new File("D:\\Mine\\宜生鲜\\归档数据\\分类");
		for (File file : dir.listFiles()) {
			String name = file.getName();

			Category c = Category.dao.findByNumber(name.split("\\.")[0]);

			String path = "/files/upload/category/" + name;
			c.setCategory_icon(path);
			c.update();

			FileUtils.copyFile(file,new File(PathKit.getWebRootPath() + path));
		}
	}

	@Test
	public void category() {
		File file = new File("D:\\Mine\\宜生鲜\\水果产品导入模板.xlsx");

		List<List<String>> rowList = PoiImporter.readSheet(file, new Rule());
		List<String> cIdList = new ArrayList<>();
		List<String> pIdList = new ArrayList<>();
		for (int i = 1; i < rowList.size(); i++) {
			List<String> r = rowList.get(i);
			if(StrKit.isBlank(r.get(0))){//内容已结束,退出
				continue;
			}

			cIdList.add(r.get(0).trim());
			pIdList.add(r.get(1).trim());

		}




	}


	@Test
	public void insertOp(){
		Map<String, String> param = new HashMap<>();
		param.put("enctype", "0");
		param.put("syslog", "1");
		param.put("method", "2");
		param.put("referer", "0");
		param.put("csrf", "0");
		param.put("pvtype", "0");
		param.put("ispv", "0");
//        param.put("privilegess", "0");
		param.put("ipblack", "0");
		param.put("formtoken", "0");
//        param.put("splitpage", "0");
		param.put("rowfilter", "0");
		param.put("onemany", "0");


		param.put("moduleids", "b991b11b6b2f4c08a6b8ed977d91b4b8");
		param.put("splitpage", "0");
		param.put("privilegess", "1");


		String[] ss = {"保存", "修改", "删除", "详情"};
		String[] st = {"/save", "/update", "/delete", "/view"};

		for (int i=0; i<ss.length; i++) {
			param.put("names", "后台商品规格" + ss[i]);
			param.put("url", "/admin/productSku" + st[i]);

			Operator operator = Operator.dao.getModelByMap(null, param);
			operator.save();
		}

	}

	@Test
	public void testArea() throws IOException {
		/*String path = "D:\\Mine\\宜生鲜\\area.json";
		String s = FileKit.readFile(path);

		List<Tarea> list = JSON.parseArray(s, Tarea.class);

		List<Area> areaList = new ArrayList<>();
		for (Tarea a : list) {
			Area province = new Area();
			province.set("id", a.getId()).set("name", a.getAreaName()).set("ord", a.getId())
					.set("parent_id", 0)
					.set("level", 1)
					.set("path", 0);
			areaList.add(province);
			if (a.getChildren().size() > 0) {
				for (Tarea b : a.getChildren()) {
					Area city = new Area();
					city.set("id", b.getId()).set("name", b.getAreaName()).set("ord", b.getId())
							.set("parent_id", a.getId())
							.set("level", 2)
							.set("path", 0 + "/" + a.getId());
					areaList.add(city);
					if (b.getChildren().size() > 0) {
						for (Tarea c : b.getChildren()) {
							Area xian = new Area();
							xian.set("id", c.getId()).set("name", c.getAreaName()).set("ord", c.getId())
									.set("parent_id", b.getId())
									.set("level", 3)
									.set("path", city.get("path") + "/" + city.getId());
							areaList.add(xian);
						}
					}
				}
			}
		}

		System.out.println(areaList.size());
		System.out.println(JSON.toJSONString(areaList));

		Db.batchSave(areaList, 100);*/
	}


	@Test
	public void testUpdateMobile() {
		String s = "SELECT a.*,b.mobile from t_user_money a left join pt_user b on a.create_userid = b.id where a" +
				".type = 'rebate' order by a.id desc";
		List<Record> list = Db.find(s);
		for (Record record : list) {
			String content = record.get("content");
			String mobile = StrKit.getFormatMobile(record.get("mobile"));
			String str = content.replaceAll("(【1.{10}】)", "【"+ mobile +"】");
			System.out.println(content + " ==> " + str);
			Db.update("update t_user_money set content = ? where id = ?", str, record.getInt("id"));
		}
	}
}

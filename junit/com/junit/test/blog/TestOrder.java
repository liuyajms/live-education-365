package com.junit.test.blog;

import com.alibaba.fastjson.JSON;
import com.application.module.report.ReportService;
import com.jfinal.plugin.activerecord.Record;
import com.junit.TestBase;
import com.platform.constant.ConstantInit;
import com.platform.dto.SplitPage;
import com.platform.mvc.base.BaseService;
import com.platform.plugin.ServicePlugin;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

/**
 */
public class TestOrder extends TestBase {
	
	@Test
    public void test() throws SQLException{
		BaseService bService = (BaseService) ServicePlugin.getService(BaseService.baseServiceName);
		bService.baseDelete("test_blog", "ids001,ids002");

    }

    @Test
    public void test2() {
	    SplitPage s = new SplitPage();
	    String start = "2021-06-01 00:00";
	    String end = "2021-07-31 00:00";
        BaseService.paging(ConstantInit.db_dataSource_main, s, "app.report.teacherIncomeSelect", "app.report.teacherIncomeFrom");
        List<Record> res = new ReportService().getIncomeList((List<Record>) s.getList(), start, end);
        Record o = res.get(0);
        System.out.println(JSON.toJSONString(res));
    }

}

package com.junit.test.blog;

import com.alibaba.fastjson.JSON;
import com.jfinal.ext.kit.excel.PoiImporter;
import com.jfinal.ext.kit.excel.Rule;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.junit.TestBase;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TestImport extends TestBase {
    @Test
    public void testImportProject() {
        File file = new File("E:\\社保\\302-all.xlsx");

        List<List<String>> rowList = PoiImporter.readSheet(file, new Rule());
        List<Record> resultList = new ArrayList<>();
        String deptId = null;
        for (int i = 7; i < rowList.size(); i++) {
            Record o = new Record();
            List<String> r = rowList.get(i);
            if(StrKit.notBlank(r.get(0))){
                deptId = r.get(0);
            } else {
                if("上年结转艾滋病防治（中央成财社发[2019]49号）".equals(r.get(1))){
                    System.out.println("))))))))");
                }
                Integer p = r.get(7).equals("是") ? 1 : (r.get(7).equals("否") ? 0 : null);
                o.set("dept_id", deptId).set("project_name", r.get(1).trim())
                        .set("function_code", r.get(2).substring(1, 8).trim())
                        .set("function_name", r.get(2).substring(9).trim())
                        .set("project_type", r.get(3).trim())
                        .set("project_yiju", r.get(4).trim())
                        .set("project_content", r.get(5).trim())
                        .set("project_jixiao", r.get(6).trim())
                        .set("is_purchase", p)
                        .set("money_finance", StrKit.isBlank(r.get(10)) ? null : new BigDecimal(r.get(10).trim()
                                .replace(",",""))
                                .multiply(new BigDecimal("10000")).intValue())
                        .set("money_other", StrKit.isBlank(r.get(11)) ? null : new BigDecimal(r.get(11).trim()
                                .replace(",",""))
                                .multiply(new BigDecimal("10000")).intValue())
                        .set("money_funds", StrKit.isBlank(r.get(12)) ? null : new BigDecimal(r.get(12).trim()
                                .replace(",",""))
                                .multiply(new BigDecimal("10000")).intValue())
                        .set("last_finance", StrKit.isBlank(r.get(13)) ? null : new BigDecimal(r.get(13).trim()
                                .replace(",",""))
                                .multiply(new BigDecimal("10000")).intValue())
                        .set("last_funds", StrKit.isBlank(r.get(14)) ? null : new BigDecimal(r.get(14).trim()
                                .replace(",",""))
                                .multiply(new BigDecimal("10000")).intValue())
                        .set("memo", r.get(15));
                resultList.add(o);
                System.out.println(r.get(1));
            }
        }
        Db.tx(() -> {
            Db.batchSave("t_project", resultList, 100);
            return true;
        });
        System.out.println(JSON.toJSONString(resultList));

    }
}

package com.junit.test.blog;

import com.application.model.Order;
import com.application.module.withdraw.WithdrawService;
import com.application.service.UserService;
import com.application.service.orderbusi.VipChargeOrderNotify;
import com.junit.TestBase;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.SQLException;

/**
 * 博客单元测试类
 * @author 董华健  dongcb678@163.com
 */
public class TestUser extends TestBase {

	@Test
    public void testt() {
	    WithdrawService.transferMoney("xxxxx", new BigDecimal(100), "xdfdfdfdfdf");
    }

	@Test
    public void testAddUser() throws SQLException{

	    String url = "http://39.108.9.240/education/files/logo.jpg";
	    new UserService().registerUser("13551007022", "xxxxx", "wx", "tt", url);
    }

    @Test
    public void testOrder() {
        Order o = Order.dao.findById(140);
        new VipChargeOrderNotify().handler(o);
    }
}

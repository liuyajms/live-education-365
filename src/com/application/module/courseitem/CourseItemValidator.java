package com.application.module.courseitem;

import com.application.module.course.Course;
import com.application.validate.ApiValidator;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;

public class CourseItemValidator extends ApiValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CourseItemValidator.class);
	
	@SuppressWarnings("unused")
	private CourseItemService courseItemService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/api/courseItem")){
			valiList(controller);
		} else if (actionKey.equals("/app/courseItem/update")){
			
		}
	}

    private void valiList(Controller controller) {
        int cid = controller.getParaToInt("courseId");
        Course course = Course.dao.findById(cid);
        if (course == null) {
            addError("请选择课程");
        }
    }


}

package com.application.module.courseitem;

import com.application.module.recommend.Recommend;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
@Table(tableName = CourseItem.table_name, pkName = "id")
public class CourseItem extends BaseModel<CourseItem> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(CourseItem.class);
	
	public static final CourseItem dao = new CourseItem().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_course_item";
	
	/**
	 * sqlId : app.courseItem.splitPageFrom
	 * 描述：分页from
	 */
    public static final String sqlId_splitPageSelect = "app.courseItem.splitPageSelect";
	public static final String sqlId_splitPageFrom = "app.courseItem.splitPageFrom";

	private Integer id;
	private Integer course_id;
	private String item_name;
	private Integer item_ord;
	private Integer item_playcount;
	private String item_length;
	private Integer item_size;
	private String item_url;
	private Timestamp create_time;
	private Integer create_userid;
	private Timestamp delete_time;
	private Integer delete_userid;
	private Integer item_unlocks;
    private Integer item_unlocks_vip;
    private Integer item_teacher_id;//add at 21.6.6

    public Integer getItem_unlocks() {
        return getInt("item_unlocks");
    }

    public Integer getItem_unlocks_vip() {
        return getInt("item_unlocks_vip");
    }

    public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setCourse_id(Integer course_id){
		set("course_id", course_id);
	}
	public Integer getCourse_id() {
		return get("course_id");
	}
	public void setItem_name(String item_name){
		set("item_name", item_name);
	}
	public String getItem_name() {
		return get("item_name");
	}
	public void setItem_ord(Integer item_ord){
		set("item_ord", item_ord);
	}
	public Integer getItem_ord() {
		return get("item_ord");
	}
	public void setItem_playcount(Integer item_playcount){
		set("item_playcount", item_playcount);
	}
	public Integer getItem_playcount() {
		return get("item_playcount");
	}
	public void setItem_length(String item_length){
		set("item_length", item_length);
	}
	public String getItem_length() {
		return get("item_length");
	}
	public void setItem_size(Integer item_size){
		set("item_size", item_size);
	}
	public Integer getItem_size() {
		return get("item_size");
	}
	public void setItem_url(String item_url){
		set("item_url", item_url);
	}
	public String getItem_url() {
		return get("item_url");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}
	public void setDelete_time(Timestamp delete_time){
		set("delete_time", delete_time);
	}
	public Timestamp getDelete_time() {
		return get("delete_time");
	}
	public void setDelete_userid(Integer delete_userid){
		set("delete_userid", delete_userid);
	}
	public Integer getDelete_userid() {
		return get("delete_userid");
	}

    public void setItem_teacher_id(Integer teacher_id){
        set("item_teacher_id", teacher_id);
    }
    public Integer getItem_teacher_id() {
        return get("item_teacher_id");
    }

	/*
	查询课时是否已解锁，学习的时长多少
	 */
    public Page<CourseItem> findByCourseId(int pageNumber, int pageSize, Integer courseId, Integer userid) {
        Map<String, Object> map = new HashMap<>();
        map.put("courseId", courseId);
        map.put("userid", userid);
        String sel = getSqlByBeetl("app.courseItem.getApiListSelect", map);
        String from = getSqlByBeetl("app.courseItem.getApiList", map);
        return paginate(pageNumber, pageSize, sel, from);
    }

    public int findCount(int courseId) {
        String sql = "select count(*) from " + table_name + " where delete_time is null and course_id =?";
        return Db.queryLong(sql, courseId).intValue();
    }

    /**
     * 查询总的可用课时数
     * @return
     */
    public BigDecimal findCount() {
        String sql = "select count(*) from " + table_name + " where delete_time is null";
        return BigDecimal.valueOf(Db.queryLong(sql).intValue());
    }

    public void addPlay(int id) {
        Db.update("update "+ table_name + " set item_playcount = item_playcount+1 where id = ?", id);
    }

    /**
     * 查询课时列表，最多20条
     * @param catId
     * @return
     */
    public List<CourseItem> findByCatId(String catId) {
        String s = "SELECT b.* from t_course a join t_course_item b on b.course_id = a.id where a.delete_time is null and b.delete_time is null";
        if(StrKit.notBlank(catId)) {
            s += " and course_categoryid = ?";
        }
        s += "  order by course_ord, a.id desc, item_ord, b.id limit 20";
        if(StrKit.notBlank(catId)) {
            return find(s, catId);
        }
        return find(s);
    }

    /**
     * 删除课时表数据
     * 同时删除推荐数据
     * @param cUserId
     * @param courseId
     */
    public void deleteByCourseId(int cUserId, String courseId) {
//        Db.update("update "+ table_name + " set delete_time = now(), delete_userid = ? where course_id = ?", cUserId, courseId);
        List<CourseItem> items = find("select * from " + table_name + " where course_id = ?", courseId);
        for (CourseItem item : items) {
            item.setDelete_time(new Timestamp(System.currentTimeMillis()));
            item.setDelete_userid(cUserId);
            item.update();

            Recommend obj = Recommend.dao.findByModuleId(Recommend.TYPE_COURSE, item.getId());
            if(obj != null) {
                obj.delete();
            }
        }
    }
}

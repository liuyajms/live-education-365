package com.application.module.courseitem;

import com.application.module.couponused.CouponUsedService;
import com.application.module.course.Course;
import com.application.module.coursezan.CourseZan;
import com.application.module.study.Study;
import com.application.module.teacherinfo.Teacherinfo;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = CourseItemService.serviceName)
public class CourseItemService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CourseItemService.class);
	
	public static final String serviceName = "courseItemService";

    public Page<CourseItem> getListByCourseId(int pageNumber, int pageSize, int courseId, int userId) {
        Page<CourseItem> page = CourseItem.dao.findByCourseId(pageNumber, pageSize, courseId, userId);

        page.getList().forEach(o-> {
            int hasUnlock = new CouponUsedService().hasUnlock(userId, o.getId());
            o.put("has_unlock", hasUnlock);
        });
        return page;
    }

    public CourseItem getCourseItemDetail(Integer itemId, int userId) {
        CourseItem item = CourseItem.dao.findById(itemId);
        int hasUnlock = new CouponUsedService().hasUnlock(userId, itemId);

        Course course = Course.dao.findById(item.getCourse_id());
        Teacherinfo info = Teacherinfo.dao.findDetail(item.getItem_teacher_id());

        int zans = CourseZan.dao.findCourseZans(course.getId());
        Study study = Study.dao.findByCourseItemId(itemId, userId);

        item.put("has_unlock", hasUnlock).put("teacher", info)
                .put("zans", zans).put("course", course)
                .put("study", study);

        return item;
    }
}

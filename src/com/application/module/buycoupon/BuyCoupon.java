package com.application.module.buycoupon;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = BuyCoupon.table_name, pkName = "id")
public class BuyCoupon extends BaseModel<BuyCoupon> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(BuyCoupon.class);
	
	public static final BuyCoupon dao = new BuyCoupon().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_buy_coupon";
	
	/**
	 * sqlId : app.buyCoupon.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.buyCoupon.splitPageFrom";

	private Integer id;
	private Integer userid;
	private Integer coupon_type_id;
	private Integer coupon_num;
	private BigDecimal coupon_price;
	private Timestamp create_time;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setUserid(Integer userid){
		set("userid", userid);
	}
	public Integer getUserid() {
		return get("userid");
	}
	public void setCoupon_type_id(Integer coupon_type_id){
		set("coupon_type_id", coupon_type_id);
	}
	public Integer getCoupon_type_id() {
		return get("coupon_type_id");
	}
	public void setCoupon_num(Integer coupon_num){
		set("coupon_num", coupon_num);
	}
	public Integer getCoupon_num() {
		return get("coupon_num");
	}
	public void setCoupon_price(BigDecimal coupon_price){
		set("coupon_price", coupon_price);
	}
	public BigDecimal getCoupon_price() {
		return get("coupon_price");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	
}

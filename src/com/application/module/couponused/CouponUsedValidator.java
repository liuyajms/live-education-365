package com.application.module.couponused;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class CouponUsedValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CouponUsedValidator.class);
	
	@SuppressWarnings("unused")
	private CouponUsedService unlockService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/unlock/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/unlock/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(CouponUsed.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/unlock/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/unlock/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

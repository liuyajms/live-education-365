package com.application.module.couponused;

import com.application.module.couponget.CouponGet;
import com.application.module.courseitem.CourseItem;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = CouponUsed.table_name, pkName = "id")
public class CouponUsed extends BaseModel<CouponUsed> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(CouponUsed.class);
	
	public static final CouponUsed dao = new CouponUsed().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_coupon_used";
	
	/**
	 * sqlId : app.unlock.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.couponused.splitPageFrom";

	private Integer id;
	private Integer userid;
	private Integer course_id;
	private Integer course_itemid;
	private Integer unlocks;
	private Integer plays;
	private Timestamp create_time;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setUserid(Integer userid){
		set("userid", userid);
	}
	public Integer getUserid() {
		return get("userid");
	}
	public void setCourse_id(Integer course_id){
		set("course_id", course_id);
	}
	public Integer getCourse_id() {
		return get("course_id");
	}
	public void setCourse_itemid(Integer course_itemid){
		set("course_itemid", course_itemid);
	}
	public Integer getCourse_itemid() {
		return get("course_itemid");
	}
	public void setUnlocks(Integer unlocks){
		set("unlocks", unlocks);
	}
	public Integer getUnlocks() {
		return get("unlocks");
	}
	public void setPlays(Integer plays){
		set("plays", plays);
	}
	public Integer getPlays() {
		return get("plays");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}

    public int count(int courseId, int userId) {
        return Db.queryLong("select count(1) from " + table_name + " where course_id =? and userid = ?", courseId, userId).intValue();
    }

    public void addUnlock(int userid, List<CouponGet> coupons, CourseItem item, int needs) {
        CouponUsed o = new CouponUsed();
        o.setUserid(userid);
        o.setCourse_id(item.getCourse_id());
        o.setCourse_itemid(item.getId());
        o.setCreate_time(new Timestamp(System.currentTimeMillis()));
        o.setUnlocks(needs);
        List<Integer> idList = new ArrayList<>();

        Db.tx(() -> {
            coupons.forEach(t -> {
                idList.add(t.getId());
                t.update();
            });
            boolean f = o.set("coupon_idstr", StringUtils.join(idList, ",")).save();
            return f;
        });

    }

    public void addPlay(int userid, CouponGet coupon, CourseItem item) {
        CouponUsed o = new CouponUsed();
        o.setUserid(userid);
        o.setCourse_id(item.getCourse_id());
        o.setCourse_itemid(item.getId());
        o.setCreate_time(new Timestamp(System.currentTimeMillis()));
        o.setPlays(1);
        coupon.setCost(coupon.getCost()+1);
        Db.tx(() -> {
            coupon.update();
            return o.set("coupon_idstr", coupon.getId()).save();
        });

    }
}

package com.application.module.couponused;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = CouponUsedService.serviceName)
public class CouponUsedService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CouponUsedService.class);
	
	public static final String serviceName = "couponUsedService";

    /**
     * 查询课程解锁课时数
     * @param userId
     * @param courseId
     * @return
     */
    public int getUnlockCourseItemNum(int userId, int courseId) {
        int n = CouponUsed.dao.find("select 1 from t_coupon_used where userid = ? and course_id = ?", userId, courseId).size();
        /*if(n == 0) {
            n = BuyCourse.dao.hasBuy(userId, courseId);
            return n > 0 ? -1 : n;
        }*/
        return n;
    }

    /**
     * 是否已解锁课时
     * @param userId
     * @param courseItemId
     * @return
     */
    public int hasUnlock(int userId, int courseItemId) {
        int n = CouponUsed.dao.find("select 1 from t_coupon_used where userid = ? and course_itemid = ?", userId, courseItemId).size();
        return n;
    }
}

package com.application.module.teacherinfo;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class TeacherinfoValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(TeacherinfoValidator.class);
	
	@SuppressWarnings("unused")
	private TeacherinfoService teacherinfoService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/teacherinfo/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/teacherinfo/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Teacherinfo.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/teacherinfo/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/teacherinfo/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

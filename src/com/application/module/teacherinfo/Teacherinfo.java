package com.application.module.teacherinfo;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
@Table(tableName = Teacherinfo.table_name, pkName = "teacher_id")
public class Teacherinfo extends BaseModel<Teacherinfo> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Teacherinfo.class);
	
	public static final Teacherinfo dao = new Teacherinfo().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_teacherinfo";
	
	/**
	 * sqlId : app.teacherinfo.splitPageFrom
	 * 描述：分页from
	 */
    public static final String sqlId_splitPageSel = "app.teacherinfo.splitPageSelect";
    public static final String sqlId_splitPageFrom = "app.teacherinfo.splitPageFrom";

	private Integer teacher_id;
	private String teacher_desc;
	private String teacher_tag;
	private String teacher_feature;
	
	public void setTeacher_id(Integer teacher_id){
		set("teacher_id", teacher_id);
	}
	public Integer getTeacher_id() {
		return get("teacher_id");
	}
	public void setTeacher_desc(String teacher_desc){
		set("teacher_desc", teacher_desc);
	}
	public String getTeacher_desc() {
		return get("teacher_desc");
	}
	public void setTeacher_tag(String teacher_tag){
		set("teacher_tag", teacher_tag);
	}
	public String getTeacher_tag() {
		return get("teacher_tag");
	}
	public void setTeacher_feature(String teacher_feature){
		set("teacher_feature", teacher_feature);
	}
	public String getTeacher_feature() {
		return get("teacher_feature");
	}

    public void saveData(Teacherinfo teacherinfo) {
	    teacherinfo.save();
    }

    public List<Teacherinfo> findByKey(String key) {
        Map<String, Object> map = new HashMap<>();
        map.put("keyword", key);
        return find(getSqlByBeetl("app.teacherinfo.queryTeacherList", map));
    }

    public Teacherinfo findDetail(Integer teacher_id, String colums) {
        if(colums == null) {
            colums = "a.name as teacher_name, a.img as teacher_img, b.*";
        }
        String s = "select " + colums + " from pt_user a left join t_teacherinfo b " +
                "on a.id = b.teacher_id where a.id = ?";
        return findFirst(s, teacher_id);
    }

    public Teacherinfo findDetail(Integer teacher_id) {
	    return findDetail(teacher_id, null);
    }
}

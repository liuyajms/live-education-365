package com.application.module.teacherinfo;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = TeacherinfoService.serviceName)
public class TeacherinfoService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(TeacherinfoService.class);
	
	public static final String serviceName = "teacherinfoService";
	
}

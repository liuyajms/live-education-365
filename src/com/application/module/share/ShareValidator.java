package com.application.module.share;

import com.application.model.Global;
import com.application.module.coursezan.CourseZanService;
import com.application.validate.ApiValidator;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.platform.interceptor.AuthInterceptor;
import com.platform.mvc.user.User;

public class ShareValidator extends ApiValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(ShareValidator.class);
	
	@SuppressWarnings("unused")
	private CourseZanService courseZanService;
	
	protected void validate(Controller c) {
		String actionKey = getActionKey();
		if (actionKey.equals("/api/share/save")){
            User user = AuthInterceptor.getCurrentUser(c.getRequest(), c.getResponse(), true);
            if(isExceeded(user.getId())) {
                addError("exceed max share count");
                log.error("########## share exceed");
            }
		}
	}

    private boolean isExceeded(Integer userid) {
        String value = Global.cacheGetByCode(Global.Code.gift_shareLimit).getValue();
        int count = Share.dao.count(userid);
        if(StrKit.notBlank(value)) {
            if (count >= Integer.parseInt(value)) {
                return true;
            }
        }
        return false;
    }


}

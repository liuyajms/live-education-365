package com.application.module.share;

import com.jfinal.ext.util.DateUtil;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = Share.table_name, pkName = "id")
public class Share extends BaseModel<Share> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Share.class);
	
	public static final Share dao = new Share().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_share";
	
	/**
	 * sqlId : app.share.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.share.splitPageFrom";

    private Integer id;
    private Integer userid;
    private Integer date;
    private String code;
    private Integer used;
    private Integer used2;
    private Integer type;
    private Timestamp create_time;

    public void setId(Integer id){
        set("id", id);
    }
    public Integer getId() {
        return get("id");
    }
    public void setUserid(Integer userid){
        set("userid", userid);
    }
    public Integer getUserid() {
        return get("userid");
    }
    public void setDate(Integer date){
        set("date", date);
    }
    public Integer getDate() {
        return get("date");
    }
    public void setCode(String code){
        set("code", code);
    }
    public String getCode() {
        return get("code");
    }
    public void setUsed(Integer used){
        set("used", used);
    }
    public Integer getUsed() {
        return get("used");
    }
    public void setType(Integer type){
        set("type", type);
    }
    public Integer getType() {
        return get("type");
    }
    public void setCreate_time(Timestamp create_time){
        set("create_time", create_time);
    }
    public Timestamp getCreate_time() {
        return get("create_time");
    }


    public List<Share> findAvailable(int userid) {
	    String s = "select * from " + table_name + " where userid = ? and used <> 1";
        return find(s, userid);
    }

    public List<Share> findAvailablePlay(int userid) {
        String s = "select * from " + table_name + " where userid = ? and used2 <> 1";
        return find(s, userid);
    }

    public int count(Integer userid) {
        int createDay = DateUtil.getCurrentDay();
        String s = "select count(1) from " + table_name + " where userid = ? and date = ?";
        return Db.queryLong(s, userid, createDay).intValue();
    }
}

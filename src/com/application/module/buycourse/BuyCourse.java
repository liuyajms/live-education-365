package com.application.module.buycourse;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = BuyCourse.table_name, pkName = "id")
public class BuyCourse extends BaseModel<BuyCourse> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(BuyCourse.class);
	
	public static final BuyCourse dao = new BuyCourse().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_buy_course";
	
	/**
	 * sqlId : app.myCourse.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.myCourse.splitPageFrom";

	private Integer id;
	private Integer course_id;
	private Integer userid;
	private Timestamp create_time;
	private String course_price;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setCourse_id(Integer course_id){
		set("course_id", course_id);
	}public void setCourse_name(String course_name){
        set("course_name", course_name);
    }
	public Integer getCourse_id() {
		return get("course_id");
	}
	public void setUserid(Integer userid){
		set("userid", userid);
	}
	public Integer getUserid() {
		return get("userid");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCourse_price(String course_price){
		set("course_price", course_price);
	}
	public String getCourse_price() {
		return get("course_price");
	}

    public int hasBuy(int userId, int courseId) {
        return find("select 1 from " + table_name + " where userid = ? and course_id = ?", userId, courseId).size();
    }
}

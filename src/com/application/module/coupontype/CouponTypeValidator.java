package com.application.module.coupontype;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class CouponTypeValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CouponTypeValidator.class);
	
	@SuppressWarnings("unused")
	private CouponTypeService couponTypeService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/couponType/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/couponType/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(CouponType.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/couponType/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/couponType/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

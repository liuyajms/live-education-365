package com.application.module.coupontype;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = CouponTypeService.serviceName)
public class CouponTypeService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CouponTypeService.class);
	
	public static final String serviceName = "couponTypeService";
	
}

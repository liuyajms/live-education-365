package com.application.module.coupontype;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = CouponType.table_name, pkName = "id")
public class CouponType extends BaseModel<CouponType> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(CouponType.class);
	
	public static final CouponType dao = new CouponType().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_coupon_type";
	
	/**
	 * sqlId : app.couponType.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.couponType.splitPageFrom";

	private Integer id;
	private String name;
	private BigDecimal price;
	private Integer num;
	private Integer ord;
	private Timestamp create_time;
	private Integer create_userid;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setName(String name){
		set("name", name);
	}
	public String getName() {
		return get("name");
	}
	public void setPrice(BigDecimal price){
		set("price", price);
	}
	public BigDecimal getPrice() {
		return get("price");
	}
	public void setNum(Integer num){
		set("num", num);
	}
	public Integer getNum() {
		return get("num");
	}
	public void setOrd(Integer ord){
		set("ord", ord);
	}
	public Integer getOrd() {
		return get("ord");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}

}

package com.application.module.confightml;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

@SuppressWarnings("unused")
@Table(tableName = ConfigHtml.table_name, pkName = "id")
public class ConfigHtml extends BaseModel<ConfigHtml> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(ConfigHtml.class);
	
	public static final ConfigHtml dao = new ConfigHtml().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_config_html";
	
	/**
	 * sqlId : app.configHtml.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.configHtml.splitPageFrom";

	private String id;
	private String title;
	private String content;
	
	public void setId(String id){
		set("id", id);
	}
	public String getId() {
		return get("id");
	}
	public void setTitle(String title){
		set("title", title);
	}
	public String getTitle() {
		return get("title");
	}
	public void setContent(String content){
		set("content", content);
	}
	public String getContent() {
		return get("content");
	}
	
}

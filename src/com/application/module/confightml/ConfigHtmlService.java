package com.application.module.confightml;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = ConfigHtmlService.serviceName)
public class ConfigHtmlService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(ConfigHtmlService.class);
	
	public static final String serviceName = "configHtmlService";
	
}

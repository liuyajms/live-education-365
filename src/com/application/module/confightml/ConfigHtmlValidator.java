package com.application.module.confightml;

import com.application.validate.ApiValidator;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;

public class ConfigHtmlValidator extends ApiValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(ConfigHtmlValidator.class);
	
	@SuppressWarnings("unused")
	private ConfigHtmlService configHtmlService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/admin-api/configHtml/save")
                || actionKey.equalsIgnoreCase("/admin-api/configHtml/update")){
			validateRequired("id", "msg", "非空");
            validateRequired("title", "title", "非空");
            validateRequired("content", "msg", "非空");
        }
	}
	

}

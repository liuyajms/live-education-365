package com.application.module.musicitem;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = MusicItem.table_name, pkName = "id")
public class MusicItem extends BaseModel<MusicItem> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(MusicItem.class);
	
	public static final MusicItem dao = new MusicItem().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_music_item";
	
	/**
	 * sqlId : app.musicItem.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.musicItem.splitPageFrom";

	private Integer id;
	private Integer music_id;
	private String item_name;
	private Integer item_ord;
	private Integer item_playcount;
	private String item_length;
	private BigDecimal item_size;
	private String item_url;
	private String item_desc;
	private Timestamp create_time;
	private Integer create_userid;
	private Timestamp delete_time;
	private Integer delete_userid;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setMusic_id(Integer music_id){
		set("music_id", music_id);
	}
	public Integer getMusic_id() {
		return get("music_id");
	}
	public void setItem_name(String item_name){
		set("item_name", item_name);
	}
	public String getItem_name() {
		return get("item_name");
	}
	public void setItem_ord(Integer item_ord){
		set("item_ord", item_ord);
	}
	public Integer getItem_ord() {
		return get("item_ord");
	}
	public void setItem_playcount(Integer item_playcount){
		set("item_playcount", item_playcount);
	}
	public Integer getItem_playcount() {
		return get("item_playcount");
	}
	public void setItem_length(String item_length){
		set("item_length", item_length);
	}
	public String getItem_length() {
		return get("item_length");
	}
	public void setItem_size(BigDecimal item_size){
		set("item_size", item_size);
	}
	public BigDecimal getItem_size() {
		return get("item_size");
	}
	public void setItem_url(String item_url){
		set("item_url", item_url);
	}
	public String getItem_url() {
		return get("item_url");
	}
	public void setItem_desc(String item_desc){
		set("item_desc", item_desc);
	}
	public String getItem_desc() {
		return get("item_desc");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}
	public void setDelete_time(Timestamp delete_time){
		set("delete_time", delete_time);
	}
	public Timestamp getDelete_time() {
		return get("delete_time");
	}
	public void setDelete_userid(Integer delete_userid){
		set("delete_userid", delete_userid);
	}
	public Integer getDelete_userid() {
		return get("delete_userid");
	}
	
}

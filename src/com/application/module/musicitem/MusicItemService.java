package com.application.module.musicitem;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = MusicItemService.serviceName)
public class MusicItemService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(MusicItemService.class);
	
	public static final String serviceName = "musicItemService";
	
}

package com.application.module.musicitem;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class MusicItemValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(MusicItemValidator.class);
	
	@SuppressWarnings("unused")
	private MusicItemService musicItemService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/musicItem/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/musicItem/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(MusicItem.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/musicItem/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/musicItem/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

package com.application.module.coursezan;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class CourseZanValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CourseZanValidator.class);
	
	@SuppressWarnings("unused")
	private CourseZanService courseZanService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/courseZan/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/courseZan/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(CourseZan.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/courseZan/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/courseZan/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

package com.application.module.coursezan;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = CourseZanService.serviceName)
public class CourseZanService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CourseZanService.class);
	
	public static final String serviceName = "courseZanService";
	
}

package com.application.module.coursezan;

import com.application.module.courseitem.CourseItem;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = CourseZan.table_name, pkName = "id")
public class CourseZan extends BaseModel<CourseZan> {

    private static final long serialVersionUID = 6761767368352810428L;

    private static final Log log = Log.getLog(CourseZan.class);

    public static final CourseZan dao = new CourseZan().dao();

    /**
     * 表名称
     */
    public static final String table_name = "t_course_zan";

    /**
     * sqlId : app.courseZan.splitPageFrom
     * 描述：分页from
     */
    public static final String sqlId_splitPageFrom = "app.courseZan.splitPageFrom";

    private Integer id;
    private Integer userid;
    private Integer course_id;
    private Integer course_itemid;
    private Timestamp create_time;

    public void setId(Integer id) {
        set("id", id);
    }

    public Integer getId() {
        return get("id");
    }

    public void setUserid(Integer userid) {
        set("userid", userid);
    }

    public Integer getUserid() {
        return get("userid");
    }

    public void setCourse_id(Integer course_id) {
        set("course_id", course_id);
    }

    public Integer getCourse_id() {
        return get("course_id");
    }

    public void setCourse_itemid(Integer course_itemid) {
        set("course_itemid", course_itemid);
    }

    public Integer getCourse_itemid() {
        return get("course_itemid");
    }

    public void setCreate_time(Timestamp create_time) {
        set("create_time", create_time);
    }

    public Timestamp getCreate_time() {
        return get("create_time");
    }

    public int findCourseZans(Integer courseId) {
        return Db.queryLong("select count(*) from " + table_name + " where course_id = ?", courseId).intValue();
    }

    public CourseZan has(int userId, Integer courseId) {
        return has(userId, courseId, null);
    }

    public CourseZan has(int userId, Integer courseId, Integer itemId) {
        String s = "select id from " + table_name + " where userid = ? and course_id = ? ";
        if(itemId != null) {
            return findFirst(s + "and course_itemid = ?", userId, courseId, itemId);
        }
        return findFirst(s + " and course_itemid is null ", userId, courseId);
    }

    public int addOrDel(Integer courseId, Integer itemId, int userId) {
        CourseZan zan = has(userId, courseId, itemId);
        if (zan != null) {
            zan.delete();
            return 0;
        }
        zan = new CourseZan();
        zan.setCourse_id(courseId);
        zan.setCourse_itemid(itemId);
        zan.setCreate_time(new Timestamp(System.currentTimeMillis()));
        zan.setUserid(userId);
        zan.save();
        return 1;
    }

    public int addItemOrDel(Integer itemId, int userId) {
        String sql = "select * from " + table_name + " where userid = ? and course_itemid = ?";
        CourseZan zan = findFirst(sql, userId, itemId);
        if (zan != null) {
            zan.delete();
            return 0;
        }
        CourseItem item = CourseItem.dao.findById(itemId);
        zan = new CourseZan();
        zan.setCourse_id(item.getCourse_id());
        zan.setCourse_itemid(itemId);
        zan.setCreate_time(new Timestamp(System.currentTimeMillis()));
        zan.setUserid(userId);
        zan.save();
        return 1;
    }

    public BigDecimal findCount(String mStart, String mEnd) {
        String s = "select count(1) as amount from " + table_name + " where create_time between ? and ?";
        return BigDecimal.valueOf(Db.queryLong(s, mStart, mEnd));
    }
}

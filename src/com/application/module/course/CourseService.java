package com.application.module.course;

import com.application.module.couponused.CouponUsedService;
import com.application.module.courseitem.CourseItem;
import com.application.module.coursezan.CourseZan;
import com.application.module.favorite.Favorite;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = CourseService.serviceName)
public class CourseService extends BaseService {

    CouponUsedService couponUsedService;

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CourseService.class);
	
	public static final String serviceName = "courseService";

    public Course getView(int courseId, int uid) {
        Course course = Course.dao.findById(courseId);
//        Teacherinfo info = Teacherinfo.dao.findDetail(course.getTeacher_id());
        Page<CourseItem> items = CourseItem.dao.findByCourseId(1, 3, courseId, uid);
        int hasFavorite = Favorite.dao.has(uid, courseId);
        int hasZan = CourseZan.dao.has(uid, courseId) == null ? 0 : 1;
        int zans = CourseZan.dao.findCourseZans(courseId);

        course.put("has_favorite", hasFavorite)
                .put("has_zan", hasZan)
//                .put("teacher", info)
                .put("zans", zans)
                .put("items_length", items.getTotalRow())
                .put("items", items.getList());

        for (CourseItem item : items.getList()) {
            int hasUnlock = couponUsedService.hasUnlock(uid, item.getId());
            item.put("has_unlock", hasUnlock);
        }

        return course;
    }
}

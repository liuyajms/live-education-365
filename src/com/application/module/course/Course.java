package com.application.module.course;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = Course.table_name, pkName = "id")
public class Course extends BaseModel<Course> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Course.class);
	
	public static final Course dao = new Course().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_course";
	
	/**
	 * sqlId : app.course.splitPageFrom
	 * 描述：分页from
	 */
    public static final String sqlId_splitPageSelect = "app.course.splitPageSelect";
    public static final String sqlId_splitPageFrom = "app.course.splitPageFrom";

	private Integer id;
	private String course_name;
	private String course_img;
	private String course_no;
	private Integer course_status;
	private BigDecimal course_price;
	private Integer course_sales;
	private Integer course_categoryid;
	private Integer course_ord;
	private String course_desc;
    private String course_intro;
	private Timestamp create_time;
	private Integer create_userid;
	private Timestamp delete_time;
	private Integer delete_userid;

    public String getCourse_intro() {
        return course_intro;
    }

    public void setCourse_intro(String course_intro) {
        this.course_intro = course_intro;
    }

    public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setCourse_name(String course_name){
		set("course_name", course_name);
	}
	public String getCourse_name() {
		return get("course_name");
	}
	public void setCourse_img(String course_img){
		set("course_img", course_img);
	}
	public String getCourse_img() {
		return get("course_img");
	}
	public void setCourse_no(String course_no){
		set("course_no", course_no);
	}
	public String getCourse_no() {
		return get("course_no");
	}
	public void setCourse_status(Integer course_status){
		set("course_status", course_status);
	}
	public Integer getCourse_status() {
		return get("course_status");
	}
	public void setCourse_price(BigDecimal course_price){
		set("course_price", course_price);
	}
	public BigDecimal getCourse_price() {
		return get("course_price");
	}
	public void setCourse_sales(Integer course_sales){
		set("course_sales", course_sales);
	}
	public Integer getCourse_sales() {
		return get("course_sales");
	}
	public void setCourse_categoryid(Integer course_categoryid){
		set("course_categoryid", course_categoryid);
	}
	public Integer getCourse_categoryid() {
		return get("course_categoryid");
	}
	public void setCourse_ord(Integer course_ord){
		set("course_ord", course_ord);
	}
	public Integer getCourse_ord() {
		return get("course_ord");
	}
	public void setCourse_desc(String course_desc){
		set("course_desc", course_desc);
	}
	public String getCourse_desc() {
		return get("course_desc");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}
	public void setDelete_time(Timestamp delete_time){
		set("delete_time", delete_time);
	}
	public Timestamp getDelete_time() {
		return get("delete_time");
	}
	public void setDelete_userid(Integer delete_userid){
		set("delete_userid", delete_userid);
	}
	public Integer getDelete_userid() {
		return get("delete_userid");
	}


    public BigDecimal findCount() {
        String s = "select count(1) as amount from " + table_name + " where delete_time is null";
        return BigDecimal.valueOf(Db.queryLong(s));
    }
}

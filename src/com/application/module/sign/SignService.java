package com.application.module.sign;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = SignService.serviceName)
public class SignService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(SignService.class);
	
	public static final String serviceName = "signService";
	
}

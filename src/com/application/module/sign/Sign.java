package com.application.module.sign;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = Sign.table_name, pkName = "id")
public class Sign extends BaseModel<Sign> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Sign.class);
	
	public static final Sign dao = new Sign().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_sign";
	
	/**
	 * sqlId : app.sign.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.sign.splitPageFrom";

	private Integer id;
	private Integer userid;
	private Timestamp sign_time;
	private Integer used;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setUserid(Integer userid){
		set("userid", userid);
	}
	public Integer getUserid() {
		return get("userid");
	}
	public void setSign_time(Timestamp sign_time){
		set("sign_time", sign_time);
	}
	public Timestamp getSign_time() {
		return get("sign_time");
	}
	public void setUsed(Integer used){
		set("used", used);
	}
	public Integer getUsed() {
		return get("used");
	}

    public List<Sign> findAvailableList(Integer userId) {
        return find("select * from "+ table_name + " where userid = ? and used =0 or used is null order by id desc limit 50", userId);
    }

}

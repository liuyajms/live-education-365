package com.application.module.sign;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class SignValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(SignValidator.class);
	
	@SuppressWarnings("unused")
	private SignService signService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/sign/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/sign/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Sign.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/sign/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/sign/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

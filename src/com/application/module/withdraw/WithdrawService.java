package com.application.module.withdraw;

import com.github.wxpay.sdk.MyWXPayConfig;
import com.jfinal.ext.kit.MsgException;
import com.jfinal.ext.util.PayUtil;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

import java.math.BigDecimal;

@Service(name = WithdrawService.serviceName)
public class WithdrawService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(WithdrawService.class);
	
	public static final String serviceName = "withdrawService";

	public static void transferMoney(String orderNo, BigDecimal money, String toUserName) {
	    if(StrKit.isBlank(toUserName)){
	        throw new MsgException("提现用户信息错误");
        }
        if(money == null || money.intValue() <=0) {
            throw new MsgException("提现金额错误");
        }

        try {
            PayUtil.wxTransfer(orderNo, money, toUserName, MyWXPayConfig.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
            throw new MsgException("服务器配置错误");
        }

    }
	
}

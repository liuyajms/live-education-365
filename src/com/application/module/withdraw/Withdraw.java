package com.application.module.withdraw;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = Withdraw.table_name, pkName = "id")
public class Withdraw extends BaseModel<Withdraw> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Withdraw.class);
	
	public static final Withdraw dao = new Withdraw().dao();

	public final static int STATUS_APPLY = 0;
    public final static int STATUS_OK = 1;
    public final static int STATUS_REJECT = 2;

	/**
	 * 表名称
	 */
	public static final String table_name = "t_withdraw";

	/**
	 * sqlId : app.withdraw.splitPageFrom
	 * 描述：分页from
	 */
    public static final String sqlId_splitPageSel = "app.withdraw.splitPageSelect";
	public static final String sqlId_splitPageFrom = "app.withdraw.splitPageFrom";

	private Integer id;
	private Integer apply_userid;
	private BigDecimal apply_money;
	private Timestamp apply_time;
	private String apply_no;
	private Integer status;
	private Integer reply_userid;
	private String reply_content;
	private Timestamp reply_time;

    public String getApply_no() {
        return get("apply_no");
    }

    public void setApply_no(String apply_no) {
        set("apply_no", apply_no);
    }

    public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setApply_userid(Integer apply_userid){
		set("apply_userid", apply_userid);
	}
	public Integer getApply_userid() {
		return get("apply_userid");
	}
	public void setApply_money(BigDecimal apply_money){
		set("apply_money", apply_money);
	}
	public BigDecimal getApply_money() {
		return get("apply_money");
	}
	public void setApply_time(Timestamp apply_time){
		set("apply_time", apply_time);
	}
	public Timestamp getApply_time() {
		return get("apply_time");
	}
	public void setStatus(Integer status){
		set("status", status);
	}
	public Integer getStatus() {
		return get("status");
	}
	public void setReply_userid(Integer reply_userid){
		set("reply_userid", reply_userid);
	}
	public Integer getReply_userid() {
		return get("reply_userid");
	}
	public void setReply_content(String reply_content){
		set("reply_content", reply_content);
	}
	public String getReply_content() {
		return get("reply_content");
	}
	public void setReply_time(Timestamp reply_time){
		set("reply_time", reply_time);
	}
	public Timestamp getReply_time() {
		return get("reply_time");
	}

	public static String generateNo() {
	    return "w" + StrKit.getRandomNo();
    }

    public List<Withdraw> findWaitList(int userid) {
        return find("select * from " + table_name + " where apply_userid = ? and status = ? and delete_time is null", userid, STATUS_APPLY);
    }
}

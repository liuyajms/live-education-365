package com.application.module.buyvip;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = BuyVip.table_name, pkName = "id")
public class BuyVip extends BaseModel<BuyVip> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(BuyVip.class);
	
	public static final BuyVip dao = new BuyVip().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_buy_vip";
	
	/**
	 * sqlId : app.buyVip.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.buyVip.splitPageFrom";

	private Integer id;
	private Integer userid;
	private Integer vip_type_id;
	private BigDecimal vip_price;
	private String vip_expire_type;
	private Integer vip_expire_num;
	private Timestamp create_time;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setUserid(Integer userid){
		set("userid", userid);
	}
	public Integer getUserid() {
		return get("userid");
	}
	public void setVip_type_id(Integer vip_type_id){
		set("vip_type_id", vip_type_id);
	}
	public Integer getVip_type_id() {
		return get("vip_type_id");
	}
	public void setVip_price(BigDecimal vip_price){
		set("vip_price", vip_price);
	}
	public BigDecimal getVip_price() {
		return get("vip_price");
	}
	public void setVip_expire_type(String vip_expire_type){
		set("vip_expire_type", vip_expire_type);
	}
	public String getVip_expire_type() {
		return get("vip_expire_type");
	}
	public void setVip_expire_num(Integer vip_expire_num){
		set("vip_expire_num", vip_expire_num);
	}
	public Integer getVip_expire_num() {
		return get("vip_expire_num");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	
}

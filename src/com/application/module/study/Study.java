package com.application.module.study;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = Study.table_name, pkName = "id")
public class Study extends BaseModel<Study> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Study.class);
	
	public static final Study dao = new Study().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_study";
	
	/**
	 * sqlId : app.study.splitPageFrom
	 * 描述：分页from
	 */
    public static final String sqlId_apiSelect = "app.study.apiSelect";
    public static final String sqlId_apiFrom = "app.study.apiFrom";


	private Integer id;
	private Integer course_id;
	private Integer course_itemid;
	private Integer duration;
	private Timestamp study_time;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setCourse_id(Integer course_id){
		set("course_id", course_id);
	}
	public Integer getCourse_id() {
		return get("course_id");
	}
	public void setCourse_itemid(Integer course_itemid){
		set("course_itemid", course_itemid);
	}
	public Integer getCourse_itemid() {
		return get("course_itemid");
	}
	public void setDuration(Integer duration){
		set("duration", duration);
	}
	public Integer getDuration() {
		return get("duration");
	}
	public void setStudy_time(Timestamp study_time){
		set("study_time", study_time);
	}
	public Timestamp getStudy_time() {
		return get("study_time");
	}

    public int has(int userId, Integer courseId) {
	    //and course_itemid is null
        return find("select 1 from " + table_name + " where userid = ? and course_id = ? ", userId, courseId).size();
    }

    public Study findByCourseItemId(Integer courseItemid, int userid) {
        return findFirst("select * from "+ table_name + " where userid = ? and course_itemid = ? ", userid, courseItemid);
    }

    public int findCount(int userid, int courseId) {
        String sql = "select count(*) from " + table_name + " where userid= ? and course_id =?";
        return Db.queryLong(sql, userid, courseId).intValue();
    }

    public int statsDuration(int courseId) {
        return Db.queryBigDecimal("SELECT COALESCE(sum(duration),0) as duration from t_study where course_id = ?", courseId).intValue();
    }
}

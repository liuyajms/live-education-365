package com.application.module.study;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = StudyService.serviceName)
public class StudyService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(StudyService.class);
	
	public static final String serviceName = "studyService";
	
}

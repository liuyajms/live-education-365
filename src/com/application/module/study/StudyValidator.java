package com.application.module.study;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class StudyValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(StudyValidator.class);
	
	@SuppressWarnings("unused")
	private StudyService studyService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/study/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/study/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Study.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/study/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/study/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

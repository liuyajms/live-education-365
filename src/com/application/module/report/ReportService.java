package com.application.module.report;

import com.application.model.Global;
import com.application.model.Order;
import com.application.module.courseitem.CourseItem;
import com.application.module.courseplay.CoursePlay;
import com.application.module.coursezan.CourseZan;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service(name = ReportService.serviceName)
public class ReportService extends BaseService {

    @SuppressWarnings("unused")
    private static final Log log = Log.getLog(ReportService.class);

    public static final String serviceName = "reportService";

    /**
     * 收益按月分组
     *
     * @param list
     */
    public void groupByMonth(List<Record> list) {

    }

    /**
     * 365锦囊服务号课件收益分成方式：
     * 1、结算公式：A老师的当月收入：本月扣除会员返利后的总收入×Z%×（视频数÷总视频数×N%＋当月视频点击数÷当月总点击数×M%＋视频点赞数÷当月总点赞数×P%）=当月总收入。（Z：分成率；N、M、P代表权重比率。N+M+P=100%）.
     * 2、按月统计，系统自动扣除之前已分成部分。按月结算分成。乙方课件划分为免费和收费两部分，但两部分课件收益计算无差别。
     * 举例：A老师共提供了45个视频放在课架中（不分免费和收费），假如课架中有视频500个，A老师当月的视频或者音频点击数有950个，点赞数80个，当月各类音视频总点击数是10000个，总点赞数是1000个，当月平台总收入是24万元，其中会员返利4万元，余总收入20万元。
     * 老师稿酬占当月营业总收入55%（另45%用于公司运转和视频流量等），视频数权重60%，点击数权重20%，点赞数权重20%，
     * 那么A老师的当月收入是：（240000元-40000元）×55%×（45个视频÷500视频×60%＋950个点击数÷10000点击数×20%＋80个点赞数÷1000个点赞数×20%）=9790元。
     */
    public List<Record> getIncomeList(List<Record> list, String mStart, String mEnd) {
//        1.查询讲师课程数，本月点赞数，播放数
//        查询平台总收入（扣除返利后总的收入）
        BigDecimal totalIncome = Order.dao.findTotalAmountByDate(mStart, mEnd);
        BigDecimal rebateIncome = Order.dao.findTotalRebateByDate(mStart, mEnd);
        BigDecimal totalAmount = totalIncome.subtract(rebateIncome);


//        2.查询本月总的点击数、点赞数、视频总数
        BigDecimal totalClickCount = CoursePlay.dao.findCount(mStart, mEnd);
        BigDecimal totalZansCount = CourseZan.dao.findCount(mStart, mEnd);
        BigDecimal totalCourseNumCount = CourseItem.dao.findCount();

//        3.查询分成比例
        String clickWeight = Global.cacheGetByCode(Global.Code.income_click).getValue().replace("%", "");
        String zansWeight = Global.cacheGetByCode(Global.Code.income_zan).getValue().replace("%", "");
        String courseNumWeight = Global.cacheGetByCode(Global.Code.income_num).getValue().replace("%", "");
        String incomeWeight = Global.cacheGetByCode(Global.Code.income_percent).getValue().replace("%", "");

        for (Record o : list) {
            BigDecimal a = new BigDecimal("0.01");
            BigDecimal cw = new BigDecimal(0);
            BigDecimal zw = new BigDecimal(0);
            BigDecimal nw = new BigDecimal(0);
            BigDecimal pw = new BigDecimal(0);

            Long play_count = o.getLong("play_count") == null ? 0 : o.getLong("play_count");
            Long zans_num = o.getLong("zans_num") == null ? 0 : o.getLong("zans_num");
            Long course_num = o.getLong("course_num") == null ? 0 : o.getLong("course_num");

//            获取点击数比例
            if (StrKit.notBlank(clickWeight) && !"0".equals(totalClickCount.toString())) {
                cw = new BigDecimal(clickWeight).multiply(a)
                        .multiply(BigDecimal.valueOf(play_count))
                        .divide(totalClickCount, RoundingMode.HALF_UP);
            }
//            获取点赞数比例
            if (StrKit.notBlank(zansWeight) && !"0".equals(totalZansCount.toString())) {
                zw = new BigDecimal(zansWeight).multiply(a).multiply(BigDecimal.valueOf(zans_num))
                        .divide(totalZansCount, RoundingMode.HALF_UP);
            }
//            获取课时数比例
            if (StrKit.notBlank(courseNumWeight) && !"0".equals(totalCourseNumCount.toString())) {
                nw = new BigDecimal(courseNumWeight).multiply(a).multiply(BigDecimal.valueOf(course_num))
                        .divide(totalCourseNumCount, RoundingMode.HALF_UP);
            }

            BigDecimal shareAmount = new BigDecimal(incomeWeight).multiply(a).multiply(totalAmount).setScale(2, BigDecimal.ROUND_HALF_UP);;
            BigDecimal res = cw.add(zw).add(nw).multiply(shareAmount).setScale(2, BigDecimal.ROUND_HALF_UP);
            o.set("share_amount", shareAmount).set("total_video", totalCourseNumCount)
                    .set("teacher_income", res)
                    .set("total_zan", totalZansCount).set("total_click", totalClickCount);
        }

        return list;
    }
}

package com.application.module.couponget;

import com.application.module.couponused.CouponUsedService;
import com.application.validate.ApiValidator;
import com.jfinal.core.Controller;
import com.jfinal.ext.util.DateUtil;
import com.jfinal.log.Log;
import com.platform.interceptor.AuthInterceptor;
import com.platform.mvc.user.User;

public class CouponGetValidator extends ApiValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CouponGetValidator.class);
	
	@SuppressWarnings("unused")
	private couponGetService userCouponService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/api/couponGet/sign")){
		    valiSign(controller);
		} else if (actionKey.equals("/api/couponGet/unlock")){
			valiUnlock(controller);
		}
	}

    private void valiSign(Controller c) {
        User user = AuthInterceptor.getCurrentUser(c.getRequest(), c.getResponse(), true);

        if(CouponGet.dao.hasSign(user.getId(), DateUtil.getCurrentDay())) {
            addError("msg", "今日已签到");
        }
    }

    private void valiUnlock(Controller c) {
        User user = AuthInterceptor.getCurrentUser(c.getRequest(), c.getResponse(), true);

        int courseItemId = c.getParaToInt("courseItemId");
        if(new CouponUsedService().hasUnlock(user.getId(), courseItemId) > 0) {
            addError("msg", "该课时已解锁，请刷新尝试");
        }
    }
}

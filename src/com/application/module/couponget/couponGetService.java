package com.application.module.couponget;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = couponGetService.serviceName)
public class couponGetService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(couponGetService.class);
	
	public static final String serviceName = "couponGetService";
	
}

package com.application.module.livecourse;

import com.application.model.Global;
import com.application.util.LiveAuthUtil;
import com.jfinal.ext.util.qrcode.BgImage;
import com.jfinal.ext.util.qrcode.QRCodeUtil;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

import java.text.MessageFormat;

@Service(name = LiveCourseService.serviceName)
public class LiveCourseService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(LiveCourseService.class);
	
	public static final String serviceName = "liveCourseService";

    public static final String proj = PropKit.get("proj_url");
    public static final String qrDir = PropKit.get("qrcode.dir");
    public static final String h5Url = PropKit.get("h5_url");

    public void createOrUpdate(LiveCourse model) {
        String playDomain = Global.dao.findByCode(Global.Code.live_playDomain.toString()).getValue();
        String pushDomain = Global.dao.findByCode(Global.Code.live_pushDomain.toString()).getValue();
        String app = Global.dao.findByCode(Global.Code.live_appName.toString()).getValue();
        String playKey = Global.dao.findByCode(Global.Code.live_playSecret.toString()).getValue();
        String pushKey = Global.dao.findByCode(Global.Code.live_pushSecret.toString()).getValue();
//        long exp = System.currentTimeMillis() / 1000 + 1 * 3600;  // expiration time: 1 hour after current time

        long pushExp = model.getLive_starttime().getTime()/1000 + 3600;
        long playExp = model.getLive_endtime() == null ? pushExp + 3600: model.getLive_endtime().getTime()/1000 + 3600;

//        String uri = "rtmp://play.example.com/live/test";
//        rtmp://pull.wish360.com.cn/365/course1?auth_key=1614524343-0-0-d25e4b1fbad2536c54b029f904b0bf52
        String pushUri = "rtmp://" + pushDomain + "/" + app + "/" + model.getStream_name();
        String pushUrl = LiveAuthUtil.aAuth(pushUri, pushKey, pushExp);
//        http://live.wish360.com.cn/365/course1.m3u8?auth_key=1614432901-0-0-b6977b1ade96a93ab09a83bc4baba1cf
        String playUri = "rtmp://" + playDomain + "/" + app + "/" + model.getStream_name() + ".m3u8";
        String playUrl = LiveAuthUtil.aAuth(playUri, playKey, playExp).replace("rtmp://", "http://");

        model.setLive_url(playUrl);
        model.set("push_url", pushUrl);

        String ids = model.getId() == null ? LiveCourse.dao.findLastestId() : model.getId().toString();

        String s = createQrCode(model.getCreate_userid(), ids);
        model.setLive_qrcode(s);
        if(model.getId() != null) {
            model.update();
        } else {
            model.save(ids);
        }
    }

    /**
     * 生成直播课堂二维码
     * @param uid
     * @param id
     * @return
     */
    private String createQrCode(Integer uid, String id) {
        String urlStr = "{0}/live/{1}.jpg";
        String qrCodeUrl = MessageFormat.format(urlStr, "/files/qrcode", "qr_" + id);
        String savePath = MessageFormat.format(urlStr, qrDir, "qr_"+ id);
        String text = h5Url + "#/pages/live/detail?id=" + id +"&inviteId=" + uid;
        String imgPath = qrDir + "ic_launcher.png";
        String bgPath = qrDir + "ic_bg.jpg";

        try {
            Global g1 = Global.cacheGetByCode(Global.Code.qrcode_xy);
            Global g2 = Global.cacheGetByCode(Global.Code.qrcode_size);
            String[] xy = g1.getValue().split(",");
            String[] size = g2.getValue().split(",");
//                190, 550, 141, 141
            BgImage bg = new BgImage(bgPath, Integer.parseInt(xy[0]), Integer.parseInt(xy[1]), Integer.parseInt(size[0]), Integer.parseInt(size[1]));
            QRCodeUtil.encode(text, imgPath, savePath, true, bg);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return qrCodeUrl;
    }
}

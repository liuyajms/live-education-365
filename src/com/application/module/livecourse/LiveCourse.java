package com.application.module.livecourse;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = LiveCourse.table_name, pkName = "id")
public class LiveCourse extends BaseModel<LiveCourse> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(LiveCourse.class);
	
	public static final LiveCourse dao = new LiveCourse().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_live_course";
	
	/**
	 * sqlId : app.liveCourse.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.liveCourse.splitPageFrom";

	private Integer id;
	private String live_name;
	private String live_cover;
	private String live_desc;
	private String live_images;
	private String live_url;
	private String live_qrcode;
	private Timestamp live_starttime;
	private Timestamp live_endtime;
	private String steam_name;
	private Timestamp create_time;
	private Integer create_userid;

    public String getLive_qrcode() {
        return get("live_qrcode");
    }

    public void setLive_qrcode(String live_qrcode) {
        set("live_qrcode", live_qrcode);
    }

    public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setLive_name(String live_name){
		set("live_name", live_name);
	}
	public String getLive_name() {
		return get("live_name");
	}
	public void setLive_cover(String live_cover){
		set("live_cover", live_cover);
	}
	public String getLive_cover() {
		return get("live_cover");
	}
	public void setLive_desc(String live_desc){
		set("live_desc", live_desc);
	}
	public String getLive_desc() {
		return get("live_desc");
	}
	public void setLive_images(String live_images){
		set("live_images", live_images);
	}
	public String getLive_images() {
		return get("live_images");
	}
	public void setLive_url(String live_url){
		set("live_url", live_url);
	}
	public String getLive_url() {
		return get("live_url");
	}
	public void setLive_starttime(Timestamp live_starttime){
		set("live_starttime", live_starttime);
	}
	public Timestamp getLive_starttime() {
		return get("live_starttime");
	}
	public void setLive_endtime(Timestamp live_endtime){
		set("live_endtime", live_endtime);
	}
	public Timestamp getLive_endtime() {
		return get("live_endtime");
	}
	public void setStream_name(String stream_name){
		set("stream_name", stream_name);
	}
	public String getStream_name() {
		return get("stream_name");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}

    public String findLastestId() {
        String s = "select max(id) as id from " + table_name;
        Integer id = findFirst(s).getId();
        return String.valueOf(id+1);
    }
}

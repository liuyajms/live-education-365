package com.application.module.livecourse;

import com.application.validate.ApiValidator;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;

public class LiveCourseValidator extends ApiValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(LiveCourseValidator.class);
	
	@SuppressWarnings("unused")
	private LiveCourseService liveCourseService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/admin-api/liveCourse/save")){
			validateRequired("live_starttime", "msg", "请输入直播开始时间");
            validateRequired("live_endtime", "msg", "请输入直播开始时间");
		} else if (actionKey.equals("/app/liveCourse/update")){
			
		}
	}
}

package com.application.module.music;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class MusicValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(MusicValidator.class);
	
	@SuppressWarnings("unused")
	private MusicService musicService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/music/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/music/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Music.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/music/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/music/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

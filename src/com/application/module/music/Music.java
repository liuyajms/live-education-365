package com.application.module.music;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;
import java.util.Date;

@SuppressWarnings("unused")
@Table(tableName = Music.table_name, pkName = "id")
public class Music extends BaseModel<Music> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Music.class);
	
	public static final Music dao = new Music().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_music";
	
	/**
	 * sqlId : app.music.splitPageFrom
	 * 描述：分页from
	 */
    public static final String sqlId_splitPageSelect2 = "app.music.splitPageSelect";
	public static final String sqlId_splitPageFrom = "app.music.splitPageFrom";

	private Integer id;
	private String name;
	private String images;
	private Integer playcount;
	private String length;
	private String size;
	private String url;
	private Integer teacher_id;
	private String description;
	private String author;
	private String author_img;
	private Timestamp start_date;
	private Timestamp create_time;
	private Integer create_userid;
	private Timestamp delete_time;
	private Integer delete_userid;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setName(String name){
		set("name", name);
	}
	public String getName() {
		return get("name");
	}
	public void setImages(String images){
		set("images", images);
	}
	public String getImages() {
		return get("images");
	}
	public void setPlaycount(Integer playcount){
		set("playcount", playcount);
	}
	public Integer getPlaycount() {
		return get("playcount");
	}
	public void setLength(String length){
		set("length", length);
	}
	public String getLength() {
		return get("length");
	}
	public void setSize(String size){
		set("size", size);
	}
	public String getSize() {
		return get("size");
	}
	public void setUrl(String url){
		set("url", url);
	}
	public String getUrl() {
		return get("url");
	}
	public void setTeacher_id(Integer teacher_id){
		set("teacher_id", teacher_id);
	}
	public Integer getTeacher_id() {
		return get("teacher_id");
	}
	public void setDescription(String description){
		set("description", description);
	}
	public String getDescription() {
		return get("description");
	}
	public void setAuthor(String author){
		set("author", author);
	}
	public String getAuthor() {
		return get("author");
	}
	public void setAuthor_img(String author_img){
		set("author_img", author_img);
	}
	public String getAuthor_img() {
		return get("author_img");
	}
	public void setStart_date(Timestamp start_date){
		set("start_date", start_date);
	}
	public Timestamp getStart_date() {
		return get("start_date");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}
	public void setDelete_time(Timestamp delete_time){
		set("delete_time", delete_time);
	}
	public Timestamp getDelete_time() {
		return get("delete_time");
	}
	public void setDelete_userid(Integer delete_userid){
		set("delete_userid", delete_userid);
	}
	public Integer getDelete_userid() {
		return get("delete_userid");
	}


    public Music findTop() {
        String sql = "select a.* from t_music a where start_date < ? and delete_time is null order by start_date desc ";
        Music first = findFirst(sql, new Date(System.currentTimeMillis()));
        return first;
    }

    public int updatePlayCount(Integer id) {
        return Db.update("update " + table_name + " set playcount = playcount+1 where id = ?", id);
    }
}

package com.application.module.music;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = MusicService.serviceName)
public class MusicService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(MusicService.class);
	
	public static final String serviceName = "musicService";
	
}

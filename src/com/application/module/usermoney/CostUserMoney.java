package com.application.module.usermoney;

import com.application.model.DictValue;
import com.jfinal.kit.StrKit;

public class CostUserMoney extends AbstractUserMoney {
    @Override
    protected String getContent(String money, int createUserId, DictValue value) {
        String s = StrKit.notBlank(value.getName()) ? value.getName() : value.getValue();
        String content = s.replace("{money}", String.valueOf(-1 * Float.valueOf(money)));
        return content;
    }
}

package com.application.module.usermoney;

import com.application.model.DictValue;
import com.jfinal.kit.StrKit;
import com.platform.mvc.user.User;
/*
分销余额返利标题：两种类型：分昵称是不是手机号，
①如果不是11位手机号就显示  【xxxx】购物返利，例如 ：【宋雷】购物返利；
②如果昵称是11位手机号就显示  例如：【150****3575】购物返利
 */
public class RebateUserMoney extends AbstractUserMoney {
    @Override
    protected String getContent(String money, int createUserId, DictValue value) {
        User createUser = User.dao.cacheGetByUserId(createUserId);
        String user = StrKit.getFormatMobile(createUser.getUsername());
        if (createUser.getName() == null ||
                (createUser.getName().length() != 11) && !createUser.getUsername().equals(createUser.getName())) {
            user = createUser.getName();
        }

        String content = value.getName().replace("{user}", user).replace("{money}", money);
        return content;
    }
}

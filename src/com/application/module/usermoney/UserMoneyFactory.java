package com.application.module.usermoney;

import java.util.HashMap;
import java.util.Map;

public class UserMoneyFactory {
    static Map<String, AbstractUserMoney> map = new HashMap<>();

    static {
        map.put("c", new RebateUserMoney());
        map.put("v", new RebateUserMoney());
        map.put("p", new RebateUserMoney());
    }

    public static AbstractUserMoney getObject(String no) {
        return map.get(no.substring(0, 1));
    }
}

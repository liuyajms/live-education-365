package com.application.module.usermoney;

import com.application.model.DictValue;
import com.application.model.UserMoney;

import java.math.BigDecimal;
import java.sql.Timestamp;

public abstract class AbstractUserMoney {

    public void create(int userid, String money, String orderNo, int createUserId, String gift){
        String type = getClass().getSimpleName().replace("UserMoney", "").toLowerCase();
        DictValue value = DictValue.dao.findByTypeAndCode(DictValue.type_userMoney, type);

        String content = getContent(money, createUserId, value);

        UserMoney um = new UserMoney();
        um.setUserid(userid);
        um.setMoney(new BigDecimal(money));
        um.setType(type);
        um.setOrder_no(orderNo);
        um.setCreate_userid(createUserId);
        um.setCreate_time(new Timestamp(System.currentTimeMillis()));
        um.setContent(content);
        um.setGift(gift);
        um.save();
    }

    public void create(int userid, String money, String orderNo, int createUserId){
        create(userid, money, orderNo, createUserId, "");
    }

    protected abstract String getContent(String money, int createUserId, DictValue value);


}

package com.application.module.usermoney;

import com.application.model.DictValue;

public class ChargeUserMoney extends AbstractUserMoney {
    @Override
    protected String getContent(String money, int createUserId, DictValue value) {
        String content = value.getName().replace("{money}", money);
        return content;
    }
}

package com.application.module.recommend;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = RecommendService.serviceName)
public class RecommendService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(RecommendService.class);
	
	public static final String serviceName = "recommendService";
	
}

package com.application.module.recommend;

import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  model
 * @author liuya
 */
@SuppressWarnings("unused")
@Table(tableName = "t_recommend", pkName = "id")
public class Recommend extends BaseModel<Recommend> {

    public final static String TYPE_COURSE="course";
    public final static String TYPE_NEWS="news";
    public final static String TYPE_CAT="cat";


    private static final long serialVersionUID = 6761767368352810428L;

    private static Logger log = Logger.getLogger(Recommend.class);

    public static final Recommend dao = new Recommend();

    /**
     * 字段描述： 
     * 字段类型：varchar(32)  长度：32
     */
    public static final String column_ids = "id";

    /**
     * 字段描述： 
     * 字段类型：varchar(32)  长度：32
     */
    public static final String column_module = "type";

    /**
     * 字段描述： 
     * 字段类型：varchar(32)  长度：32
     */
    public static final String column_newsId = "module_id";

    /**
     * 字段描述： 
     * 字段类型：timestamp  长度：null
     */
    public static final String column_createTime = "create_time";

    /**
     * 字段描述： 
     * 字段类型：varchar(32)  长度：32
     */
    public static final String column_createUserId = "create_userid";


    /**
     * sqlId : application.recommend.splitPageFrom
     * 描述：分页from
     */
    public static final String sqlId_splitPageFrom = "app.recommend.splitPageFrom";
    public static String sqlId_getList = "app.recommend.getList";
    public static String sqlId_pageSelect = "app.recommend.pageSelect";
    public static String sqlId_isPushed = "app.recommend.isPushed";
    public static String sqlId_getCourseListSelect = "app.recommend.getCourseListSelect";
    public static String sqlId_getCourseListFrom = "app.recommend.getCourseListFrom";
    public static String sqlId_getNewsListSelect = "app.recommend.getNewsListSelect";
    public static String sqlId_getNewsListFrom = "app.recommend.getNewsListFrom";

    public int findAllCount() {
        return Db.queryLong("select count(*) as n from t_recommend").intValue();
    }

    public void delExtra(int size) {
        Db.update("delete from t_recommend order by create_time limit ?", size);
    }

    public List<Recommend> findListByType(String type){
        Map<String, Object> map = new HashMap<>();
        map.put("type", type);

        String sel = getSqlByBeetl(sqlId_pageSelect, map);
        String from = getSqlByBeetl(sqlId_splitPageFrom, map);

        return find(sel + from);
    }

    public Recommend findByModuleId(String type, int moduleId) {
        return findFirst("select * from " + getTableName() + " where type = ? and module_id = ?", type, moduleId);
    }
}

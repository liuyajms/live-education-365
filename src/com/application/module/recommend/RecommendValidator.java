package com.application.module.recommend;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class RecommendValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(RecommendValidator.class);
	
	@SuppressWarnings("unused")
	private RecommendService recommendService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/recommend/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/recommend/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Recommend.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/recommend/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/recommend/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

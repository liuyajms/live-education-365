package com.application.module.courseplay;

import com.application.module.courseitem.CourseItem;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = CoursePlay.table_name, pkName = "id")
public class CoursePlay extends BaseModel<CoursePlay> {

    private static final long serialVersionUID = 6761767368352810428L;

    private static final Log log = Log.getLog(CoursePlay.class);

    public static final CoursePlay dao = new CoursePlay().dao();

    /**
     * 表名称
     */
    public static final String table_name = "t_course_play";

    /**
     * sqlId : app.coursePlay.splitPageFrom
     * 描述：分页from
     */
    public static final String sqlId_splitPageFrom = "app.coursePlay.splitPageFrom";

    private Integer id;
    private Integer userid;
    private Integer course_id;
    private Integer course_itemid;
    private Timestamp create_time;

    public void setId(Integer id) {
        set("id", id);
    }

    public Integer getId() {
        return get("id");
    }

    public void setUserid(Integer userid) {
        set("userid", userid);
    }

    public Integer getUserid() {
        return get("userid");
    }

    public void setCourse_id(Integer course_id) {
        set("course_id", course_id);
    }

    public Integer getCourse_id() {
        return get("course_id");
    }

    public void setCourse_itemid(Integer course_itemid) {
        set("course_itemid", course_itemid);
    }

    public Integer getCourse_itemid() {
        return get("course_itemid");
    }

    public void setCreate_time(Timestamp create_time) {
        set("create_time", create_time);
    }

    public Timestamp getCreate_time() {
        return get("create_time");
    }


    public void addPlay(Integer itemId, int userId) {
        CourseItem item = CourseItem.dao.cacheGet(itemId);
        CoursePlay o = new CoursePlay();
        o.setCourse_id(item.getCourse_id());
        o.setCourse_itemid(itemId);
        o.setCreate_time(new Timestamp(System.currentTimeMillis()));
        o.setUserid(userId);
        o.save();
    }

    public BigDecimal findCount(String mStart, String mEnd) {
        String s = "select count(1) as amount from " + table_name + " where create_time between ? and ?";
        return BigDecimal.valueOf(Db.queryLong(s, mStart, mEnd));
    }
}

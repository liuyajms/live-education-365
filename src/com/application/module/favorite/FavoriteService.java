package com.application.module.favorite;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = FavoriteService.serviceName)
public class FavoriteService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(FavoriteService.class);
	
	public static final String serviceName = "favoriteService";
	
}

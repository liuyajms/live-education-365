package com.application.module.favorite;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = Favorite.table_name, pkName = "id")
public class Favorite extends BaseModel<Favorite> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Favorite.class);
	
	public static final Favorite dao = new Favorite().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_favorite";
	
	/**
	 * sqlId : app.favorite.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.favorite.splitPageFrom";
    public static final String sqlId_apiSelect = "app.favorite.apiSelect";
    public static final String sqlId_apiFrom = "app.favorite.apiFrom";

	private Integer id;
	private Integer userid;
	private Integer course_id;
	private Timestamp create_time;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setUserid(Integer userid){
		set("userid", userid);
	}
	public Integer getUserid() {
		return get("userid");
	}
	public void setCourse_id(Integer course_id){
		set("course_id", course_id);
	}
	public Integer getCourse_id() {
		return get("course_id");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}

    public int has(int userId, Integer courseId) {
        return find("select 1 from " + table_name + " where userid = ? and course_id = ?", userId, courseId).size();
    }

    public int addOrDel(int courseId, int userId) {
        Favorite first = findFirst("select * from " + table_name + " where course_id = ? and userid = ?", courseId, userId);
        if (first == null) {
            Favorite f = new Favorite();
            f.setCourse_id(courseId);
            f.setUserid(userId);
            f.setCreate_time(new Timestamp(System.currentTimeMillis()));
            f.save();
            return 1;
        }
        first.delete();
        return 0;
    }

    public int findCourseFavorites(Integer courseId) {
       return Db.queryLong("select count(*) from " + table_name + " where course_id = ?", courseId).intValue();
    }
}

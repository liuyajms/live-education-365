package com.application.module.favorite;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class FavoriteValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(FavoriteValidator.class);
	
	@SuppressWarnings("unused")
	private FavoriteService favoriteService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/favorite/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/favorite/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Favorite.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/favorite/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/favorite/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

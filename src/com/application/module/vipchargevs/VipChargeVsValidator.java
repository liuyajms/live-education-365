package com.application.module.vipchargevs;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class VipChargeVsValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(VipChargeVsValidator.class);
	
	@SuppressWarnings("unused")
	private VipChargeVsService vipChargeVsService;
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/vipChargeVs/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/vipChargeVs/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(VipChargeVs.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/vipChargeVs/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/vipChargeVs/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

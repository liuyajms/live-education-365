package com.application.module.vipchargevs;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = VipChargeVs.table_name, pkName = "id")
public class VipChargeVs extends BaseModel<VipChargeVs> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(VipChargeVs.class);
	
	public static final VipChargeVs dao = new VipChargeVs().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_vip_charge_vs";
	
	/**
	 * sqlId : app.vipChargeVs.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.vipChargeVs.splitPageFrom";

	private Integer id;
	private String title;
	private String normal_text;
	private String normal_bgcolor;
	private String normal_color;
	private String vip_text;
	private String vip_bgcolor;
	private String vip_color;
	private Integer ord;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setTitle(String title){
		set("title", title);
	}
	public String getTitle() {
		return get("title");
	}
	public void setNormal_text(String normal_text){
		set("normal_text", normal_text);
	}
	public String getNormal_text() {
		return get("normal_text");
	}
	public void setNormal_bgcolor(String normal_bgcolor){
		set("normal_bgcolor", normal_bgcolor);
	}
	public String getNormal_bgcolor() {
		return get("normal_bgcolor");
	}
	public void setNormal_color(String normal_color){
		set("normal_color", normal_color);
	}
	public String getNormal_color() {
		return get("normal_color");
	}
	public void setVip_text(String vip_text){
		set("vip_text", vip_text);
	}
	public String getVip_text() {
		return get("vip_text");
	}
	public void setVip_bgcolor(String vip_bgcolor){
		set("vip_bgcolor", vip_bgcolor);
	}
	public String getVip_bgcolor() {
		return get("vip_bgcolor");
	}
	public void setVip_color(String vip_color){
		set("vip_color", vip_color);
	}
	public String getVip_color() {
		return get("vip_color");
	}
	public void setOrd(Integer ord){
		set("ord", ord);
	}
	public Integer getOrd() {
		return get("ord");
	}

    public List<VipChargeVs> findList() {
        return find("select * from " + table_name + " order by ord,id limit 10 ");
    }
}

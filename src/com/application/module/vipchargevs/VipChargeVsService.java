package com.application.module.vipchargevs;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = VipChargeVsService.serviceName)
public class VipChargeVsService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(VipChargeVsService.class);
	
	public static final String serviceName = "vipChargeVsService";
	
}

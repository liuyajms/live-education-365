package com.application.module.userinfo;

import com.application.common.Constant;
import com.application.validate.ApiValidator;
import com.jfinal.core.Controller;
import com.platform.interceptor.AuthInterceptor;
import com.platform.mvc.user.User;

public class UserinfoValidator extends ApiValidator {


	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/admin-api/user/changeRole")){
		    valiUpdate(controller);
		}
	}

    private void valiUpdate(Controller c) {
        User cUser = AuthInterceptor.getCurrentUser(c.getRequest(), c.getResponse(), true);
        /*if(user.getId().intValue() == c.getParaToInt("id") && !user.getRoleId().equalsIgnoreCase(c.getPara("role_id"))) {
            addError("msg", "禁止修改管理员角色");
        }*/
        /*Integer userId = c.getParaToInt("id");
        User user = User.dao.findById(userId);*/
        if(!cUser.getRoleId().equalsIgnoreCase(Constant.group_SuperAdmin)) {
            addError("msg", "请联系超级管理员");
        }

    }


}

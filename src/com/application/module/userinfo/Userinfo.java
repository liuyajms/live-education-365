package com.application.module.userinfo;

import com.application.model.Global;
import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

@SuppressWarnings("unused")
@Table(tableName = Userinfo.table_name, pkName = "userid")
public class Userinfo extends BaseModel<Userinfo> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Userinfo.class);
	
	public static final Userinfo dao = new Userinfo().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_userinfo";
	
	/**
	 * sqlId : app.teacherinfo.splitPageFrom
	 * 描述：分页from
	 */
    public static final String sqlId_splitPageSel = "app.userinfo.splitPageSelect";
    public static final String sqlId_splitPageFrom = "app.userinfo.splitPageFrom";

    @Deprecated
    public void create(Integer userId) {
        Global global = Global.cacheGetByCode(Global.Code.default_tips);
        Userinfo userinfo = new Userinfo();
        userinfo.set("tips", global.getValue())
                .set("userid", userId)
                .save();
    }

    /**
     * 查询用户详细信息，及默认提醒事项
     * @param id
     * @return
     */
    public Userinfo findInfo(Integer id) {
        Userinfo userinfo = findById(id);
        Global global = Global.cacheGetByCode(Global.Code.default_tips);
        if(userinfo == null) {
            userinfo = new Userinfo();
        }
        if (userinfo.get("tips") == null) {
            userinfo.set("tips", global.getValue());
        }
        return userinfo;
    }
}

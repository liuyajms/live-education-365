package com.application.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RouteItemVo implements Serializable {
    private int orderid;
    private int routeid;
    private List<Integer> idarr;

    private Integer addressid;
    private Integer stationid;

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public int getRouteid() {
        return routeid;
    }

    public void setRouteid(int routeid) {
        this.routeid = routeid;
    }

    public List<Integer> getIdarr() {
        if(idarr == null) {
            idarr = new ArrayList<>();
            idarr.add(getOrderid());
        }
        return idarr;
    }

    public void setIdarr(List<Integer> idarr) {
        this.idarr = idarr;
    }

    public Integer getAddressid() {
        return addressid;
    }

    public void setAddressid(Integer addressid) {
        this.addressid = addressid;
    }

    public Integer getStationid() {
        return stationid;
    }

    public void setStationid(Integer stationid) {
        this.stationid = stationid;
    }
}

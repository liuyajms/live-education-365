package com.application.vo;

import com.platform.mvc.user.User;

import java.io.Serializable;

public class UserVo implements Serializable {

    private String authmark;
    private User user;
    private int has_stats_menu;
    private int has_signed;

    public UserVo(String authmark, User user, int has_stats_menu, int has_signed) {
        this.authmark = authmark;
        this.user = user;
        this.has_stats_menu = has_stats_menu;
        this.has_signed = has_signed;
    }

    public UserVo(String authmark, User user) {
        this.authmark = authmark;
        this.user = user;
    }

    public UserVo(String authmark, User user, int has_stats_menu) {
        this.authmark = authmark;
        this.user = user;
        this.has_stats_menu = has_stats_menu;
    }

    public int getHas_signed() {
        return has_signed;
    }

    public void setHas_signed(int has_signed) {
        this.has_signed = has_signed;
    }

    public int getHas_stats_menu() {
        return has_stats_menu;
    }

    public void setHas_stats_menu(int has_stats_menu) {
        this.has_stats_menu = has_stats_menu;
    }

    public String getAuthmark() {
        return authmark;
    }

    public void setAuthmark(String authmark) {
        this.authmark = authmark;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

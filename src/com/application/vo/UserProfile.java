package com.application.vo;

import java.io.Serializable;
import java.sql.Date;

public class UserProfile implements Serializable {
    private int point;
    private String roleId;
    private java.sql.Date roleDate;
    private int id;
    private int has_signed;

    public UserProfile(int point, String roleId, Date roleDate, int id, int has_signed) {
        this.point = point;
        this.roleId = roleId;
        this.roleDate = roleDate;
        this.id = id;
        this.has_signed = has_signed;
    }

    public int getHas_signed() {
        return has_signed;
    }

    public void setHas_signed(int has_signed) {
        this.has_signed = has_signed;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Date getRoleDate() {
        return roleDate;
    }

    public void setRoleDate(Date roleDate) {
        this.roleDate = roleDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}

package com.application.vo;

import java.io.Serializable;

public class DeliverTimeVo implements Serializable {
    private String date;
    private String timeStart;
    private String timeEnd;

    public DeliverTimeVo() {
    }

    public DeliverTimeVo(String date, String timeStart, String timeEnd) {
        this.date = date;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }
}

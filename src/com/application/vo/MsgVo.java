package com.application.vo;

import java.io.Serializable;
import java.util.Date;

public class MsgVo implements Serializable {
    private int id;
    private String title;
    private String content;
    private String type;
    private String typeName;
    private String toUserid;
    private Date readTime;
    private Date createTime;

    public MsgVo(int id, String title, String content, String type, String typeName, String toUserid, Date readTime, Date createTime) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.type = type;
        this.typeName = typeName;
        this.toUserid = toUserid;
        this.readTime = readTime;
        this.createTime = createTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Date getReadTime() {
        return readTime;
    }

    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getToUserid() {
        return toUserid;
    }

    public void setToUserid(String toUserid) {
        this.toUserid = toUserid;
    }
}

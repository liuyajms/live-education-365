package com.application.marquee;

import com.application.api.entity.ResultEntity;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

@Deprecated
public class MarqueeValidator extends Validator {

    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(MarqueeValidator.class);

    protected void validate(Controller controller) {
        String actionKey = getActionKey();
        if (actionKey.equals("/shop/talent/marquee/save")) {
            //限制推送五条
            long size = Marquee.dao.findAllCount();
            if (size >= 5) {
                addError("msg", "已超过最大轮播图5张的限制");
            }

        } else if (actionKey.equals("/shop/talent/marquee/update")) {

        }
    }

    protected void handleError(Controller controller) {
        controller.keepModel(Marquee.class);

        String actionKey = getActionKey();
        if (actionKey.equals("/shop/talent/marquee/save")) {
//            controller.render("/talent/xxx.html");

            controller.renderJson(new ResultEntity(HttpStatus.SC_BAD_REQUEST, controller.getAttrForStr("msg")));

        } else if (actionKey.equals("/shop/talent/marquee/update")) {
            controller.render("/talent/xxx.html");

        }
    }

}

package com.application.marquee;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import org.apache.log4j.Logger;

import java.util.List;

/**
 *  model
 * @author liuya
 */
@SuppressWarnings("unused")
@Table(tableName = "t_marquee", pkName = "id")
public class Marquee extends BaseModel<Marquee> {


	private static final long serialVersionUID = 6761767368352810428L;

	private static Logger log = Logger.getLogger(Marquee.class);
	
	public static final Marquee dao = new Marquee();
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_ids = "id";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_module = "type";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_newsId = "module_id";
	
	/**
	 * 字段描述： 
	 * 字段类型：timestamp  长度：null
	 */
	public static final String column_createTime = "create_time";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_createUserId = "create_userid";
	
	
	/**
	 * sqlId : application.marquee.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.marquee.splitPageFrom";
	public static String sqlId_getList = "app.marquee.getList";
	public static String sqlId_pageSelect = "app.marquee.pageSelect";
	public static String sqlId_isPushed = "app.marquee.isPushed";
    public static String sqlId_getModuleListSelect = "app.marquee.getModuleListSelect";
    public static String sqlId_getModuleListFrom = "app.marquee.getModuleListFrom";


	public int findAllCount() {
		return Db.queryLong("select count(*) as n from t_marquee").intValue();
	}

	public void delExtra(int size) {
		Db.update("delete from t_marquee order by create_time limit ?", size);
	}

	public List<Marquee> findListByType(String type){
		List<Marquee> list;
		String sql = "select * from " + getTableName() ;
		if(StrKit.notBlank(type)){
			sql  += " where type = ?";
			list = find(sql, type);
		} else {
			list = find(sql);
		}
		return list;
	}
}

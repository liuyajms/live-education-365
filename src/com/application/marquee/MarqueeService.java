package com.application.marquee;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = MarqueeService.serviceName)
public class MarqueeService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(MarqueeService.class);
	
	public static final String serviceName = "marqueeService";
	
}

package com.application.condition;

import com.jfinal.kit.StrKit;

import java.io.Serializable;

public class OrderCondition implements Serializable {
    private String startDate;
    private String endDate;
    private Integer status;
    private String stationNo;
    private Integer userid;
    private String keyword;
    private int comment;
    private int level;
    private int stationStatus;
    private String id;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStationStatus() {
        return stationStatus;
    }

    public void setStationStatus(int stationStatus) {
        this.stationStatus = stationStatus;
    }

    public String getQuerySql() {
        StringBuffer sb = new StringBuffer(1000);

        if (getUserid() != null) {
            if (getLevel() == 1) {
                sb.append(" and order_userid in ( select id from pt_user where referee_userid = " + getUserid() + ")");
            } else if (getLevel() == 2) {
                String s = "SELECT a.id from pt_user a left JOIN pt_user b on a.referee_userid = b.id\n" +
                        "where b.referee_userid = " + getUserid();
                sb.append(" and order_userid in (" + s + ")");
            } else {
                sb.append(" and order_userid = " + getUserid());
            }
        }

        if (getComment() == 1) {
            sb.append(" and is_comment = 0 ");
        }

        if(StrKit.notBlank(getKeyword())){
            sb.append(" and (instr(order_username, '"+ getKeyword() +"') >0 or instr(order_no, '"+ getKeyword() +"') >0 ) ");
        }

        if(StrKit.notBlank(getStartDate())){
            sb.append(" and create_time >= '" + getStartDate() + "'");
        }

        if(StrKit.notBlank(getEndDate())){
            sb.append(" and create_time <= '" + getEndDate() + "'");
        }

        Integer originStatus = getStatus();
        if(originStatus != null){
            if (originStatus >= 400) {
                sb.append(" and station_status =" + originStatus);
            } else if(originStatus == 1){//查询待收货订单，需要排除掉待自提订单
                sb.append(" and station_status is null ");
            }
        }

        if(StrKit.notBlank(getStationNo())) {
            sb.append(" and station_id = '" + getStationNo() + "'");
        }

        if(getStationStatus() != 0) {
            sb.append(" and station_status = " + getStationStatus());
        }

        if(StrKit.notBlank(getId())) {
            sb.append(" and id = " + getId());
        }

        if(StrKit.notBlank(getType())) {
            sb.append(" and order_type = " + getType());
        }

        return sb.toString();
    }
    
    public OrderCondition() {
    }

    public OrderCondition(String startDate, String endDate, Integer status, String stationNo, Integer userid) {
        this(startDate, endDate, status, stationNo, userid, null, 0);
    }

    public OrderCondition(String startDate, String endDate, Integer status, String stationNo, Integer userid, String keyword, int comment) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.stationNo = stationNo;
        this.userid = userid;
        this.keyword = keyword;
        this.comment = comment;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStationNo() {
        return stationNo;
    }

    public void setStationNo(String stationNo) {
        this.stationNo = stationNo;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}

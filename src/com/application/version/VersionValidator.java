package com.application.version;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
import org.apache.log4j.Logger;

public class VersionValidator extends Validator {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(VersionValidator.class);
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/shop/talent/version/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/shop/talent/version/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Version.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/shop/talent/version/save")){
			controller.render("/talent/xxx.html");
		
		} else if (actionKey.equals("/shop/talent/version/update")){
			controller.render("/talent/xxx.html");
		
		}
	}
	
}

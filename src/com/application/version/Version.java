package com.application.version;

import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 *  model
 * @author liuya
 */
@SuppressWarnings("unused")
@Table(tableName = Version.table_name, pkName = "id")
public class Version extends BaseModel<Version> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static Logger log = Logger.getLogger(Version.class);
	
	public static final Version dao = new Version();

	public static final String table_name = "t_version";


	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_ids = "id";
	
	/**
	 * 字段描述： 
	 */
	public static final String column_num = "num";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(512)  长度：512
	 */
	public static final String column_description = "description";
	
	/**
	 * 字段描述：是否强制更新 
	 * 字段类型：tinyint(4)  长度：null
	 */
	public static final String column_forced = "forced";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(512)  长度：512
	 */
	public static final String column_url = "url";
	
	/**
	 * 字段描述：ipa,apk 
	 * 字段类型：varchar(255)  长度：255
	 */
	public static final String column_platform = "platform";

	public static final String column_createTime = "createTime";

	/**
	 * sqlId : talent.version.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "talent.version.splitPageFrom";

	private String ids;
	private int num;//最新的内部版本号
	private String description;
	private String forced;
	private String url;
	private String platform;
	private String version;
	private Date createTime;

	public Date getCreateTime() {
		return get(column_createTime);
	}

	public void setCreateTime(Date createTime) {
		set(column_createTime, createTime);
	}

	public void setIds(String ids){
		set(column_ids, ids);
	}
	public String getIds() {
		return get(column_ids);
	}
	public void setNum(int num){
		set(column_num, num);
	}
	public int getNum() {
		return get(column_num);
	}
	public void setDescription(String description){
		set(column_description, description);
	}
	public String getDescription() {
		return get(column_description);
	}
	public void setForced(Integer forced){
		set(column_forced, forced);
	}
	public Integer getForced() {
		return get(column_forced);
	}
	public void setUrl(String url){
		set(column_url, url);
	}
	public String getUrl() {
		return get(column_url);
	}
	public void setPlatform(String platform){
		set(column_platform, platform);
	}
	public String getPlatform() {
		return get(column_platform);
	}

	public Version findLatestVersion(String platform) {
		return findFirst("select * from "+ table_name + " where platform = ? order by num desc limit 1", platform);
	}
}

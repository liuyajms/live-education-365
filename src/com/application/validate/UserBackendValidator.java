package com.application.validate;

import com.jfinal.core.Controller;
import com.platform.mvc.user.User;

public class UserBackendValidator extends ApiValidator {
    @Override
    protected void validate(Controller c) {
        String key = getActionKey();
        if ("/admin-api/user/delete".equalsIgnoreCase(key)) {
            valiDelete(c);
        }

    }

    private void valiDelete(Controller c) {
        String ids = c.getPara() == null ? c.getPara("id") : c.getPara();
        for (String s : ids.split(",")) {
            User user = User.dao.findById(s);
            if(user.get("openid") != null && !"".equals(user.get("openid"))) {
                addError("msg", "【"+ user.getName() +"】用户为微信绑定用户，不能删除。");
                break;
            }
        }
    }


}

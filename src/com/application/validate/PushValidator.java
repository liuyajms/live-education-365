package com.application.validate;

import com.application.model.Push;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.platform.mvc.base.BaseValidator;

public class PushValidator extends BaseValidator {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(PushValidator.class);
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/app/push/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/app/push/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Push.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/app/push/save")){
			controller.render("/app/xxx.html");
		
		} else if (actionKey.equals("/app/push/update")){
			controller.render("/app/xxx.html");
		
		}
	}
	
}

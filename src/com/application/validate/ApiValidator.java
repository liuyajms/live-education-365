package com.application.validate;

import com.application.api.entity.ResultEntity;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.platform.mvc.base.BaseValidator;

import java.util.HashMap;
import java.util.Map;

public abstract class ApiValidator extends BaseValidator {

    @Override
    protected void handleError(Controller c) {
        String actionKey = getActionKey();
        if(actionKey.startsWith("/api")){
            String msg = c.getAttrForStr("regMsg");
            if(StrKit.isBlank(msg)){
                msg = c.getAttrForStr("msg");
            }
            c.renderJson(new ResultEntity(400, msg));
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("status", 500);
            map.put("data", null);
            map.put("msg", c.getAttrForStr("msg"));
            map.put("url", "");
            c.renderJson(map);
        }
    }

    protected void addError(String errorMessage) {
        super.addError("msg", errorMessage);
    }
}

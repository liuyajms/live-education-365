package com.application.validate;

import com.application.common.OrderStatus;
import com.jfinal.core.Controller;

import java.util.ArrayList;
import java.util.List;

public class OrderListValidator extends ApiValidator {
    @Override
    protected void validate(Controller c) {
        listVali(c);
    }

    private void listVali(Controller c) {
        Integer status = c.getParaToInt("status");
        List<Integer> sList = new ArrayList<>();
        if (status != null) {
            switch (status) {
                case 0:
                    sList.add(OrderStatus.PAY_WAIT);
                    break;
                case 1:
                    sList.add(OrderStatus.PAY_OVER);
                    sList.add(OrderStatus.TAKE_WAIT);
                    break;
                case 2:
                    sList.add(OrderStatus.TAKE_OVER);
                    break;
                case 3:
                    sList.add(OrderStatus.CLOSED_CANCEL);
                    sList.add(OrderStatus.CLOSED_TIMEOUT);
                    sList.add(OrderStatus.CLOSED_OTHER);
                    break;
                case 4:
                    sList.add(OrderStatus.TAKE_WAIT);
                    break;
                default:
                    addError("regMsg", "status参数错误");
                    return;
            }
        }

        c.setAttr("client_status", sList);
    }

}

package com.application.validate;

import com.application.common.OrderStatus;
import com.application.model.Order;
import com.jfinal.core.Controller;

public class OrderValidator extends ApiValidator {
    @Override
    protected void validate(Controller c) {
        String key = getActionKey();
        if ("/api/order/pay".equalsIgnoreCase(key)) {
            Order order = Order.dao.findById(c.getPara("orderId"));
            if (order == null || order.getDelete_time() != null) {
                addError("msg", "订单不存在或已被删除");
                return;
            }
            if (order.getOrder_status() != OrderStatus.PAY_WAIT) {
                addError("msg", "订单状态异常");
                return;
            }
            c.setAttr("order", order);
        }

    }


}

package com.application.validate;

import com.application.model.Order;
import com.application.model.VipChargeType;
import com.application.module.buycourse.BuyCourse;
import com.application.module.coupontype.CouponType;
import com.application.module.course.Course;
import com.jfinal.core.Controller;
import com.platform.constant.ConstantWebContext;
import org.apache.log4j.Logger;

/*
校验订单类型是否符合
 */
public class OrderTypeValidator extends ApiValidator {

    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(OrderTypeValidator.class);

    protected void validate(Controller c) {
        int type = c.getParaToInt("order_type");
        Integer moduleId = c.getParaToInt("module_id");
        switch (type) {
            case Order.TYPE_VIP_CHARGE:
                valiVip(c, moduleId);
                break;
            case Order.TYPE_PLAY_COUPON:
                valiCoupon(c, moduleId);
                break;
            case Order.TYPE_COURSE:
                valiCourse(c, moduleId);
                break;
            default:
                addError("msg", "订单类别错误");

        }
    }

    private void valiCoupon(Controller c, Integer moduleId) {
        CouponType obj = CouponType.dao.findById(moduleId);
        if (obj == null) {
            addError("msg", "参数错误");
            return;
        }

        c.setAttr("order_price", obj.getPrice());
    }

    private void valiVip(Controller c, Integer moduleId) {
        VipChargeType obj = VipChargeType.dao.findById(moduleId);
        if (obj == null) {
            addError("msg", "参数错误");
            return;
        }

        c.setAttr("order_price", obj.getRequire_money());
    }

    private void valiCourse(Controller c, Integer moduleId) {
        Course obj = Course.dao.findById(moduleId);
        if (obj == null) {
            addError("参数错误");
            return;
        }
        //校验该用户是否已购买此课程
        int uid = Integer.parseInt(c.getAttr(ConstantWebContext.request_cUserIds));
        int i = BuyCourse.dao.hasBuy(uid, moduleId);
        if(i > 0) {
            addError("请勿重复购买此课程");
        }

        c.setAttr("order_price", obj.getCourse_price());
    }
}

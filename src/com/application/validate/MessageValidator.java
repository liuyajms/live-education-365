package com.application.validate;

import com.application.model.Message;
import com.jfinal.core.Controller;
import com.platform.mvc.user.User;

public class MessageValidator extends ApiValidator {
    @Override
    protected void validate(Controller c) {
        String key = getActionKey();
        if ("/admin/message/save".equalsIgnoreCase(key)) {
            String type = c.getPara("type");
            Integer userid = c.getParaToInt("to_userid");
            if (!Message.TYPE_SYS.equalsIgnoreCase(type) && (userid == null || User.dao.findById(userid) == null)) {
                addError("msg", "发送的用户不存在或已注销");
                return;
            }

        }

    }


}

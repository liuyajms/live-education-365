package com.application.validate;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.platform.interceptor.AuthInterceptor;
import com.platform.mvc.user.User;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/*
校验下单用户手机号是否绑定
 */
public class UserMobileBindValidator extends ApiValidator {

    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(UserMobileBindValidator.class);

    protected void validate(Controller c) {
        User user = AuthInterceptor.getCurrentUser(c.getRequest(), c.getResponse(), true);
        if(StrKit.isBlank(user.get("mobile"))) {
            addError("msg", "请先绑定手机号");
        }
    }

    @Override
    protected void handleError(Controller c) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", 420);
        map.put("data", null);
        map.put("msg", c.getAttrForStr("msg"));
        c.renderJson(map);
    }
}

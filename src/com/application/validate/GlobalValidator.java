package com.application.validate;

import com.jfinal.core.Controller;
import com.jfinal.ext.util.BaiduApi;

public class GlobalValidator extends ApiValidator {


    @Override
    protected void validate(Controller c) {
        String key = getActionKey();

        if ("/admin/global/save".equalsIgnoreCase(key) || "/admin/global/update".equalsIgnoreCase(key)) {
            if("coordinate".equalsIgnoreCase(c.getPara("code"))) {
                String[] d = BaiduApi.addressToGPS(c.getPara("name"));
                if (d == null) {
                    addError("msg", "请检查配送地址是否正确");
                    return;
                }
                c.setAttr("value", d[0] + "," + d[1]);
            } else {
                c.setAttr("value", c.getPara("value"));
            }
        }

    }

}

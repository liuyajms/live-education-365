package com.application.validate;

import com.application.model.Area;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.platform.mvc.user.User;

public class StatsParamValidator extends ApiValidator {
    @Override
    protected void validate(Controller c) {
        String key = getActionKey();
        Integer areaId = null, userId = null;
        if (StrKit.notBlank(c.getPara("areaName"))) {
            String[] ss = c.getPara("areaName").split("-");
            Area area = Area.dao.findByName(null, ss[0], ss[1]);
            if (area != null)
                areaId = area.getId();
        }
        if (StrKit.notBlank(c.getPara("username"))) {
            User user = User.dao.findByUsername(c.getPara("username"));
            if (user != null) {
                userId = user.getId();
            }
        }
        c.setAttr("areaId", areaId);
        c.setAttr("userid", userId);

        //用户下线与下单人参数选项校验
        String level = c.getPara("_query.level");
        if(level != null && !"0".equals(level) && StrKit.isBlank(c.getPara("username"))) {
            addError("msg", "请选择下单人后查询用户下线");
        }
    }

}

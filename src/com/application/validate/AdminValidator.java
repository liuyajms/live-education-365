package com.application.validate;

import com.jfinal.core.Controller;
import com.platform.interceptor.AuthInterceptor;
import com.platform.mvc.user.User;

public class AdminValidator extends ApiValidator {


    @Override
    protected void validate(Controller c) {

        User user = AuthInterceptor.getCurrentUser(c.getRequest(), c.getResponse(), true);

        if (!"SuperAdmin".equalsIgnoreCase(user.getRoleId()) && !"Admin".equalsIgnoreCase(user.getRoleId())) {
            addError("msg", "权限校验失败，请联系管理员");
            return;
        }

    }

}

package com.application.validate;

import com.application.common.OrderStatus;
import com.application.model.Order;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class OrderAdminValidator extends ApiValidator {

    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(OrderAdminValidator.class);

    protected void validate(Controller c) {
        if ("/admin/order/send".equalsIgnoreCase(getActionKey())) {
            //订单状态判断
            Integer orderId = c.getParaToInt("id");
            Order order = Order.dao.findById(orderId);
            if(order.getOrder_status() != OrderStatus.PAY_OVER && order.getOrder_status() != OrderStatus.REFUND_REJECT){
                addError("msg", "仅支持已付款订单发货");
                return;
            }
        } else if ("/admin/order/refund".equalsIgnoreCase(getActionKey())) {
            //订单状态判断
            Integer orderId = c.getParaToInt("order_id");
            Order order = Order.dao.findById(orderId);
            if(order == null){
                addError("msg", "订单不存在或已被删除");
                return;
            }
            if(order.getOrder_status() != OrderStatus.REFUND_WAIT){
                addError("msg", "仅支持已申请退款订单退款");
                return;
            }
        } else if ("/admin/order".equalsIgnoreCase(getActionKey())) {
//            全部-1，待付款0、待发货1、已完成2、已取消3、待退款4、待收货5
            Integer status = c.getParaToInt("_query.status");
            List<Integer> sList = new ArrayList<>();
            if (status != null && status != -1) {
                switch (status) {
                    case 0:
                        sList.add(OrderStatus.PAY_WAIT);
                        break;
                    case 1:
                        sList.add(OrderStatus.PAY_OVER);
                        break;
                    case 2:
                        sList.add(OrderStatus.TAKE_OVER);
                        break;
                    case 3:
                        sList.add(OrderStatus.CLOSED_CANCEL);
                        sList.add(OrderStatus.CLOSED_TIMEOUT);
                        sList.add(OrderStatus.CLOSED_OTHER);
                        break;
                    case 4:
                        sList.add(OrderStatus.REFUND_WAIT);
                        break;
                    case 5:
                        sList.add(OrderStatus.TAKE_WAIT);
                        break;
                }
            }

            c.setAttr("client_status", sList);

            /*
            时间处理
             */
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                if (StrKit.notBlank(c.getPara("_query.startDate"))) {
                    format.parse(c.getPara("_query.startDate"));
                }
                if (StrKit.notBlank(c.getPara("_query.endDate"))) {
                    format.parse(c.getPara("_query.endDate"));
                }
            } catch (Exception e) {
                addError("msg", "请输入正确的时间格式yyyy-mm-dd");
            }


        }
    }


}

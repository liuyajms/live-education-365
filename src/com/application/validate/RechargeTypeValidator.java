package com.application.validate;

import com.jfinal.core.Controller;

public class RechargeTypeValidator extends ApiValidator {
    @Override
    protected void validate(Controller c) {
        String key = getActionKey();
        if ("/admin/rechargeType/save".equalsIgnoreCase(key)) {
            validateInteger("a.gift_money", "msg", "金额为整数");
        }
    }


}

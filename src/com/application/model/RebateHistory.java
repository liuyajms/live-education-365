package com.application.model;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = RebateHistory.table_name, pkName = "id")
public class RebateHistory extends BaseModel<RebateHistory> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(RebateHistory.class);
	
	public static final RebateHistory dao = new RebateHistory().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_rebate_history";
	
	/**
	 * sqlId : app.rebateHistory.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.rebateHistory.splitPageFrom";

	private Integer id;
	private String order_no;
	private Integer order_item_id;
	private Integer rebate_id;
	private String money_1;
	private String money_2;
	private Integer userid_1;
	private Integer userid_2;
	private String roleid_1;
	private String roleid_2;
	private Integer order_userid;
	private Timestamp create_time;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setOrder_no(String order_no){
		set("order_no", order_no);
	}
	public String getOrder_no() {
		return get("order_no");
	}
	public void setOrder_item_id(Integer order_item_id){
		set("order_item_id", order_item_id);
	}
	public Integer getOrder_item_id() {
		return get("order_item_id");
	}
	public void setRebate_id(Integer rebate_id){
		set("rebate_id", rebate_id);
	}
	public Integer getRebate_id() {
		return get("rebate_id");
	}
	public void setMoney_1(String money_1){
		set("money_1", money_1);
	}
	public String getMoney_1() {
		return get("money_1");
	}
	public void setMoney_2(String money_2){
		set("money_2", money_2);
	}
	public String getMoney_2() {
		return get("money_2");
	}
	public void setUserid_1(Integer userid_1){
		set("userid_1", userid_1);
	}
	public Integer getUserid_1() {
		return get("userid_1");
	}
	public void setUserid_2(Integer userid_2){
		set("userid_2", userid_2);
	}
	public Integer getUserid_2() {
		return get("userid_2");
	}
	public void setRoleid_1(String roleid_1){
		set("roleid_1", roleid_1);
	}
	public String getRoleid_1() {
		return get("roleid_1");
	}
	public void setRoleid_2(String roleid_2){
		set("roleid_2", roleid_2);
	}
	public String getRoleid_2() {
		return get("roleid_2");
	}
	public void setOrder_userid(Integer order_userid){
		set("order_userid", order_userid);
	}
	public Integer getOrder_userid() {
		return get("order_userid");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	
}

package com.application.model;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import com.platform.mvc.user.User;

import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = OrderFlow.table_name,pkName = "id")
public class OrderFlow extends BaseModel<OrderFlow> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(OrderFlow.class);
	
	public static final OrderFlow dao = new OrderFlow().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_order_flow";
	
	/**
	 * sqlId : app.orderFlow.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.orderFlow.splitPageFrom";

	private Integer id;
	private Integer order_id;
	private Integer order_status;
	private String description;
	private Timestamp create_time;
	private Integer create_userid;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setOrder_id(Integer order_id){
		set("order_id", order_id);
	}
	public Integer getOrder_id() {
		return get("order_id");
	}
	public void setOrder_status(Integer order_status){
		set("order_status", order_status);
	}
	public Integer getOrder_status() {
		return get("order_status");
	}
	public void setDescription(String description){
		set("description", description);
	}
	public String getDescription() {
		return get("description");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}
	public void setCreate_username(String create_username){
		set("create_username", create_username);
	}
	public String getCreate_username() {
		return get("create_username");
	}

	public void create(int orderId, int status, int userid, String desc) {
		String username = "";
		if(userid != 0){
			username = User.dao.cacheGetByUserId(userid).getUsername();
		}
		OrderFlow flow = new OrderFlow();
		flow.setOrder_id(orderId);
		flow.setOrder_status(status);
		flow.setCreate_userid(userid);
		flow.setCreate_username(username);
		flow.setDescription(desc);
		flow.setCreate_time(new Timestamp(System.currentTimeMillis()));
		flow.save();
	}

	public OrderFlow findByOrderId(int orderId, int status){
		return findFirst("select * from "+ table_name + " where order_id = ? and order_status = ?", orderId, status);
	}

	public List<OrderFlow> findByOrderId(int orderId){
		return find("select * from "+ table_name + " where order_id = ? order by create_time ", orderId);
	}
}

package com.application.model;

import com.platform.annotation.Table;
import com.platform.constant.ConstantInit;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp; 

import com.jfinal.log.Log;

@SuppressWarnings("unused")
@Table(tableName = News.table_name, pkName = "id")
public class News extends BaseModel<News> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(News.class);
	
	public static final News dao = new News().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_news";
	
	/**
	 * sqlId : app.news.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.news.splitPageFrom";

	private Integer id;
	private String title;
	private String content;
	private String author;
	private String label;
	private String image;
	private Integer clicks;
	private String type;
	private Timestamp create_time;
	private Integer create_userid;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setTitle(String title){
		set("title", title);
	}
	public String getTitle() {
		return get("title");
	}
	public void setContent(String content){
		set("content", content);
	}
	public String getContent() {
		return get("content");
	}
	public void setAuthor(String author){
		set("author", author);
	}
	public String getAuthor() {
		return get("author");
	}
	public void setLabel(String label){
		set("label", label);
	}
	public String getLabel() {
		return get("label");
	}
	public void setImage(String image){
		set("image", image);
	}
	public String getImage() {
		return get("image");
	}
	public void setClicks(Integer clicks){
		set("clicks", clicks);
	}
	public Integer getClicks() {
		return get("clicks");
	}
	public void setType(String type){
		set("type", type);
	}
	public String getType() {
		return get("type");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}
	
}

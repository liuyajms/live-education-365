package com.application.model;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import com.platform.mvc.user.User;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * model
 *
 */
@SuppressWarnings("unused")
@Table(tableName = UserPoint.table_name, pkName = "id")
public class UserPoint extends BaseModel<UserPoint> {


    public static final UserPoint dao = new UserPoint();

    public static final String table_name = "t_user_point";

    public static String type_login = "1";
    public static String type_invite_gift = "2";
    public static String type_order_gift = "3";
    public static String type_invite_order = "4";//被邀请人订单
    public static String type_order_cost = "-1"; //消费
    public static String type_comment_gift = "5";

    private final String DATE_FORMAT = "yyyyMMdd";

    public UserPoint loginCheck(Integer userId) {
        String dateStr = new SimpleDateFormat(DATE_FORMAT).format(new Date());
        return findFirst("select * from t_user_point where userid = ? and type = ? and date = ?",
                userId, type_login, dateStr);
    }

    public Page<UserPoint> findList(int num, int size, int userIds) {
        String from = " from " + table_name + " where userid = ? order by id desc";
        Page<UserPoint> page = paginate(num, size, "select *", from, userIds);

        page.getList().forEach(o->{
            DictValue value = DictValue.dao.findByTypeAndCode(DictValue.type_userPoint, o.get("type"));
            o.put("type_name", value == null ? "" : value.getValue());
        });

        return page;
    }

    /**
     * 新增积分消费记录
     */
    public void create(int userid, int usePoint, String type, Integer sourceId) {
        DictValue value = DictValue.dao.findByTypeAndCode(DictValue.type_userPoint, type);
        String content = getContent(String.valueOf(usePoint), value);

        if(type == type_order_cost) usePoint = usePoint * -1;

        new UserPoint().set("userid", userid)
                .set("point", usePoint)
                .set("type", type)
                .set("source", sourceId)
                .set("create_time", new Date())
                .set("content", content)
                .set("date", new SimpleDateFormat(DATE_FORMAT).format(new Date()))
                .save();

        User.dao.updatePoint(userid, usePoint);
    }

    /*
    取消订单时，对应恢复积分信息
     */
    public void restore(int userid, int usePoint, Integer sourceId) {
        String s = "select * from " + table_name + " where userid = ? and point = ? and source = ?";
        UserPoint first = findFirst(s, userid, usePoint, sourceId);
        if (first != null) {
            first.delete();
            User.dao.updatePoint(userid, usePoint*-1);
        }
    }

    private String getContent(String point, DictValue value) {
        if(StrKit.notBlank(value.getValue())) {
            return value.getValue().replace("{point}", point);
        }
        return value.getName();
    }
}

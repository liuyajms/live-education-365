package com.application.model;

import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import com.jfinal.ext.kit.MsgException;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = Push.table_name, pkName = "id")
public class Push extends BaseModel<Push> {

	public static final int type_order = 3;
	public static final int type_product = 1;
	public static final int type_group = 2;

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log LOG = Log.getLog(Push.class);
	
	public static final Push dao = new Push().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_push";
	
	/**
	 * sqlId : app.push.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.push.splitPageFrom";

	private Integer id;
	private String title;
	private Integer type;
	private Integer mid;
	private Timestamp create_time;
	private Integer create_userid;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setTitle(String title){
		set("title", title);
	}
	public String getTitle() {
		return get("title");
	}
	public void setType(Integer type){
		set("type", type);
	}
	public Integer getType() {
		return get("type");
	}
	public void setMid(Integer mid){
		set("mid", mid);
	}
	public Integer getMid() {
		return get("mid");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}

	/*
	连接极光推送
	 */
	public static void pushToClient(Push push) {
//		LocalPushConsumer.sendPush("", push.getTitle(), push.getTitle(), "", new HashMap<>());

		String MASTER_SECRET = PropKit.get("push.secret");
		String APP_KEY = PropKit.get("push.key");
		JPushClient jpushClient = new JPushClient(MASTER_SECRET, APP_KEY);

		// For push, all you need do is to build PushPayload object.
		PushPayload payload = buildPushObject_all_all_alert(push);

		try {
			PushResult result = jpushClient.sendPush(payload);
			LOG.info("Got result - " + result);

		} catch (Exception e) {
			// Connection error, should retry later
			LOG.error("Connection error, should retry later", e);
			throw new MsgException("推送失败：" + e.getMessage());
		}
	}

	public static PushPayload buildPushObject_all_all_alert(Push push) {
		String tag = "tag_yxs";
		Integer t = push.get("target");
		if(t != null && t.intValue() != 0) {
			tag = "tag_" + push.get("target");
		}
		return PushPayload.newBuilder()
				.setPlatform(Platform.android_ios())
				.setOptions(Options.newBuilder()
						.setApnsProduction(true)
						.build())
				.setAudience(Audience.tag(tag))
				.setNotification(Notification.newBuilder()
						.addPlatformNotification(
								IosNotification.newBuilder().setAlert(push.getTitle()).setBadge(1).setSound("default")
										.addExtra("from", "JPush")
										.addExtra("type", push.getType())
										.addExtra("mid", push.getMid())
										.build())
						.addPlatformNotification(
								AndroidNotification.newBuilder().setAlert(push.getTitle())
										.addExtra("from", "JPush")
										.addExtra("type", push.getType())
										.addExtra("mid", push.getMid())
										.build()
						)
				.build()).build();
//				.setMessage(Message.newBuilder()
//				.setMsgContent(push.getTitle())
//				.addExtra("from", "JPush").addExtra("type", push.getType()).addExtra("mid", push.getId())
//				.build()).build();
	}
}

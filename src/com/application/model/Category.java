package com.application.model;

import com.jfinal.ext.kit.MsgException;
import com.jfinal.ext.util.SqlUtil;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import com.platform.tools.ToolCache;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/*
缓存分类列表数据
 */
@SuppressWarnings("unused")
@Table(tableName = Category.table_name, pkName = "id")
public class Category extends BaseModel<Category> {


    public static final Category dao = new Category();

    public static final String table_name = "t_category";

    public static final String sqlId_splitPageSelect = "app.category.splitPageSelect";
    public static final String sqlId_splitPageFrom = "app.category.splitPageFrom";
    public static final String sqlId_splitPageFrom2 = "app.category.splitPageFrom2";

    public static final Integer ROOT_ID = 0;

    private Integer id;
    private String category_no;
    private String category_name;
    private String category_icon;
    private Integer parent_id;
    private Integer category_ord;
    private Integer category_status;
    private Date create_time;
    private String create_userid;

    public void setId(Integer id) {
        set("id", id);
    }

    public Integer getId() {
        return get("id");
    }

    public void setCategory_name(String category_name) {
        set("category_name", category_name);
    }

    public String getCategory_name() {
        return get("category_name");
    }

    public void setCategory_icon(String category_icon) {
        set("category_icon", category_icon);
    }

    public String getCategory_icon() {
        return get("category_icon");
    }

    public void setParent_id(Integer parent_id) {
        set("parent_id", parent_id);
    }

    public Integer getParent_id() {
        return get("parent_id");
    }

    public void setCategory_no(String category_no) {
        set("category_no", category_no);
    }

    public String getCategory_no() {
        return get("category_no");
    }

    public void setCategory_ord(Integer category_ord) {
        set("category_ord", category_ord);
    }

    public Integer getCategory_ord() {
        return get("category_ord");
    }

    public void setCategory_status(Integer category_status) {
        set("category_status", category_status);
    }

    public Integer getCategory_status() {
        return get("category_status");
    }

    public void setCreate_time(Date create_time) {
        set("create_time", create_time);
    }

    public Date getCreate_time() {
        return get("create_time");
    }

    public void setCreate_userid(String create_userid) {
        set("create_userid", create_userid);
    }

    public String getCreate_userid() {
        return get("create_userid");
    }

    public void setCategory_path(String Category_path) {
        set("category_path", Category_path);
    }

    public String getCategory_path() {
        return get("category_path");
    }

    /**
     * 获取列表
     *
     * @param parentId null查询父类，
     * @return
     */
    public List<Category> findListByParentId(Integer parentId) {
        String sql = "select * from " + table_name + " where category_status = 1 and {query} order by category_ord ";
        if(parentId == null){
            return find(SqlUtil.getSql(sql, " parent_id is null"));
        } else {
            return find(SqlUtil.getSql(sql, " parent_id = ?"), parentId);
        }
    }

    /**
     * 查询所有分类列表(带格式）
     *
     * @return
     */
    public List<Category> findAllList() {
        String sql = "select * from " + table_name + " where category_status = 1 " +
                "and parent_id = ? order by category_ord ";
        List<Category> list = find(sql, ROOT_ID);
//        list.forEach(o -> o.put("subList", findListByParentId(o.getId())));

        return list;
    }

    /**
     * 查询所有原始分类数据
     * @return
     */
    public List<Category> findList() {
        return find("select * from " + table_name + " order by category_ord ");
    }


    public List<Category> findTopList() {
        String s = "select * from " + table_name + " where parent_id is null " +
                "and top_time is not null order by top_time";
        return find(s);
    }

    /**
     * 后台新增分类
     *
     * @param category
     * @return
     */
    public boolean create(Map<String, String> map) {
        boolean f = getCategoryModel(null, map).save();
        ToolCache.remove("categoryList");
        return f;
    }

    public boolean updateData(int id, Map<String, String> map) {
        boolean f = getCategoryModel(id, map).update();
        ToolCache.remove("categoryList");
        cacheRemove(id);
        return f;
    }

    private Category getCategoryModel(Integer id, Map<String, String> map) {
        Category cat = new Category();
        cat.setId(id);
        cat.setCategory_no(map.get("category_no"));
        cat.setCategory_icon(map.get("category_icon"));
        cat.setCategory_name(map.get("category_name"));
        if(map.get("category_ord") != null)
        cat.setCategory_ord(Integer.valueOf(map.get("category_ord")));
        cat.setCategory_status(map.get("category_status") == null ? 1 : Integer.valueOf(map.get("category_status")));
        Integer pid = StringUtils.isEmpty(map.get("parent_id")) ? ROOT_ID : Integer.parseInt(map.get("parent_id"));
        cat.setParent_id(pid);
        /*
        设置category_path
         */
        if(pid == ROOT_ID){
            cat.setCategory_path(ROOT_ID.toString());
        } else {
            Category p = findById(cat.getParent_id());
            if(p == null){
                throw new MsgException("上级分类设置错误");
            }
            cat.setCategory_path(p.getCategory_path() + "/" + cat.getParent_id());
        }
        return cat;
    }


    public Category getDetail(Integer id) {
        Category cat = findById(id);
        Integer pid = cat.getParent_id();
        if(pid != null && pid != ROOT_ID){
            cat.put("parentname", findById(pid).getCategory_name());
        }
        return cat;
    }

    /*
    删除时如果有子类,则不允许删除
     */
    public void delData(String ids) {
        for (String idS : ids.split(",")) {
            int id = Integer.parseInt(idS);
            List<Category> list = findListByParentId(id);
            if(list.size() > 0){
                throw new MsgException("请先删除子类" + list.get(0).getCategory_name());
            }
            deleteById(id);
            cacheRemove(id);
        }

    }

    public Category findByNumber(String number) {
        return findFirst("select * from " + table_name + " where category_no = ?", number);
    }
}

package com.application.model;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = RechargeType.table_name, pkName = "id")
public class RechargeType extends BaseModel<RechargeType> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(RechargeType.class);
	
	public static final RechargeType dao = new RechargeType().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_recharge_type";

	public static final String column = "`describe`,gift_money,money,id,time_start,time_end";
	
	/**
	 * sqlId : app.rechargeType.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.rechargeType.splitPageFrom";

	private Integer id;
	private BigDecimal money;
	private Integer gift_money;
	private String describe;
	private Integer ord;
	private String image;
	private Timestamp time_start;
	private Timestamp time_end;
	private Timestamp create_time;
	private Integer create_userid;
	private Timestamp delete_time;
	private Integer delete_userid;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setMoney(BigDecimal money){
		set("money", money);
	}
	public BigDecimal getMoney() {
		return get("money");
	}
	public void setGift_money(Integer gift_money){
		set("gift_money", gift_money);
	}
	public Integer getGift_money() {
		return get("gift_money");
	}
	public void setDescribe(String describe){
		set("describe", describe);
	}
	public String getDescribe() {
		return get("describe");
	}
	public void setOrd(Integer ord){
		set("ord", ord);
	}
	public Integer getOrd() {
		return get("ord");
	}
	public void setImage(String image){
		set("image", image);
	}
	public String getImage() {
		return get("image");
	}
	public void setTime_start(Timestamp time_start){
		set("time_start", time_start);
	}
	public Timestamp getTime_start() {
		return get("time_start");
	}
	public void setTime_end(Timestamp time_end){
		set("time_end", time_end);
	}
	public Timestamp getTime_end() {
		return get("time_end");
	}
	public void setCreate_time(Timestamp create_time){
		set("create_time", create_time);
	}
	public Timestamp getCreate_time() {
		return get("create_time");
	}
	public void setCreate_userid(Integer create_userid){
		set("create_userid", create_userid);
	}
	public Integer getCreate_userid() {
		return get("create_userid");
	}
	public void setDelete_time(Timestamp delete_time){
		set("delete_time", delete_time);
	}
	public Timestamp getDelete_time() {
		return get("delete_time");
	}
	public void setDelete_userid(Integer delete_userid){
		set("delete_userid", delete_userid);
	}
	public Integer getDelete_userid() {
		return get("delete_userid");
	}

	public List<RechargeType> findAvailableList() {
		String s = "SELECT "+ column +" from "+table_name +" where ?>time_start and ?< time_end" +
				" and delete_time is null order by ord, id desc";
		Date date = new Date();
		return find(s, date, date);
	}

	public void create(int userId, RechargeType type) {
		type.setCreate_time(new Timestamp(System.currentTimeMillis()));
		type.setCreate_userid(userId);
		type.save();
	}
}

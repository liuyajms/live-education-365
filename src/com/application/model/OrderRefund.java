package com.application.model;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import com.platform.mvc.user.User;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = OrderRefund.table_name, pkName = "id")
public class OrderRefund extends BaseModel<OrderRefund> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(OrderRefund.class);
	
	public static final OrderRefund dao = new OrderRefund().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_order_refund";
	
	/**
	 * sqlId : app.orderRefund.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.orderRefund.splitPageFrom";

	public static final int RESULT_WAIT = 0;
	public static final int RESULT_SUCCESS = 1;
	public static final int RESULT_REJECT = 2;


	private Integer id;
	private Integer order_id;
	private Integer refund_userid;
	private String refund_username;
	private Timestamp refund_time;
	private String refund_reason;
	private String refund_amount;
	private Timestamp answer_time;
	private Integer answer_result;
	private Integer answer_userid;

	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setOrder_id(Integer order_id){
		set("order_id", order_id);
	}
	public Integer getOrder_id() {
		return get("order_id");
	}
	public void setRefund_userid(Integer refund_userid){
		set("refund_userid", refund_userid);
	}
	public Integer getRefund_userid() {
		return get("refund_userid");
	}
	public void setRefund_username(String refund_username){
		set("refund_username", refund_username);
	}
	public String getRefund_username() {
		return get("refund_username");
	}
	public void setRefund_time(Timestamp refund_time){
		set("refund_time", refund_time);
	}
	public Timestamp getRefund_time() {
		return get("refund_time");
	}
	public void setRefund_reason(String refund_reason){
		set("refund_reason", refund_reason);
	}
	public String getRefund_reason() {
		return get("refund_reason");
	}
	public void setRefund_amount(String refund_amount){
		set("refund_amount", refund_amount);
	}
	public String getRefund_amount() {
		return get("refund_amount");
	}
	public void setAnswer_time(Timestamp answer_time){
		set("answer_time", answer_time);
	}
	public Timestamp getAnswer_time() {
		return get("answer_time");
	}
	public void setAnswer_result(Integer answer_result){
		set("answer_result", answer_result);
	}
	public Integer getAnswer_result() {
		return get("answer_result");
	}
	public void setAnswer_userid(Integer answer_userid){
		set("answer_userid", answer_userid);
	}
	public Integer getAnswer_userid() {
		return get("answer_userid");
	}

	public void create(int orderId, int userid, String amount, String reason){
		User user = User.cacheGetByUserId(userid);
		OrderRefund refund = new OrderRefund();
		refund.setRefund_amount(amount);
		refund.setRefund_reason(reason);
		refund.setRefund_time(new Timestamp(System.currentTimeMillis()));
		refund.setRefund_username(user.getUsername());
		refund.setRefund_userid(userid);
		refund.setOrder_id(orderId);
		refund.save();
	}

	public void answer(int id, int userid, int result){
		OrderRefund obj = findById(id);
		obj.setAnswer_result(result);
		obj.setAnswer_time(new Timestamp(System.currentTimeMillis()));
		obj.setAnswer_userid(userid);
		obj.update();
	}

	public OrderRefund findByOrderId(int orderId){
		return findFirst("select * from " + table_name + " where order_id = ? and answer_result = ?",
				orderId, RESULT_WAIT);
	}
}

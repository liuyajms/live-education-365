package com.application.model;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = Version.table_name, pkName = "id")
public class Version extends BaseModel<Version> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(Version.class);
	
	public static final Version dao = new Version().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_version";
	
	/**
	 * sqlId : app.version.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.version.splitPageFrom";

	private Integer id;
	private String version;
	private Integer num;
	private String description;
	private Integer forced;
	private String url;
	private String platform;
	private Timestamp createTime;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setVersion(String version){
		set("version", version);
	}
	public String getVersion() {
		return get("version");
	}
	public void setNum(Integer num){
		set("num", num);
	}
	public Integer getNum() {
		return get("num");
	}
	public void setDescription(String description){
		set("description", description);
	}
	public String getDescription() {
		return get("description");
	}
	public void setForced(Integer forced){
		set("forced", forced);
	}
	public Integer getForced() {
		return get("forced");
	}
	public void setUrl(String url){
		set("url", url);
	}
	public String getUrl() {
		return get("url");
	}
	public void setPlatform(String platform){
		set("platform", platform);
	}
	public String getPlatform() {
		return get("platform");
	}
	public void setCreateTime(Timestamp createTime){
		set("createTime", createTime);
	}
	public Timestamp getCreateTime() {
		return get("createTime");
	}
	
}

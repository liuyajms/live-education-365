package com.application.model;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = JobLog.table_name, pkName = "id")
public class JobLog extends BaseModel<JobLog> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(JobLog.class);
	
	public static final JobLog dao = new JobLog().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_job_log";
	
	/**
	 * sqlId : app.jobLog.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.jobLog.splitPageFrom";

	private Integer id;
	private String jobname;
	private String jobclass;
	private Timestamp createat;
	private Integer total;
	private Integer failed;
	private String errormsg;
	private String errorids;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setJobname(String jobname){
		set("jobname", jobname);
	}
	public String getJobname() {
		return get("jobname");
	}
	public void setJobclass(String jobclass){
		set("jobclass", jobclass);
	}
	public String getJobclass() {
		return get("jobclass");
	}
	public void setCreateat(Timestamp createat){
		set("createat", createat);
	}
	public Timestamp getCreateat() {
		return get("createat");
	}
	public void setTotal(Integer total){
		set("total", total);
	}
	public Integer getTotal() {
		return get("total");
	}
	public void setFailed(Integer failed){
		set("failed", failed);
	}
	public Integer getFailed() {
		return get("failed");
	}
	public void setErrormsg(String errormsg){
		set("errormsg", errormsg);
	}
	public String getErrormsg() {
		return get("errormsg");
	}
	public void setErrorids(String errorids){
		set("errorids", errorids);
	}
	public String getErrorids() {
		return get("errorids");
	}
	
}

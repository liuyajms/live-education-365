package com.application.model;

import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import com.platform.tools.ToolCache;

import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = Global.table_name, pkName = "id")
public class Global extends BaseModel<Global> {

    public Global findByCode(String s) {
        return findFirst("select * from " + table_name + " where code = ?", s);
    }

    public enum Code {
        defaultPassword, coordinate
        ,order_minPrice, admin_mobile, order_giftPoint
        ,qrcode_xy, qrcode_size,

        requiredDrawMoney,
        gift_login,gift_vipLogin,gift_share,gift_expireDay
        ,gift_loginPlay, gift_vipLoginPlay
        ,music_pic//每日音频封面图
        ,init_wxConfig //微信jssdk配置
        ,service_phone
        ,live_accessKey,live_accessSecret,live_appName, live_playDomain, live_playSecret, live_pushDomain, live_pushSecret
        //分享设置
        ,share_logo, share_title, share_desc
        //邮件发送配置
        ,mail_userName,mail_authCode, mail_host
        //报表定时时间配置
        ,report_cron
        //默认用户提醒事项
        ,default_tips
        //讲师收益分成配置(收益权重、点赞权重、播放次数权重、课程数权重)
        ,income_percent,income_zan, income_click, income_num
        //新用户赠送解锁券、点播券
        ,newUser_giftUnlock, newUser_giftPlay
        ,gift_sharePlay, gift_shareLimit//每天分享赠送上限
    }

    public static final Global dao = new Global();

    public static final String table_name = "t_global";

    public static final String sqlId_splitPageFrom = "com.application.global.splitPageFrom";

    public static Global cacheGetByCode(Code globalCode){
        String code = globalCode.toString();
        String key = table_name + code;
        Global g = ToolCache.get(key);
        if(g == null){
            g = Global.dao.findFirst("select * from " + table_name + " where code = ?", code);
            if(g != null){
                ToolCache.set(key, g);
            } else {
                return new Global();
            }
        }
        return g;
    }

    public void cacheRemoveByCode(Code code){
        String key = getTableName() + code;
        ToolCache.remove(key);
    }

    //----------------------------

    private int id;
    private String code;
    private String name;
    private String value;
    private Timestamp updateTime;
    private Integer updateUserid;

    public void setUpdateTime(Timestamp updateTime) {
        set("update_time", updateTime);
    }

    public void setUpdateUserid(Integer updateUserid) {
        set("update_userid", updateUserid);
    }

    public void setId(int id) {
        set("id", id);
    }

    public void setCode(String code) {
        set("code", code);
    }

    public void setName(String name) {
        set("name", name);
    }

    public void setValue(String value) {
        set("value", value);
    }

    public int getId() {
        return get("id");
    }

    public String getCode() {
        return get("code");
    }

    public String getName() {
        return get("name");
    }

    public String getValue() {
        return get("value");
    }

    public Global create(int cUserId, String code, String name, String value) {
        Global a = new Global();
        a.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        a.setUpdateUserid(cUserId);
        a.setName(name);
        a.setCode(code);
        a.setValue(value);
        return a;
    }
}

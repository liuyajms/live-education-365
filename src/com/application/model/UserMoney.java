package com.application.model;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("unused")
@Table(tableName = UserMoney.table_name, pkName = "id")
public class UserMoney extends BaseModel<UserMoney> {

    public static final UserMoney dao = new UserMoney();

    public static final String table_name = "t_user_money";

    public static final String sqlId_splitPageSelect = "app.userMoney.splitPageSelect";
    public static final String sqlId_splitPageFrom = "app.userMoney.splitPageFrom";


    /*
    充值charge,邀请好友购买会员奖励rebate_vip，邀请购买课程rebate_course，邀请购买点播券rebate_coupon
     */
    public static final String TYPE_CHARGE = "charge";
    public static final String TYPE_REBATE = "rebate";//返利AbstractUserMoney
    public static final String TYPE_REFUND = "refund";
    public static final String TYPE_Withdraw = "withdraw";

    private Integer id;
    private Integer userid;
    private String money;
    private String gift;
    private String type;
    private String order_no;
    private Timestamp create_time;
    private Integer create_userid;
    private String content;

    public void setId(Integer id){
        set("id", id);
    }
    public Integer getId() {
        return get("id");
    }
    public void setUserid(Integer userid){
        set("userid", userid);
    }
    public Integer getUserid() {
        return get("userid");
    }
    public void setMoney(BigDecimal money){
        set("money", money);
    }
    public BigDecimal getMoney() {
        return get("money");
    }
    public void setGift(String gift){
        set("gift", gift);
    }
    public String getGift() {
        return get("gift");
    }
    public void setType(String type){
        set("type", type);
    }
    public String getType() {
        return get("type");
    }
    public void setOrder_no(String order_no){
        set("order_no", order_no);
    }
    public String getOrder_no() {
        return get("order_no");
    }
    public void setCreate_time(Timestamp create_time){
        set("create_time", create_time);
    }
    public Timestamp getCreate_time() {
        return get("create_time");
    }
    public void setCreate_userid(Integer create_userid){
        set("create_userid", create_userid);
    }
    public Integer getCreate_userid() {
        return get("create_userid");
    }
    public void setContent(String content){
        set("content", content);
    }
    public String getContent() {
        return get("content");
    }


    @Deprecated
    public Page<UserMoney> findList(int num, int size, Integer userid) {
        String from = " from " + table_name + " where userid = ? order by id desc";
        Page<UserMoney> page = paginate(num, size, "select *", from, userid);

        page.getList().forEach(o->{
            DictValue value = DictValue.dao.findByTypeAndCode(DictValue.type_userMoney, o.get("type"));
            String val = StrKit.notBlank(o.getContent()) ? o.getContent() : (value == null ? "" : value.getValue());
            o.put("type_name", val);
        });

        return page;
    }

    public void deleteByOrderNo(String orderNo) {
        Db.update("delete from " + table_name + " where order_no = ?", orderNo);
    }

    public Object findTotalAward(int userId) {
        String s = "select COALESCE(sum(money),0) as total from t_user_money where userid = ? and type= ?";
        return findFirst(s, userId, TYPE_REBATE).get("total");
    }
}

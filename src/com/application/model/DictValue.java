package com.application.model;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import com.platform.tools.ToolCache;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * model
 *
 * @author 董华健  dongcb678@163.com
 */
@SuppressWarnings("unused")
@Table(tableName = DictValue.table_name, pkName = "id")
public class DictValue extends BaseModel<DictValue> {

    private static final long serialVersionUID = 6761767368352810428L;

    private static final Log log = Log.getLog(DictValue.class);

    public static final DictValue dao = new DictValue().dao();

    /**
     * 表名称
     */
    public static final String table_name = "dict_value";

    private int id;
    private String code;
    private String type;
    private String value;
    private String ext;
    private String name;

    public String getName() {
        return get("name");
    }

    public void setName(String name) {
        set("name", name);
    }

    public int getId() {
        return get("id");
    }

    public void setId(int id) {
        set("id", id);
    }

    public String getCode() {
        return get("code");
    }

    public void setCode(String code) {
        set("code", code);
    }

    public String getType() {
        return get("type");
    }

    public void setType(String type) {
        set("type", type);
    }

    public String getValue() {
        return get("value");
    }

    public void setValue(String value) {
        set("value", value);
    }

    public String getExt() {
        return get("ext");
    }

    public void setExt(String ext) {
        set("ext", ext);
    }

    public List<DictValue> findByType(String type) {
        return find("select * from dict_value where type = ? order by ord",type);
    }


    public static final String type_news = "news.type";
    public static final String type_userLevel = "user.level";
    public static final String type_userHobby = "user.hobby";
    public static final String type_deliver_time = "deliver.time";
    public static final String type_deliver = "deliver";
    public static final String type_redPacket = "red.packet";
    public static final String type_defaultpass = "user.defaultpass";
    public static String type_userPoint = "userpoint.type";
    public static String type_userMoney = "usermoney.type";

    public static String type_message = "message.type";
    public static String type_apply = "apply.type";


    public DictValue getModelByMap(Integer ids, Map<String, String> paramMap) {
        Set<String> attrs = getTable().getColumnTypeMap().keySet();

        DictValue dict = new DictValue();
        paramMap.keySet().stream().filter(attr -> attrs.contains(attr)).forEach(attr -> {
            dict.set(attr, paramMap.get(attr));
        });

        dict.set("id", ids == null ? null : ids);

        if(attrs.contains("status") && StrKit.notBlank(paramMap.get("status"))){
            dict.set("status", "1".equals(paramMap.get("status")) ? 1 : 0);
        }

        return dict;
    }

    public DictValue findByTypeAndCode(String type, String code) {
        String key = type + "_" + code;
        if (ToolCache.get(key) != null) {
            return ToolCache.get(key);
        }
        DictValue v = findFirst("select * from dict_value where type = ? and code = ?",type, code);
        if(v != null) {
            ToolCache.set(key, 3600, v);
            return v;
        }
        return new DictValue();
    }
}

package com.application.model;

import com.jfinal.log.Log;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unused")
@Table(tableName = VipChargeType.table_name, pkName = "id")
public class VipChargeType extends BaseModel<VipChargeType> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static final Log log = Log.getLog(VipChargeType.class);
	
	public static final VipChargeType dao = new VipChargeType().dao();

	/**
	 * 表名称
	 */
	public static final String table_name = "t_vip_charge_type";

	public static final String TYPE_YEAR = "year";
	public static final String TYPE_MONTH = "month";
	public static final String TYPE_DAY = "day";

	/**
	 * sqlId : app.vipChargeType.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "app.vipChargeType.splitPageFrom";

	private Integer id;
	private String desc;
	private BigDecimal require_money;
	private String expire_type;
	private Integer expire_num;
	private Timestamp delete_time;
	
	public void setId(Integer id){
		set("id", id);
	}
	public Integer getId() {
		return get("id");
	}
	public void setDesc(String desc){
		set("desc", desc);
	}
	public String getDesc() {
		return get("desc");
	}
	public void setRequire_money(BigDecimal require_money){
		set("require_money", require_money);
	}
	public BigDecimal getRequire_money() {
		return get("require_money");
	}
	public void setExpire_type(String expire_type){
		set("expire_type", expire_type);
	}
	public String getExpire_type() {
		return get("expire_type");
	}
	public void setExpire_num(Integer expire_num){
		set("expire_num", expire_num);
	}
	public Integer getExpire_num() {
		return get("expire_num");
	}
	public void setDelete_time(Timestamp delete_time){
		set("delete_time", delete_time);
	}
	public Timestamp getDelete_time() {
		return get("delete_time");
	}

	public List<VipChargeType> findAvailableList() {
		String s = "SELECT * from "+table_name +" where delete_time is null order by id desc";
		return find(s);
	}

	public void create(int userId, VipChargeType type) {
		type.save();
	}
}

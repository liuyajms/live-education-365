package com.application.model;

import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Table;
import com.platform.dto.SplitPage;
import com.platform.mvc.base.BaseModel;

import java.util.Date;

@SuppressWarnings("unused")
@Table(tableName = MessageRead.table_name, pkName = "id")
public class MessageRead extends BaseModel<MessageRead> {


    public static final MessageRead dao = new MessageRead();

    public static final String table_name = "t_message_read";


    public Page<MessageRead> getPageData(int number, int size, int msgId) {
        SplitPage splitPage = new SplitPage();
        String sel = "select a.*";
        String fro = " from " + table_name + " a where message_id = ? order by id desc";
        Page<MessageRead> page = paginate(number, size, sel, fro, msgId);
        return page;
    }

    public boolean isRead(int userIds, int msgId) {
        return find("select 1 from " +table_name + " where message_id = ? and userid = ?", msgId, userIds).size() > 0;
    }

    public void create(int userIds, int msgId) {
        new MessageRead().set("userid", userIds).set("message_id", msgId).set("read_time", new Date()).save();
    }
}

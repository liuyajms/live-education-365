package com.application.util;

import com.jfinal.kit.PropKit;
import com.platform.mvc.base.BaseService;
import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SendEmailUtil {
    public static void main(String[] args) throws GeneralSecurityException, MessagingException {
        String s = "<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";
        s+="这是一段文本<img src='http://39.108.9.240/education/files/upload/2a25626a6fb04987b6349779f19c4ecc.jpg'>";
        s+= "</html>";


        Map<String, Object> map = new HashMap<>();
        String html = BaseService.getSqlByBeetl("app.report.sendMailTemplate", map);

        send("带图片和附件的邮件", html, "1552028114@qq.com");
    }

    private static final String mailHost = "smtp.qq.com";
    private static final String userName = PropKit.get("mail.userName");
    private static final String authCode = PropKit.get("mail.authCode");


    public static void send(String mailHost, String authCode, String userName, String subject, String content, String toUser) throws GeneralSecurityException, MessagingException {
        Properties prop = new Properties();
        prop.setProperty("mail.host", mailHost);  //设置QQ邮件服务器
        prop.setProperty("mail.transport.protocol", "smtp"); // 邮件发送协议
        prop.setProperty("mail.smtp.auth", "true"); // 需要验证用户名密码

        // QQ邮箱设置SSL加密
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        prop.put("mail.smtp.ssl.enable", "true");
        prop.put("mail.smtp.ssl.socketFactory", sf);

        //1、创建定义整个应用程序所需的环境信息的 Session 对象
        Session session = Session.getDefaultInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                //传入发件人的姓名和授权码
//                return new PasswordAuthentication("619046217@qq.com","16位授权码");
                return new PasswordAuthentication(userName, authCode);
            }
        });

        MimeMessage mimeMessage = new MimeMessage(session);

        mimeMessage.setFrom(new InternetAddress(userName));
        mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(toUser));
        mimeMessage.setSubject(subject);

        // MiniMultipart类是一个容器类，包含MimeBodyPart类型的对象
        Multipart mainPart = new MimeMultipart();
        // 创建一个包含HTML内容的MimeBodyPart
        BodyPart html = new MimeBodyPart();
        // 设置HTML内容
        html.setContent(content, "text/html; charset=UTF-8");
        mainPart.addBodyPart(html);
        // 将MiniMultipart对象设置为邮件内容
        mimeMessage.setContent(mainPart);
        // 发送邮件
        Transport.send(mimeMessage);
    }

    public static void send(String subject, String content, String toUser) throws GeneralSecurityException, MessagingException {
        send(mailHost, authCode, userName, subject, content, toUser);
    }

}

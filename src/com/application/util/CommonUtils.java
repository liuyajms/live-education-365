package com.application.util;

import com.jfinal.kit.StrKit;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class CommonUtils {
    public static String getImagesStr(String images) {
        List<String> imgList = new ArrayList<>();
        if(StrKit.notBlank(images)) {
            String[] split = images.split(",");
            for (String s : split) {
                if(StrKit.notBlank(s)) {
                    imgList.add(s);
                }
            }
        }
        return StringUtils.join(imgList, ",");
    }
}

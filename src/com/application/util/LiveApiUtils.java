package com.application.util;

import com.aliyun.live20161101.models.DescribeLiveStreamsOnlineListRequest;
import com.aliyun.live20161101.models.DescribeLiveStreamsOnlineListResponse;
import com.aliyun.teaopenapi.models.Config;
import com.application.model.Global;
import com.jfinal.log.Log;

public class LiveApiUtils {

    private static final Log log = Log.getLog(LiveApiUtils.class);

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.live20161101.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                .setAccessKeyId(accessKeyId)
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "live.aliyuncs.com";
        return new com.aliyun.live20161101.Client(config);
    }

    public static int getLiveStreamNum(String streamName) {
        String key = Global.cacheGetByCode(Global.Code.live_accessKey).getValue();
        String secret = Global.cacheGetByCode(Global.Code.live_accessSecret).getValue();
        String liveDomain = Global.cacheGetByCode(Global.Code.live_playDomain).getValue();
        String appName = Global.cacheGetByCode(Global.Code.live_appName).getValue();
        com.aliyun.live20161101.Client client = null;
        try {
            client = LiveApiUtils.createClient(key, secret);
            DescribeLiveStreamsOnlineListRequest describeLiveStreamsOnlineListRequest = new DescribeLiveStreamsOnlineListRequest()
                    .setDomainName(liveDomain)
                    .setAppName(appName)
                    .setStreamName(streamName);
            DescribeLiveStreamsOnlineListResponse desc = client.describeLiveStreamsOnlineList(describeLiveStreamsOnlineListRequest);
            log.info("######query ali live streamNum:" + desc.getBody().getTotalNum());
            return desc.getBody().getTotalNum();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void main(String[] args_) throws Exception {
        String key = "LTAI4FzuBcsuZMuix69LXeKG";
        String secret = "agaCUY2rU7ipFH7RpGYDyv8i54DqKj";
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.live20161101.Client client = LiveApiUtils.createClient(key, secret);
        DescribeLiveStreamsOnlineListRequest describeLiveStreamsOnlineListRequest = new DescribeLiveStreamsOnlineListRequest()
                .setDomainName("live.wish360.com.cn")
                .setAppName("365")
                .setStreamName("234");
        DescribeLiveStreamsOnlineListResponse desc = client.describeLiveStreamsOnlineList(describeLiveStreamsOnlineListRequest);
        System.out.println(desc.getBody().getTotalNum());
    }
}

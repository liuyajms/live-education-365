package com.application.admin;

import com.application.model.VipChargeType;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/admin/vipChargeType")
public class VipChargeTypeController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(VipChargeTypeController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, VipChargeType.sqlId_splitPageSelect, VipChargeType.sqlId_splitPageFrom);
		renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	public void save() {
		VipChargeType type = getModel(VipChargeType.class, "a", true);
		VipChargeType.dao.create(getCUserId(), type);
		renderOk("refresh", "添加成功", null);
	}
	
	/**
	 * 更新
	 */
	public void update() {
		int money = getParaToInt("money");
		renderOk("refresh", "修改成功", null);
	}

	/**
	 * 查看
	 */
	public void view() {
		VipChargeType obj = VipChargeType.dao.findById(getPara());
		success(obj);
	}
	
	/**
	 * 删除
	 */
	public void delete() {
		VipChargeType.dao.falseDelete(getPara("checkbox"), getCUserId());
		renderOk("refresh", "删除成功", null);
	}
	
}

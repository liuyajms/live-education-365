package com.application.admin;

import com.application.model.Message;
import com.application.validate.MessageValidator;
import com.jfinal.aop.Before;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * 系统通知
 */
@Controller("/admin/message")
public class MessageController extends BaseController {

    public void index() {
        paging(splitPage, "com.application.message.splitPageSelect", "com.application.message.splitPageFrom");
        renderOk(splitPage);
    }

    @Before(MessageValidator.class)
    public void save(){
        boolean f = Message.dao.createNotice(getPara("type"), getPara("title"), getPara("content"),
                getCUserId(), getParaToInt("to_userid"));
        if (f) {
            renderOk("refresh", "添加成功", null);
        } else {
            renderErr(400, "", "保存失败");
        }
    }

    /**
     * 删除
     */
    public void delete() {
        Message.dao.deleteByIds(ids == null ? getPara("checkbox") : ids);
        renderOk("refresh", "删除成功", null);
    }
}

package com.application.admin;

import com.application.model.Version;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.sql.Timestamp;

/**
 *
 */
@Controller("/admin/version")
public class VersionController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(VersionController.class);


	/**
	 * 列表
	 */
	public void index() {
		splitPage.setOrderColunm("id");
		splitPage.setOrderMode("desc");
		paging(splitPage, Version.sqlId_splitPageSelect, Version.sqlId_splitPageFrom);
		renderOk(splitPage);
	}

	/**
	 * 保存
	 */
	public void save() {
		Version version = super.getModel(Version.class, "a");
		version.setPlatform("android");
		version.setCreateTime(new Timestamp(System.currentTimeMillis()));
		version.save();
		renderOk("refresh", "添加成功", null);
	}


	public void update(){
		Version version = super.getModel(Version.class, "a");
		version.update();
		renderOk("refresh", "成功", null);
	}

	/**
	 * 删除
	 */
	public void delete() {
		Version.dao.deleteByIds(getPara() == null ? getPara("checkbox") : getPara());
		renderOk("refresh", "成功", null);
	}

	public void view(){
		Version version = Version.dao.findById(getPara());
		success(version);
	}

}

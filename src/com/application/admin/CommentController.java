package com.application.admin;

import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.interceptor.ReadTimeInterceptor;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/admin/comment")
public class CommentController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CommentController.class);


	/**
	 * 列表
	 */
	@Before(ReadTimeInterceptor.class)
	public void index() {
		if (splitPage.getOrderColunm() == null) {
			splitPage.setOrderColunm("a.create_time");
			splitPage.setOrderMode("desc");
		}
		paging(splitPage, "app.productComment.splitPageSelectAll", "app.productComment.splitPageFromAll");
		renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	public void save() {
//		ProductComment.dao.create(getCUserId(), super.getParamMap());
//		renderOk("refresh", "添加成功", null);
	}

	/**
	 * 删除
	 */
	public void delete() {
		renderOk("refresh", "成功", null);
	}

}

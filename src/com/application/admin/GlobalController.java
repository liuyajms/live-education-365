package com.application.admin;

import com.application.model.Global;
import com.application.validate.GlobalValidator;
import com.application.validate.SuperAdminValidator;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.base.BaseModel;

/**
 *
 */
@Controller("/admin/global")
public class GlobalController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(GlobalController.class);

	/**
	 * 列表
	 */
	public void index() {
	    splitPage.getQueryParam().put("show_type", 1);
		paging(splitPage, BaseModel.sqlId_splitPageSelect, Global.sqlId_splitPageFrom);
		renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(GlobalValidator.class)
	public void save() {
		Global a = Global.dao.create(super.getCUserId(), getPara("code"),
				getPara("name"), getAttrForStr("value"));
		a.save();
		renderOk("refresh", "添加成功", null);
	}
	
	/**
	 * 更新
	 */
	@Before({GlobalValidator.class, SuperAdminValidator.class})
	public void update() {
		Global a = Global.dao.create(super.getCUserId(), getPara("code"),
				getPara("name"), getAttrForStr("value"));
		a.setId(getParaToInt("id"));
		a.update();
		Global.dao.cacheRemoveByCode(Global.Code.valueOf(getPara("code")));
		renderOk("refresh", "修改成功", null);
	}

	/**
	 * 查看
	 */
	public void view() {
		Global group = Global.dao.findById(getPara());
		success(group);
	}
	
	/**
	 * 删除
	 */
	public void delete() {

	}
	
}

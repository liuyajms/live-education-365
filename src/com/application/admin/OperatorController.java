package com.application.admin;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.operator.Operator;
import com.platform.mvc.operator.OperatorService;
import com.platform.mvc.roleoperator.RoleOperatorService;

import java.util.Map;

/**
 * 管理
 */
@Controller("/admin/operator")
public class OperatorController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(OperatorController.class);

    OperatorService operatorService;
    /**
	 */
	public void index() {
	    if(StrKit.isBlank(splitPage.getOrderColunm())) {
            splitPage.setOrderColunm("modulenames asc, names");
            splitPage.setOrderMode("asc");
        }
        paging(splitPage, "platform.operator.splitPageSelect", "platform.operator.splitPageFrom");
        renderOk(splitPage);
	}



    /*
    @param {parentids}
     */
    public void save() {
        Map<String, String> param = super.getParamMap();
        param.put("enctype", "0");
        param.put("syslog", "0");
        //param.put("method", "2");
        param.put("referer", "0");
        param.put("csrf", "0");
        param.put("pvtype", "0");
        param.put("ispv", "0");
//        param.put("privilegess", "0");
        param.put("ipblack", "0");
        param.put("formtoken", "0");
//        param.put("splitpage", "0");
        param.put("rowfilter", "0");
        param.put("onemany", "0");
        Operator operator = Operator.dao.getModelByMap(null, param);
        if (operator.save()) {
            //自动给管理员授权
            if(operator.getPrivilegess().equalsIgnoreCase("1")){
                new RoleOperatorService().add("Admin", operator.getIds());
                new RoleOperatorService().add("SuperAdmin", operator.getIds());
                Operator.cacheAdd(operator.getIds());
            }
            renderOk("refresh", "添加成功", null);
        } else {
            renderErr(400, "", "添加失败");
        }
    }


    public void update() {
        Operator operator = Operator.dao.getModelByMap(getPara(), super.getParamMap());
        if (operator.update()) {
            Operator.cacheRemove(operator.getIds());
            renderOk("refresh", "修改成功", null);
        } else {
            renderErr(400, "", "修改失败");
        }
    }

    /**
     * 删除
     */
    public void delete() {
        String ids = getPara() == null ? getPara("ids") : getPara();
        //清除缓存
        for (String s : ids.split(",")) {
            Operator.cacheRemove(s);
        }
        operatorService.baseDelete(Operator.table_name, ids);
        renderOk("refresh", "删除成功", null);
    }

    public void view(){
        Operator operator = Operator.dao.findFirst("select * from pt_operator where ids = ?", getPara());
        renderOk(operator);
    }
}



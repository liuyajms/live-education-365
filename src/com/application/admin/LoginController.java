package com.application.admin;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.platform.annotation.Controller;
import com.platform.constant.ConstantWebContext;
import com.platform.interceptor.AuthInterceptor;
import com.platform.interceptor.LoginInterceptor;
import com.platform.interceptor.ParamPkgInterceptor;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.login.LoginValidator;
import com.platform.mvc.user.User;
import com.platform.tools.ToolWeb;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * 登陆注销
 */
@Controller("/admin/login")
@Clear({LoginInterceptor.class, ParamPkgInterceptor.class})
public class LoginController extends BaseController {

    public static String prefix = "/upload/user/";


    @Before(LoginValidator.class)
    public void vali() {
        String remember = getPara("remember");

        if (!authCode()) {
            renderErr(400, "", "验证码错误");
            return;
        }

        boolean autoLogin = false;
        if (null != remember && (remember.equals("1") || remember.equals("on"))) {
            autoLogin = true;
        }

        User user = getAttr("user");

        if(user.getRoleId() != null && "normal".equals(user.getRoleId().toLowerCase())){
            renderErr(403, "", "普通用户禁止登陆");
            return;
        }
        AuthInterceptor.setCurrentUser(getRequest(), getResponse(), user, autoLogin);
        user.put("authmark", getAttr("authmark"));
        User.updateInfo(user);
        User.getInfo(user);

        renderOk("index.html", "登录成功", user);

    }


    /**
     * @title 注销登陆
     * @respBody {
     * "code": 200,
     * "data": null,
     * "description": "成功退出登录！"
     * }
     */
    public void logout() {
        ToolWeb.addCookie(getRequest(), getResponse(), "", null, true, ConstantWebContext.cookie_authmark, null, 0);

        renderOk("login.html", "成功", null);
    }

}

package com.application.admin;

import com.application.model.Push;
import com.application.validate.PushValidator;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

/**
 * 推送 管理
 */
@Controller("/admin/push")
public class PushController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(PushController.class);
	
	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, BaseModel.sqlId_splitPageSelect, Push.sqlId_splitPageFrom);
		renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(PushValidator.class)
	public void save() {
		Push push = getModel(Push.class, "a");
		push.setCreate_time(new Timestamp(System.currentTimeMillis()));
		push.setCreate_userid(getCUserId());
		if(push.getType() == 1){//product
//			Product product = Product.dao.cacheGetById(push.getMid());
//			push.set("mname", product.getProduct_name());
		}
		push.save();
		Push.pushToClient(push);
		renderOk("refresh", "添加成功", null);
	}
	

	/**
	 * 查看
	 */
	public void view() {
		Push push = Push.dao.findById(getPara());
		setAttr("push", push);
		render("/app/push/view.html");
	}
	
	/**
	 * 删除
	 */
	public void delete() {
		Push.dao.deleteByIds(getPara() == null ? getPara("checkbox") : getPara());
		renderOk("refresh", "删除成功", null);
	}
	
}

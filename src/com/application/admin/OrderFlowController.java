package com.application.admin;

import com.application.model.OrderRefund;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/admin/orderFlow")
public class OrderFlowController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(OrderFlowController.class);
	

	/**
	 * 列表
	 */
	public void index() {
//		paging(splitPage, BaseModel.sqlId_splitPageSelect, OrderFlow.sqlId_splitPageFrom);
//		renderOk(splitPage);
	}
	
	/**
	 * 查看订单退款申请信息
	 */
	public void viewRefund() {
		OrderRefund refund = OrderRefund.dao.findByOrderId(getParaToInt());
		success(refund);
	}

	/**
	 * 查看
	 */
	public void view() {
//		Tag searchHot = Tag.dao.findById(getPara());
//		success(searchHot);
	}
	
	/**
	 * 删除
	 */
	public void delete() {
//		Tag.dao.deleteByIds(getPara() == null ? ids : getPara());
//		renderOk("refresh", "成功", null);
	}
	
}

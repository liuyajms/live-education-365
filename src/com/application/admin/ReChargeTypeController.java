package com.application.admin;

import com.application.model.RechargeType;
import com.application.validate.RechargeTypeValidator;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/admin/rechargeType")
public class ReChargeTypeController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(ReChargeTypeController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, RechargeType.sqlId_splitPageSelect, RechargeType.sqlId_splitPageFrom);
		renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(RechargeTypeValidator.class)
	public void save() {
		RechargeType type = getModel(RechargeType.class, "a", true);
		RechargeType.dao.create(getCUserId(), type);
		renderOk("refresh", "添加成功", null);
	}
	
	/**
	 * 更新
	 */
	public void update() {
		int money = getParaToInt("money");
		renderOk("refresh", "修改成功", null);
	}

	/**
	 * 查看
	 */
	public void view() {
		RechargeType obj = RechargeType.dao.findById(getPara());
		success(obj);
	}
	
	/**
	 * 删除
	 */
	public void delete() {
		RechargeType.dao.falseDelete(getPara("checkbox"), getCUserId());
		renderOk("refresh", "删除成功", null);
	}
	
}

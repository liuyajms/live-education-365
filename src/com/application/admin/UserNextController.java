package com.application.admin;

import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.user.User;


/**
 * 用户下线管理
 */
@Controller("/admin/userNext")
public class UserNextController extends BaseController {


    /**
     * 不允许添加自己为下线
     * @param userid
     * @param nextUsername
     * @param force default 0
     */
    public void save(){
        int userid = getParaToInt("userid");
        User nextUser = User.dao.findByUsername(getPara("nextUsername"));
        int force = getParaToInt("force", 0);
        if(nextUser.get("referee_userid") != null && force == 0) {
            renderErr(400, "", "该用户已有上线，请确认是否替换？");
            return;
        }
        if(nextUser.getId().intValue() == userid) {
            renderErr(400, "", "不允许添加自己为下线");
            return;
        }
        nextUser.set("referee_userid", userid).update();
        renderOk("refresh", "添加成功", null);
    }


    /**
     * 删除
     */
    public void delete() {
        String ids = getPara() == null ? getPara("checkbox") : getPara();
        for (String s : ids.split(",")) {
            User user = User.dao.findById(s);
            user.set("referee_userid", null).update();
        }
        renderOk("refresh", "删除成功", null);
    }


    /**
     * 批量转移至其他用户
     * @param nextUsername 上线用户
     * @param userid 需要转移的用户id
     */
    public void transfer(){
        User pUser = User.dao.findByUsername(getPara("nextUsername"));
        String ids = getPara("userid");
        for (String s : ids.split(",")) {
            User user = User.dao.findById(s);
            if(user.getId().intValue() != pUser.getId().intValue()) {
                user.set("referee_userid", pUser.getId()).update();
            }
        }
        renderOk("refresh", "转移成功", null);
    }
}

package com.application.admin;

import com.application.common.OrderStatus;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.user.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 后台仪表盘
 */
@Controller("/admin/dashboard")
public class DashboardController extends BaseController {


    public void index() {
        Map<String, Integer> map = new HashMap<>();
        User user = super.getCUser();
        String day = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        if ("Seller".equals(user.getRoleId())) {
//            String sql = "SELECT count(*) from orders a join site b on a.site_id = b.id where b.manage_userid = ? and" +
//                    " DATE_FORMAT(b.create_time, '%Y-%m-%d') = ?";
//            long num = Db.queryLong(sql, user.getId(), day);
//            map.put("ordersNum", num);
        } else {

        }

        String sql = "select count(1) from t_order where order_status = ? and delete_time is null";
        int sendNum = Db.queryLong(sql, OrderStatus.PAY_OVER).intValue();

//        sql = "select count(1) from t_product a left join where stock < 10 and status != ?";
        sql = "select count(1) from (select ifnull(sum(sku_stock),0) as stock from t_product a left join t_product_sku b \n" +
                "on a.id = b.product_id where a.status != ? and sku_status = 1 \n" +
                "group by a.id) t\n" +
                "where stock < 10 ";
        /*int stockNum = Db.queryLong(sql, Product.STATUS_DOWN).intValue();

        int unreadCommentNum = ProductComment.dao.findUnreadCommentNum();
        int unreadFeedbackNum = Feedback.dao.findUnreadNum();

        map.put("waitForSendNum", sendNum);
        map.put("lowStockNum", stockNum);
        map.put("unreadCommentNum", unreadCommentNum);
        map.put("unreadFeedbackNum", unreadFeedbackNum);*/

        renderOk(map);
    }


}

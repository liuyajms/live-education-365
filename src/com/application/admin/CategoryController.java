package com.application.admin;

import com.application.model.Category;
import com.application.validate.CategoryValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.tree.TreeUtil;
import com.jfinal.ext.tree.ZtreeNode;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.ArrayList;
import java.util.List;

/**
 * 分类管理
 */
@Controller("/admin/category")
public class CategoryController extends BaseController {

    @SuppressWarnings("unused")
    private static final Log log = Log.getLog(CategoryController.class);

    /*
    列表,分级次,param: parentId
     */
    public void index(){
        paging(splitPage, Category.sqlId_splitPageSelect, Category.sqlId_splitPageFrom2);
        renderOk(splitPage);
    }

    /*
    表格数据
     */
    public void treeData() {
        List<Category> list = Category.dao.findList();

        List<ZtreeNode> nodeList = new ArrayList<>();
        ZtreeNode node;
        for (Category o : list) {
            String pid = o.get("parent_id") == null ? null : o.get("parent_id").toString();
            node = new ZtreeNode();
            node.setId(o.getPKValue());
            node.setName(o.getCategory_name());
            node.setParentId(pid);
            node.setIcon(o.getCategory_icon());
            node.setOrd(o.getCategory_ord());
            node.setOperator(o.getCategory_status() == 0 ? "已停用" : "已启用");
            nodeList.add(node);
        }

        TreeUtil.setRootCode(Category.ROOT_ID.toString());
        List<ZtreeNode> ztreeNodes = TreeUtil.getJsonList(nodeList);

        renderOk("", "成功", ztreeNodes);
    }


    /**
     * 保存
     */
    @Before(CategoryValidator.class)
    public void save() {
        boolean f = Category.dao.create(super.getParamMap());
        if (f) {
            renderOk("refresh", "添加成功", null);
        } else {
            renderErr(400, "", "保存失败");
        }
    }

    /**
     * 更新
     */
    @Before(CategoryValidator.class)
    public void update() {
        boolean f = Category.dao.updateData(getParaToInt(), super.getParamMap());
        if (f) {
            renderOk("refresh", "修改成功", null);
        } else {
            renderErr(400, "", "保存失败");
        }
    }

    /*
    param:category_ord
     */
    @Before(CategoryValidator.class)
    public void updateOrd(){
        Category cat = Category.dao.findById(getParaToInt());
        cat.setCategory_ord(getParaToInt("category_ord"));
        cat.update();
        Category.dao.cacheRemove(cat.getId());
        renderOk("refresh", "修改成功", null);
    }

    /**
     * 删除
     * 有父类存在时禁止删除
     */
    public void delete() {
        String str = ids == null ? getPara("checkbox") : ids;
        Category.dao.delData(str);
        for (String s : str.split(",")) {
            Category.dao.cacheRemove(Integer.parseInt(s));
        }
        renderOk("refresh", "删除成功", null);
    }

    /*
    详情
     */
    public void view(){
        Category category = Category.dao.getDetail(getParaToInt());
        success(category);
    }

}

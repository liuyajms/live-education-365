package com.application.admin;

import com.application.model.Rebate;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.base.BaseModel;

import java.sql.Timestamp;

/**
 *
 */
@Controller("/admin/rebate")
public class RebateController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(RebateController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, BaseModel.sqlId_splitPageSelect, Rebate.sqlId_splitPageFrom);
		renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	public void save() {
		Rebate rebate = getModel(Rebate.class, "a");
		rebate.setCreate_time(new Timestamp(System.currentTimeMillis()));
		rebate.save();
		renderOk("refresh", "添加成功", null);
	}
	
	/**
	 * 更新
	 */
	public void update() {
		Rebate rebate = getModel(Rebate.class, "a");
		rebate.setId(getParaToInt("id"));
		rebate.update();
		renderOk("refresh", "修改成功", null);
	}

	/**
	 * 查看
	 */
	public void view() {
		Rebate obj = Rebate.dao.findById(getPara());
		success(obj);
	}
	
	/**
	 * 删除
	 */
	public void delete() {
		String str = getPara() == null ? getPara("checkbox") : getPara();
		for (String s : str.split(",")) {
			Record first = Db.findFirst("select id from t_product where rebate_id = ?", s);
			if(first != null && first.get("id") != null) {
				renderErr(400, "", "方案已被使用");
				return;
			}
		}
		Rebate.dao.falseDelete(str, getCUserId());
		renderOk("refresh", "成功", null);
	}
	
}

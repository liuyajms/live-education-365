package com.application.admin;

import com.application.feedback.Feedback;
import com.jfinal.aop.Before;
import com.platform.annotation.Controller;
import com.platform.interceptor.ReadTimeInterceptor;
import com.platform.mvc.base.BaseController;

/**
 * 意见反馈
 */
@Controller("/admin/feedback")
public class FeedbackController extends BaseController {

    @Before(ReadTimeInterceptor.class)
    public void index() {
        paging(splitPage, Feedback.sqlId_splitPageSelect, Feedback.sqlId_splitPageFrom);
        renderOk(splitPage);
    }


    /**
     * 删除
     */
    public void delete() {
        Feedback.dao.deleteByIds(ids == null ? getPara("checkbox") : ids);
        renderOk("reload", "删除成功", null);
    }
}

package com.application.admin;

import com.application.model.DictValue;
import com.application.validate.DictValidator;
import com.jfinal.aop.Before;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * 数据字典管理
 */
@Controller("/admin/dict")
public class DictController extends BaseController {

    public void index() {

        paging(splitPage, "com.application.dictValue.splitPageSelect", "com.application.dictValue.splitPageFrom");

        renderOk(splitPage);
    }


    /*
    @param {parentids}
     */
    @Before(DictValidator.class)
    public void save() {

        DictValue dictValue = DictValue.dao.getModelByMap(null, super.getParamMap());
        dictValue.save();

        renderOk("refresh", "添加成功", null);
    }


    public void update() {

        int id = getPara() == null ? getParaToInt("id") : getParaToInt();
        DictValue dictValue = DictValue.dao.getModelByMap(id, super.getParamMap());
        dictValue.update();

        renderOk("refresh", "修改成功", null);
    }

    /**
     * 删除
     */
    public void delete() {
        DictValue.dao.deleteByIds(ids == null ? getPara("checkbox") : ids);
        renderOk("refresh", "删除成功", null);
    }
}

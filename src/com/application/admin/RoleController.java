package com.application.admin;

import com.application.validate.SuperAdminValidator;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.role.Role;
import com.platform.mvc.role.RoleService;

/**
 * 角色管理
 */
@Controller("/admin/role")
public class RoleController extends BaseController {

    RoleService roleService;

    public void index() {

        paging(splitPage, Role.sqlId_splitPageSelect, Role.sqlId_splitPageFrom);

        renderOk(splitPage);
    }


    /*
    @param {parentids}
     */
    @Before(SuperAdminValidator.class)
    public void save() {

        Role role = new Role();
        role.setIds(getPara("ids"));
        role.setNames(getPara("names"));
        role.setDescription(getPara("description"));
        role.setNumbers(role.getIds());
        role.save();
        renderOk("refresh", "添加成功", null);
    }

    @Before(SuperAdminValidator.class)
    public void updateOptional() {

        String name = getPara("names");
        String desc = getPara("description");
        String id = getPara("ids");

        Role role = Role.dao.findById(id);
        if (StrKit.notBlank(name)) {
            role.setNames(name);
        }

        if (StrKit.notBlank(desc)) {
            role.setDescription(desc);
        }

        if (role.update()) {
            renderOk("refresh", "修改成功", null);
        } else {
            renderErr(500, "", "修改失败");
        }
    }

    /**
     * 删除
     */
    @Before(SuperAdminValidator.class)
    public void delete() {
        roleService.baseDelete(Role.table_name, ids == null ? getPara("checkbox") : ids);
        renderOk("refresh", "删除成功", null);
    }
}

package com.application.admin;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Controller;
import com.platform.constant.ConstantInit;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.roleoperator.RoleOperator;
import com.platform.mvc.roleoperator.RoleOperatorService;

/**
 * 角色功能 管理	
 * 描述：
 */
@Controller("/admin/roleOperator")
public class RoleOperatorController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(RoleOperatorController.class);

	private RoleOperatorService roleOperatorService;
	
	/**
	 * 展示角色拥有的功能和所有功能列表，参数roleId
	 */
	public void index(){

        String key = getPara("_query.keyword");
        String roleId =  getPara("_query.roleId");
        String select = "SELECT a.*, b.ids as role_op ";
        StringBuffer sb = new StringBuffer(500);
        sb.append("from pt_operator a left join pt_roleoperator b on a.ids = b.operatorIds and b.roleIds =? ");
        if (StrKit.notBlank(key)) {
            sb.append(" where (instr(a.url, '{x}') >0 or instr(a.names, '{x}')>0 ) ".replace("{x}", key));
        }
        sb.append(" order by isnull(b.ids)-1, a.names ");

        Page<RoleOperator> p = RoleOperator.dao.paginate(splitPage.getPageNumber(), splitPage.getPageSize(),
                select, sb.toString(), roleId);
        splitPage.setTotalPage(p.getTotalPage());
        splitPage.setList(p.getList());
        success(splitPage);
	}

	/**
	 * 添加角色拥有的功能
	 */
	public void add() {
		String roleIds = getPara("roleIds");
		String operatorIds = getPara("operatorIds");
		String ids = roleOperatorService.add(roleIds, operatorIds);
		success(ids);
	}

	/**
	 * 删除角色拥有的功能
	 */
	public void del() {
		//roleOperatorService.del(getPara());

        String roleIds = getPara("roleIds");
        String operatorIds = getPara("operatorIds");
        String sql = "select * from pt_roleoperator where roleIds = ? and operatorIds =?";
        RoleOperator first = RoleOperator.dao.findFirst(sql, roleIds, operatorIds);
        if (first != null && first.delete()) {
            RoleOperator.cacheRemoveByRoleId(roleIds);
            success(null);
        } else {
            success(null);
        }
	}
	
}

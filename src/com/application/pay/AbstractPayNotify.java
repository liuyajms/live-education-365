package com.application.pay;

import com.application.api.PayNotifyZfbController;
import com.application.common.OrderStatus;
import com.application.model.Order;
import com.application.model.OrderFlow;
import com.application.model.Rebate;
import com.application.service.OrderService;
import com.jfinal.log.Log;

import java.text.SimpleDateFormat;

public abstract class AbstractPayNotify {
    private static final Log log = Log.getLog(PayNotifyZfbController.class);

    private String payAppId;//商户id
    private String payOrderNo;//商户订单号
    private String payTime;
    private String payAmount;
    private String payType;
    
    public AbstractPayNotify(String payTime, String payAmount, String payType, String payAppId, String payOrderNo) {
        this.payTime = payTime;
        this.payAmount = payAmount;
        this.payType = payType;
        this.payAppId = payAppId;
        this.payOrderNo = payOrderNo;
    }

    abstract String getAppId();

        /*
        1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
        2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方,
        4、验证app_id是否为该商户本身。
     */
    boolean checkOrderInfo(Order order){
        String appId = getAppId();
        String msg = "";
        if (order != null) {
            if (order.getOrder_status() == OrderStatus.PAY_WAIT) {
                String p = order.getBigDecimal("total_amount").toString();
                if (payAmount.equals(p)) {//支付成功，金额正确
                    if (payAppId.equals(appId)) {
                        return true;
                    } else {
                        msg = "订单appID不一致，实际为：" + appId;
                    }
                } else {
                    msg = "订单金额不正确,应为：" + p + "，实际为：" + payAmount;
                }
            } else {//判断是否为重复接收到的微信消息
                if(order.getPay_time() != null){
                    String ymd = new SimpleDateFormat("yyyyMMddHHmmss").format(order.getPay_time());
                    if(ymd.equalsIgnoreCase(payTime)){
                        return true;
                    }
                }
                log.error("重复支付订单，订单号：" + order.get("order_no"));
            }
        } else {
            msg = "订单不存在，订单号：" + payOrderNo;
        }
        if (!"".equals(msg)) {
            log.error(msg);
        }
        return false;
    }

    boolean changeOrderState(Order order){
        OrderService orderService = new OrderService();
        boolean f = orderService.updateOrderStatus(order, order.getOrder_no(), payTime, payAmount, payType);
        //记录订单流程
        OrderFlow.dao.create(order.getId(), OrderStatus.PAY_OVER, order.getOrder_userid(), "支付订单");
        //上线返利
        Rebate.dao.rebate(order.getId(), order.getOrder_userid(), order.getOrder_no());
        return f;
    }

    abstract String returnSuccess();
    abstract String returnFailed();
    abstract boolean checkPayAmount(Order order);

    public String run(){
        String outTradeNo = payOrderNo;
        Order order = Order.dao.findByNumber(outTradeNo);
//        if (checkOrderInfo(order) && checkPayAmount(order)) {
//            changeOrderState(order);
//        }
        changeOrderState(order);
        return returnSuccess();
    }

}

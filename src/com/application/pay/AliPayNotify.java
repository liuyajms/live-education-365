package com.application.pay;

import com.application.model.Order;
import com.jfinal.ext.util.PayUtil;
import com.jfinal.kit.PropKit;

/*
1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方,
4、验证app_id是否为该商户本身。
 */
public class AliPayNotify extends AbstractPayNotify {

    private static final String appId = PropKit.get("alipay.appId");

    public AliPayNotify(String payTime, String payAmount, String payType, String payAppId, String payOrderNo) {
        super(payTime, payAmount, payType, payAppId, payOrderNo);
    }


    @Override
    String getAppId() {
        return appId;
    }

    @Override
    String returnSuccess() {
        return "success";
    }

    @Override
    String returnFailed() {
        return "failure";
    }

    @Override
    boolean checkPayAmount(Order order) {
        String money = PayUtil.queryPayAmount(order.getOrder_no());
        return money.equals(order.getTotal_amount().toString());
    }
}

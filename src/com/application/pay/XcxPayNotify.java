package com.application.pay;

import com.application.model.Order;
import com.github.wxpay.sdk.MyXcxPayConfig;
import com.github.wxpay.sdk.WXPayUtil;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;

import java.math.BigDecimal;

public class XcxPayNotify extends AbstractPayNotify {
    private static final Log log = Log.getLog(XcxPayNotify.class);

    private static final String appId = PropKit.get("xcx.appid");

    public XcxPayNotify(String payTime, String payAmount, String payType, String payAppId, String payOrderNo) {
        super(payTime, payAmount, payType, payAppId, payOrderNo);
    }

    @Override
    String getAppId() {
        return appId;
    }

    @Override
    String returnSuccess() {
        String s = "<xml> \n" +
                "\n" +
                "  <return_code><![CDATA[SUCCESS]]></return_code>\n" +
                "  <return_msg><![CDATA[OK]]></return_msg>\n" +
                "</xml> \n";
        return s;
    }

    @Override
    String returnFailed() {
        return null;
    }

    @Override
    boolean checkPayAmount(Order order) {
        try {
            String money = WXPayUtil.queryPayAmount(order.getOrder_no(), MyXcxPayConfig.getInstance());
            return getFormatMoney(money).equals(order.getTotal_amount().toString());
        } catch (Exception e) {
            log.error("获取微信支付金额失败...");
            e.printStackTrace();
        }
        return false;
    }


    public static String getFormatMoney(String fee){
        BigDecimal amount = new BigDecimal(fee).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
        return amount.toString();
    }
}

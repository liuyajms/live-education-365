package com.application.intercept;

import com.alibaba.fastjson.JSON;
import com.application.api.entity.ResultEntity;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.PropKit;
import com.jfinal.render.JsonRender;
import com.platform.constant.ConstantAuth;
import com.platform.constant.ConstantInit;
import com.platform.constant.ConstantWebContext;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.syslog.Syslog;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Map;

/**
 * 订单金额变动拦截器
 */
public class OrderPriceChangedInterceptor implements Interceptor {

    private static Logger log = Logger.getLogger(OrderPriceChangedInterceptor.class);

    @Override
    public void intercept(Invocation invoc) {
        BaseController contro = (BaseController) invoc.getController();
        Syslog reqSysLog = contro.getReqSysLog();
        try {
            invoc.invoke();

            handler(invoc);
        } catch (Exception e) {
            String expMessage = e.getMessage();
            // 开发模式下的异常信息
            if (Boolean.parseBoolean(PropKit.get(ConstantInit.config_devMode))) {
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                e.printStackTrace(new PrintWriter(buf, true));
                expMessage = buf.toString();
            }

            log.error("业务逻辑代码遇到异常时保存日志!");
            reqSysLog.set(Syslog.column_status, "0");// 失败
            reqSysLog.set(Syslog.column_description, expMessage);
            reqSysLog.set(Syslog.column_cause, "3");// 业务代码异常

            log.error("返回失败提示页面!Exception = " + e.getMessage());

            toJson(contro, ConstantAuth.auth_exception, "业务逻辑代码遇到异常Exception = " + expMessage);
        } finally {
            MDC.remove("userId");
            MDC.remove("userName");
        }
    }

    private void handler(Invocation invo) {
        BaseController ct = (BaseController) invo.getController();
        String userId = ct.getAttr(ConstantWebContext.request_cUserIds);
        //判断添加成功
        String jsonText = ((JsonRender) ct.getRender()).getJsonText();
        Map<String, Object> map = (Map<String, Object>) JSON.parse(jsonText);
        if (!"200".equals(map.get("code").toString())) {
            return;
        }
        switch (invo.getActionKey()){
            /*case "/api/login/vali":
            case "/api/login":
                User.addPoint("login", userId, PropKit.getInt("login.min"), PropKit.getInt("login.max"));
                break;*/
        }
    }


    private void toJson(BaseController contro, String type, String msg) {
        ResultEntity res = null;
        if (type.equals(ConstantAuth.auth_no_login)) {// 未登录处理
            res = new ResultEntity(HttpStatus.SC_UNAUTHORIZED, "未登录");
        } else if (type.equals(ConstantAuth.auth_exception)) {
            res = new ResultEntity(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
        }

        contro.renderJson(res);
    }
}

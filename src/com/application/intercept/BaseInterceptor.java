package com.application.intercept;

import com.alibaba.fastjson.JSON;
import com.application.api.entity.ResultEntity;
import com.application.module.withdraw.Withdraw;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.render.JsonRender;
import com.platform.constant.ConstantAuth;
import com.platform.constant.ConstantWebContext;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.user.User;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 */
public abstract class BaseInterceptor implements Interceptor {

    private static Logger log = Logger.getLogger(BaseInterceptor.class);

    @Override
    public void intercept(Invocation invoc) {
        /*BaseController contro = (BaseController) invoc.getController();
        Syslog reqSysLog = contro.getReqSysLog();
        try {
            invoc.invoke();

            handler(invoc);
        } catch (Exception e) {
            String expMessage = e.getMessage();
            // 开发模式下的异常信息
            if (Boolean.parseBoolean(PropKit.get(ConstantInit.config_devMode))) {
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                e.printStackTrace(new PrintWriter(buf, true));
                expMessage = buf.toString();
            }

            log.error("业务逻辑代码遇到异常时保存日志!");
            reqSysLog.set(Syslog.column_status, "0");// 失败
            reqSysLog.set(Syslog.column_description, expMessage);
            reqSysLog.set(Syslog.column_cause, "3");// 业务代码异常

            log.error("返回失败提示页面!Exception = " + e.getMessage());

            toJson(contro, ConstantAuth.auth_exception, "业务逻辑代码遇到异常Exception = " + expMessage);
        } finally {
            MDC.remove("userId");
            MDC.remove("userName");
        }*/

        invoc.invoke();
        handler(invoc);


        BaseController ct = (BaseController) invoc.getController();
        String jsonText = ((JsonRender) ct.getRender()).getJsonText();
        Map<String, Object> map = (Map<String, Object>) JSON.parse(jsonText);
        if ((map.containsKey("status") && "200".equals(map.get("status").toString()))
                || (map.containsKey("code") && "200".equals(map.get("code").toString()))) {
            success(ct.getAttr(ConstantWebContext.request_cUser));
        } else {
            error(invoc);
        }
    }

    protected void success(User user){};

    protected void error(Invocation invoc) {

    }

    protected abstract void handler(Invocation invoc);


    protected boolean isOk(Invocation invoc) {
        BaseController ct = (BaseController) invoc.getController();
        String jsonText = ((JsonRender) ct.getRender()).getJsonText();
        Map<String, Object> map = (Map<String, Object>) JSON.parse(jsonText);
        if ("200".equals(map.get("status").toString()) && ct.getParaToInt("status") == Withdraw.STATUS_OK) {
            return true;
        }
        return false;
    }

    private void toJson(BaseController contro, String type, String msg) {
        ResultEntity res = null;
        if (type.equals(ConstantAuth.auth_no_login)) {// 未登录处理
            res = new ResultEntity(HttpStatus.SC_UNAUTHORIZED, "未登录");
        } else if (type.equals(ConstantAuth.auth_exception)) {
            res = new ResultEntity(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
        }

        contro.renderJson(res);
    }
}

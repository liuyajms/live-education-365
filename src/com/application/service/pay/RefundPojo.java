package com.application.service.pay;

import com.application.model.Order;
import com.application.model.OrderRefund;

import java.io.Serializable;

public class RefundPojo implements Serializable {
    private Order order;
    private OrderRefund refund;
    private Integer userid;

    public RefundPojo(Order order, OrderRefund refund, int userid) {
        this.order = order;
        this.refund = refund;
        this.userid = userid;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public OrderRefund getRefund() {
        return refund;
    }

    public void setRefund(OrderRefund refund) {
        this.refund = refund;
    }
}

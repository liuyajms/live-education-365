package com.application.service.pay;

import com.application.model.Order;
import com.application.model.OrderRefund;
import com.jfinal.ext.util.PayUtil;

public class ZfbPay implements IPay {
    @Override
    public Object getPayInfo(OrderPojo pojo) throws Exception {
        Order order = pojo.getOrder();
        String body = PayConfig.getPushBody(order.getOrder_no());
        return PayUtil.generateAliSign(order.getOrder_no(), body, order.getTotal_amount().toString());
    }

    @Override
    public boolean refund(RefundPojo pojo) {
        Order order = pojo.getOrder();
        OrderRefund ref = pojo.getRefund();
        return PayUtil.refund(order.getOrder_no(), ref.getRefund_amount(), ref.getRefund_reason(), ref.getId().toString());
    }

    @Override
    public String queryPayAmount(String orderNo) {
        return PayUtil.queryPayAmount(orderNo);
    }
}

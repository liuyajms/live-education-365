package com.application.service.pay;


public interface IPay {
    Object getPayInfo(OrderPojo pojo) throws Exception;

    boolean refund(RefundPojo pojo);

    String queryPayAmount(String orderNo);
}

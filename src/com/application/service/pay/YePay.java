package com.application.service.pay;

import com.application.model.Order;
import com.application.model.Rebate;
import com.application.module.usermoney.ApplyUserMoney;
import com.application.module.usermoney.CostUserMoney;
import com.application.service.OrderService;
import com.jfinal.ext.kit.MsgException;
import com.jfinal.plugin.activerecord.Db;
import com.platform.mvc.user.User;

import java.math.BigDecimal;

public class YePay implements IPay {
    @Override
    public Object getPayInfo(OrderPojo pojo) throws Exception {
        Object obj;
        Order order = pojo.getOrder();
        User user = pojo.getUser();
        if (user.get("pay_pass").equals(pojo.getPass())) {
            boolean b = payYe(order.getTotal_amount(), order, user);
            if (b) {
                obj = 1;
            } else {
                obj = 0;
            }
        } else {
            throw new MsgException("支付密码错误");
        }
        return obj;
    }

    @Override
    public boolean refund(RefundPojo pojo) {
        boolean f;
        String refundAmount = pojo.getRefund().getRefund_amount();
        Integer orderUserid = pojo.getOrder().getOrder_userid();
        User user = User.dao.findById(orderUserid);
        //退送至赠送的金额,同时记录消费明细
        user.set("gift", user.getBigDecimal("gift").add(new BigDecimal(refundAmount)));
        new ApplyUserMoney().create(user.getId(), refundAmount, pojo.getOrder().getOrder_no(), pojo.getUserid());
        f = user.update();
        User.cacheRemove(orderUserid);
        return f;
    }

    @Override
    public String queryPayAmount(String orderNo) {
        return null;
    }

    /**
     * 余额支付
     *
     * @param totalAmount
     * @param order
     * @return
     * @throws Exception
     */
    public boolean payYe(BigDecimal totalAmount, Order order, User user) {
        OrderService service = new OrderService();
        BigDecimal money = user.get("money") != null ? user.getBigDecimal("money") : new BigDecimal(0);
        BigDecimal gift = user.get("gift") != null ? user.getBigDecimal("gift") : new BigDecimal(0);
        if (money.add(gift).compareTo(totalAmount) == -1) {
            throw new MsgException("余额不足，请充值。");
        } else {
            User m = new User();
            m.setId(user.getId());
            if (gift.compareTo(totalAmount) >= 0) {
                m.set("gift", gift.subtract(totalAmount));
            } else {
                m.set("gift", 0);
                m.set("money", money.subtract(totalAmount.subtract(gift)));
            }

            boolean rs = Db.tx(() -> {
                if (m.update()) {
                    User.cacheRemove(user.getId());
                    //修改订单状态
                    String ono = order.getOrder_no();
                    boolean f = service.updateOrderStatus(order, ono, null, totalAmount
                            .toString(), Order.PAY_YE);
                    if (f) {
                        //记录消费信息
                        String cost = totalAmount.multiply(new BigDecimal(-1)).toString();
                        new CostUserMoney().create(user.getId(), cost, ono, user.getId());
                        //上线返利
                        Rebate.dao.rebate(order.getId(), user.getId(), ono);
                        return true;
                    }
                }
                return false;
            });
            return rs;
        }
    }
}

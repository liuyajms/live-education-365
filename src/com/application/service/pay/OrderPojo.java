package com.application.service.pay;

import com.application.model.Order;
import com.platform.mvc.user.User;

import java.io.Serializable;

public class OrderPojo implements Serializable {
    private Integer payType;
    private Order order;
    private User user;
    private String pass;
    private String clientIp;
    private String openid;

    public OrderPojo(Order order, User user, String pass, String clientIp, String openid) {
        this.order = order;
        this.user = user;
        this.pass = pass;
        this.clientIp = clientIp;
        this.openid = openid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }
}

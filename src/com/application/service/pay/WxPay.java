package com.application.service.pay;

import com.application.model.Order;
import com.application.model.OrderRefund;
import com.github.wxpay.sdk.MyWXPayConfig;
import com.github.wxpay.sdk.WXPayUtil;
import com.jfinal.ext.util.PayUtil;

import java.math.BigDecimal;

public class WxPay implements IPay {

    @Override
    public Object getPayInfo(OrderPojo pojo) throws Exception {
        Order order = pojo.getOrder();
        int p = order.getTotal_amount().multiply(new BigDecimal(100)).intValue();//微信支付单位为分
        String body = PayConfig.getPushBody(order.getOrder_no());
        return PayUtil.unifiedOrder(order.getOrder_no(), body, String.valueOf(p), pojo.getClientIp(), pojo.getOpenid());
    }

    @Override
    public boolean refund(RefundPojo pojo) {
        Order order = pojo.getOrder();
        OrderRefund ref = pojo.getRefund();
        return PayUtil.wxRefund(order.getOrder_no(), ref.getId(), ref.getRefund_amount(), order.getPay_amount().toString());
    }

    @Override
    public String queryPayAmount(String orderNo) {
        String money = "";
        try {
            money = WXPayUtil.queryPayAmount(orderNo, MyWXPayConfig.getInstance());
            money = new BigDecimal(money).divide(new BigDecimal(100)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return money;
    }
}

package com.application.service;

import com.application.model.Order;
import com.application.service.pay.*;

import java.util.HashMap;
import java.util.Map;

public class ServiceFactory {
    static Map<String, IPay> payMap = new HashMap<>();
    static {
        payMap.put("0", new YePay());
        payMap.put("1", new WxPay());
        payMap.put("2", new ZfbPay());
        payMap.put("4", new XcxPay());

        payMap.put(Order.PAY_YE, new YePay());
        payMap.put(Order.PAY_WX, new WxPay());
        payMap.put(Order.PAY_ZFB, new ZfbPay());
        payMap.put(Order.PAY_XCX, new XcxPay());
    }
    /*
    //0 余额支付,返回成功即可
    //1微信 2 支付宝 4小程序
     */
    public static IPay getPayObject(Integer payType) {
        return payMap.get(payType);
    }

    public static IPay getPayObject(String payType) {
        return payMap.get(payType);
    }
}

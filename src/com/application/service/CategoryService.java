package com.application.service;

import com.application.model.Category;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service(name = CategoryService.serviceName)
public class CategoryService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CategoryService.class);
	
	public static final String serviceName = "categoryService";

    public List<String> saveMulti(List<Category> dataList) {
        //数据预处理
        parseData(dataList);
        //查找所有行业分类，判断是否已存在
        List<Category> list = Category.dao.findAllList();
        List<String> errors = new ArrayList<>();
        List<Category> plist = dataList.stream().filter(o -> StrKit.isBlank(o.get("parentname"))).collect(Collectors
                .toList());
        List<Category> slist = dataList.stream().filter(o -> !StrKit.isBlank(o.get("parentname"))).collect(Collectors
                .toList());


        for (int i = 0; i < plist.size(); i++) {
            Category o = plist.get(i);
            boolean find = checkExists(list, o);
            if(!find){
//                o.setIds(ToolRandoms.getUuid(true));
//                o.setLevel(1);
            } else {
                plist.remove(i);
            }
        }
        list.addAll(plist);//父节点加入节点列表


        for (int i = 0; i < slist.size(); i++) {
            Category o = slist.get(i);
            String pname = o.get("parentname");
//            for (Category a : list) {
//                if(a.getName().equals(pname)) {
//                    o.setParentid(a.getIds());
//                    o.setLevel(a.getLevel() + 1);
//                    break;
//                }
//            }

//            if (o.getParentid() == null) {
//                //查找索引
//                int index = 1;
//                for (int j = 0; j < dataList.size(); j++) {
//                    if (dataList.get(j).equals(o)) {
//                        index = j + 2;
//                    }
//                }
//                errors.add(MessageFormat.format("第{0}行数据错误，【{1}】不存在", index, pname));
//                continue;
//            }

            boolean find = checkExists(list, o);
            if(find){
                slist.remove(i);
            }

        }

//        if (errors.size() == 0) {
//            plist.forEach(o -> o.save(o.getIds()));
//            slist.forEach(o -> o.save(o.getIds()));
//        }

        return errors;
    }

    private void parseData(List<Category> dataList) {
        dataList.forEach(o -> {
            if (StrKit.isBlank(o.get("ord"))) {
                o.set("ord", 0);
            }
        });
    }

    private boolean checkExists(List<Category> list, Category o){
        boolean find = false;
//        for (Category a : list) {
//            if(a.getName().equals(o.getName())){
//                find = true;
//                break;
//            }
//        }
        return find;
    }

    public void saveMulti(List<Category> categoryList, List<String> errors) {
        for (int i = 0; i < categoryList.size(); i++) {
            Category c = Category.dao.findById(categoryList.get(i).getId());
            if (c != null) {
                errors.add(MessageFormat.format("第{0}行数据错误，编码{1}已存在", i + 1, c.getId()));
            }
        }

        if (errors.size() == 0) {
            Db.batchSave(categoryList, 1000);
        }

    }


}

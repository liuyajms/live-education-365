package com.application.service;

import com.jfinal.log.Log;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;

@Service(name = NoticeService.serviceName)
public class NoticeService extends BaseService {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(NoticeService.class);
	
	public static final String serviceName = "noticeService";
	
}

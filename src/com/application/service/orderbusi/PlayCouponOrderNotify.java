package com.application.service.orderbusi;

import com.application.model.Order;
import com.application.module.buycoupon.BuyCoupon;
import com.application.module.couponget.CouponGet;
import com.application.module.coupontype.CouponType;

public class PlayCouponOrderNotify implements IOrderNotify {
    @Override
    public boolean handler(Order order) {
        CouponType type = CouponType.dao.findById(order.getModule_id());
        CouponGet.dao.add(CouponGet.SOURCE_BUY, CouponGet.TYPE_PLAY, type.getNum(), order.getOrder_userid());
        saveRecord(order, type);
        return true;
    }

    private void saveRecord(Order order, CouponType type) {
        BuyCoupon obj = new BuyCoupon();
        obj.setCoupon_num(type.getNum());
        obj.setCoupon_price(order.getPay_amount());
        obj.setCoupon_type_id(type.getId());
        obj.setCreate_time(order.getPay_time());
        obj.setUserid(order.getOrder_userid());
        obj.save();
    }

}

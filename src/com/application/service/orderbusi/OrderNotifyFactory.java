package com.application.service.orderbusi;

import java.util.HashMap;
import java.util.Map;

public class OrderNotifyFactory {
    static Map<String, IOrderNotify> map = new HashMap<>();

    static {
        map.put("v", new VipChargeOrderNotify());
        map.put("p", new PlayCouponOrderNotify());
    }

    public static IOrderNotify getObject(String no) {
        return map.get(no.substring(0, 1));
    }
}

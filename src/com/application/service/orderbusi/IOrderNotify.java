package com.application.service.orderbusi;

import com.application.model.Order;

public interface IOrderNotify {
    boolean handler(Order order);
}

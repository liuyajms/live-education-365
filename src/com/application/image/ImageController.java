package com.application.image;

import com.jfinal.kit.PropKit;
import com.jfinal.upload.UploadFile;
import com.platform.annotation.Controller;
import com.platform.constant.ConstantInit;
import com.platform.mvc.base.BaseController;
import com.platform.tools.ToolDateTime;
import com.platform.tools.ToolString;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.List;


@Controller("/app/image")
public class ImageController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ImageController.class);

	private String filePath = "/upload/";

    ImageService imageService;
	/**
	 * 图片上传处理
	 */
	public void index() {
//		String moduleDir =

		filePath += ToolDateTime.format(new Date(System.currentTimeMillis()), "yyyyMMdd");

		List<UploadFile> files = getFiles(filePath, PropKit.getInt(ConstantInit.config_maxPostSize_key), ToolString
				.encoding);

		List<Image> list = imageService.upload(String.valueOf(getCUser().getId().longValue()), filePath, files);

		renderJson(list);
	}

	/**
	 * 删除
	 */
	public void delete() {
//		ImageService.service.delete("image", getPara() == null ? ids : getPara());
	}
	
}

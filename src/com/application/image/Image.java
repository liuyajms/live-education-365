package com.application.image;

import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import org.apache.log4j.Logger;

/**
 *  model
 * @author 董华健  dongcb678@163.com
 */
@SuppressWarnings("unused")
@Table(tableName = "image")
public class Image extends BaseModel<Image> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static Logger log = Logger.getLogger(Image.class);
	
	public static final Image dao = new Image();
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_ids = "ids";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_fileName = "fileName";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(256)  长度：256
	 */
	public static final String column_filePath = "filePath";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(256)  长度：256
	 */
	public static final String column_fileSize = "fileSize";

	public static final String column_contentType = "contentType";

	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_originalFileName = "originalFileName";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(256)  长度：256
	 */
	public static final String column_miniUrl = "miniUrl";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(256)  长度：256
	 */
	public static final String column_url = "url";
	
	/**
	 * 字段描述： 
	 * 字段类型：timestamp  长度：null
	 */
	public static final String column_createTime = "createTime";
	
	/**
	 * 字段描述： 
	 * 字段类型：varchar(32)  长度：32
	 */
	public static final String column_createUserId = "createUserId";
	
	
	/**
	 * sqlId : talent.image.splitPageFrom
	 * 描述：分页from
	 */
	public static final String sqlId_splitPageFrom = "talent.image.splitPageFrom";

	private String ids;
	private String miniUrl;
	private String url;
	private String fileName;
	private String filePath;

	public String getIds() {
		return get(column_ids);
	}

	public void setIds(String ids) {
		set(column_ids, ids);
	}

	public String getMiniUrl() {
		return get(column_miniUrl);
	}

	public void setMiniUrl(String miniUrl) {
		set(column_miniUrl, miniUrl);
	}

	public String getUrl() {
		return get(column_url);
	}

	public void setUrl(String url) {
		set(column_url, url);
	}

	public String getFileName() {
		return get(column_fileName);
	}

	public void setFileName(String fileName) {
		set(column_fileName, fileName);
	}

	public String getFilePath() {
		return get(column_filePath);
	}

	public void setFilePath(String filePath) {
		set(column_filePath, filePath);
	}
}

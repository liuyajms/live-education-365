package com.application.image;

import com.application.common.utils.ImageUtils;
import com.application.common.utils.OssUtil;
import com.jfinal.upload.UploadFile;
import com.platform.annotation.Service;
import com.platform.mvc.base.BaseService;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service(name = ImageService.serviceName)
public class ImageService extends BaseService {

    private static Logger log = Logger.getLogger(ImageService.class);

    public static final String serviceName = "imgService";

//    public static final ImageService service = Enhancer.enhance(ImageService.class);

    public List<Image> upload(String userId, String filePath, List<UploadFile> files) {
        List<Image> list = new ArrayList<>();
        Timestamp time = new Timestamp(System.currentTimeMillis());
        for (UploadFile file : files) {

            String newFileName = file.getFileName();
//            String miniFileName = newFileName.replace(".", "_mini.");

            String prefix = file.getFileName().split("\\.")[0];
//            String newFileName = ImageUtils.zoom(file.getFile(), new File(file.getUploadPath(), prefix + "a"), 1000,
//                    1000);
            String miniFileName = prefix + "_s";
            long fileSize = file.getFile().length();

            if(file.getContentType().startsWith("image")){
                //压缩图片
                miniFileName = ImageUtils.zoom(file.getFile(), new File(file.getUploadPath(), miniFileName));
            } else {
                miniFileName = newFileName;
            }

            //重命名文件后删除源文件
            if(!newFileName.equals(file.getFile().getName())){
                file.getFile().delete();
            }


            String fileUrl = filePath + "/" + newFileName;
            Image img = new Image();
            img.set(Image.column_fileName, newFileName);
            img.set(Image.column_contentType, file.getContentType());
            img.set(Image.column_originalFileName, file.getOriginalFileName());
            img.set(Image.column_createTime, time);
            img.set(Image.column_createUserId, userId);
            img.set(Image.column_fileSize, fileSize);
            img.set(Image.column_filePath, file.getFile().getAbsolutePath());
            img.set(Image.column_url, fileUrl);
            img.set(Image.column_miniUrl, filePath + "/" + miniFileName);

            try {
                BufferedImage image = ImageIO.read(new FileInputStream(file.getFile()));
                img.put("width", image.getWidth());
                img.put("height", image.getHeight());

                //上传图片oss
                OssUtil.upload(new FileInputStream(file.getFile()), fileUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }

            img.save();

            list.add(img);
        }
        return list;
    }

    public static Image uploadFile(UploadFile file, String filePath, String userId) {

        if (file == null) {
            return new Image();
        }

        Timestamp time = new Timestamp(System.currentTimeMillis());

        String newFileName = file.getFileName();
        long fileSize = file.getFile().length();

        //压缩图片
        String prefix = file.getFileName().split("\\.")[0];
        String miniFileName = prefix + "_mini";
        miniFileName = ImageUtils.zoom(file.getFile(), new File(file.getUploadPath(), miniFileName));

        //重命名文件后删除源文件
        if(!newFileName.equals(file.getFile().getName())){
            file.getFile().delete();
        }

        Image img = new Image();
        img.set(Image.column_fileName, newFileName);
        img.set(Image.column_contentType, file.getContentType());
        img.set(Image.column_originalFileName, file.getOriginalFileName());
        img.set(Image.column_createTime, time);
        img.set(Image.column_createUserId, userId);
        img.set(Image.column_fileSize, fileSize);
        img.set(Image.column_filePath, file.getFile().getAbsolutePath());
        img.set(Image.column_url, filePath + "/" + newFileName);
        img.setMiniUrl(filePath + "/" + miniFileName);

        img.save();

        return img;
    }

}

package com.application.image;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
import org.apache.log4j.Logger;

public class ImageValidator extends Validator {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ImageValidator.class);
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/shop/talent/image/save")){
			// validateString("username", 6, 30, "usernameMsg", "请输入登录账号!");
			
		} else if (actionKey.equals("/shop/talent/image/update")){
			
		}
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Image.class);
		
		String actionKey = getActionKey();
		if (actionKey.equals("/shop/talent/image/save")){
			controller.render("/talent/xxx.html");
		
		} else if (actionKey.equals("/shop/talent/image/update")){
			controller.render("/talent/xxx.html");
		
		}
	}
	
}

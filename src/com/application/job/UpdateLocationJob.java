package com.application.job;

import com.jfinal.log.Log;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateLocationJob implements Job {

    private static final Log log = Log.getLog(UpdateLocationJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {
            if (log.isInfoEnabled()) log.info("定时任务更新企业坐标");
//            EnterpriseService service = (EnterpriseService) ServicePlugin.getService(EnterpriseService.serviceName);
//            service.updateLocation();
        } catch (Exception e) {
            if (log.isErrorEnabled()) log.error("定时任务更新企业坐标失败：" + e.getMessage(), e);
            e.printStackTrace();
        }
    }

}

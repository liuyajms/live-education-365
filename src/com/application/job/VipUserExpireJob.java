package com.application.job;

import com.application.common.Constant;
import com.platform.mvc.user.User;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

public class VipUserExpireJob implements Job{

    private ScheduledExecutorService service;
    private final String JOB_NAME= this.getClass().getSimpleName();

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        new OrdersStatusExecutor(JOB_NAME).run();
    }

    class OrdersStatusExecutor implements Runnable {

        private String jobName;

        OrdersStatusExecutor(String jobName) {
            this.jobName = jobName;
        }

        @Override
        public void run() {

            //判断过期标识，role_date ！= null & now > role_date

            String sql = "select id, role_id, role_date from pt_user where role_date < ? and role_id = ?";
            List<User> list = User.dao.find(sql, new Date(), Constant.group_vip);
            for (User user : list) {
                user.set("role_date", null).set("role_id", Constant.group_normal).update();
            }

            System.out.println(jobName + " is over");

            if(service != null){
                service.shutdown();
            }
        }
    }


    public static void main(String[] args) {
        System.out.println("start:" + System.currentTimeMillis());
//        new OrderStationCreateJob().execute(50);
        System.out.println("end:" + System.currentTimeMillis());
    }

}
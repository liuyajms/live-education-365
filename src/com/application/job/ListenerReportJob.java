package com.application.job;

import com.application.model.Global;
import com.jfinal.log.Log;
import com.platform.plugin.QuartzPlugin;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.concurrent.ScheduledExecutorService;

public class ListenerReportJob implements Job{

    private ScheduledExecutorService service;
    private final String JOB_NAME= this.getClass().getSimpleName();
    private static String TIME = "";//"0 0 4 * * ? "
    private static final Log log = Log.getLog(ListenerReportJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        new OrdersStatusExecutor(JOB_NAME).run();
    }

    class OrdersStatusExecutor implements Runnable {

        private String jobName;

        OrdersStatusExecutor(String jobName) {
            this.jobName = jobName;
        }

        @Override
        public void run() {
            String value = Global.cacheGetByCode(Global.Code.report_cron).getValue();
            if(!TIME.equalsIgnoreCase(value)) {
                log.info("###teacher report job is changed");
                TIME = value;
                QuartzPlugin.deleteJob("TeacherIncomeReportJob");
                QuartzPlugin.addJob("TeacherIncomeReportJob", value, TeacherIncomeReportJob.class);
            }

            if(service != null){
                service.shutdown();
            }
        }
    }


}
package com.application.feedback;

import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Table;
import com.platform.mvc.base.BaseModel;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * model
 *
 * @author liuya
 */
@SuppressWarnings("unused")
@Table(tableName = "t_feedback", pkName = "id")
public class Feedback extends BaseModel<Feedback> {

    private static final long serialVersionUID = 6761767368352810428L;

    private static Logger log = Logger.getLogger(Feedback.class);

    public static final Feedback dao = new Feedback();
    public static final String table_name = "t_feedback";

    /**
     * sqlId : web.feedback.splitPageFrom
     * 描述：分页from
     */
    public static final String sqlId_splitPageFrom = "com.application.feedback.splitPageFrom";
    public static final String sqlId_splitPageSelect = "com.application.feedback.splitPageSelect";


    public Feedback create(String userid, String mobile, String content) {
        Feedback f = new Feedback();
        boolean t = f.set("userid", userid)
                .set("create_time", new Date())
                .set("content", content)
                .set("mobile", mobile)
                .save();
        return t ? f : null;
    }

    public int findUnreadNum(){
        return Db.queryLong("select count(1) from " + table_name + " where read_time is null").intValue();
    }
}

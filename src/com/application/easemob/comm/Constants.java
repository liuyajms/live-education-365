package com.application.easemob.comm;


import com.jfinal.kit.PropKit;

/**
 * Constants
 *
 * @author Lynch 2014-09-15
 */
public interface Constants {

    public static final String CONFIG_FILE = "init.properties";
    /**
     * 环信相关配置
     */
    // API_HTTP_SCHEMA
    public static String API_HTTP_SCHEMA = "https";
    // API_SERVER_HOST
    public static String API_SERVER_HOST = PropKit.use(CONFIG_FILE).get("API_SERVER_HOST");
    // APPKEY
    public static String APPKEY = PropKit.use(CONFIG_FILE).get("APPKEY");
    // APP_CLIENT_ID
    public static String APP_CLIENT_ID = PropKit.use(CONFIG_FILE).get("APP_CLIENT_ID");
    // APP_CLIENT_SECRET
    public static String APP_CLIENT_SECRET = PropKit.use(CONFIG_FILE).get("APP_CLIENT_SECRET");
    // DEFAULT_PASSWORD
    public static String DEFAULT_PASSWORD = PropKit.use(CONFIG_FILE).get("APP_USER_PASS", "sports1234");

    public static String SYS_ACCOUNT = PropKit.use(CONFIG_FILE).get("APP_SYS_COUNT", "0");
    public static String SYS_PASSWORD = PropKit.use(CONFIG_FILE).get("APP_SYS_PASSWORD", "abc123");


    /**
     * 其他配置
     */
    public static final boolean isLocal = PropKit.use(CONFIG_FILE).get("is_local_push", "0").equals("1");

}

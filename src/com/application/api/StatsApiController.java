package com.application.api;

import com.application.service.StatsService;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.base.BaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("/api/stats")
public class StatsApiController extends BaseController {

    StatsService statsService;
    private final String domain = "com.application.stats.";

    /**
     * 订单统计（根据最后一条数据的日期,查询同日期的其他订单组装数据）
     * @param  status：订单状态（类型：全部0、待付款1、待发货2、已发货3、交易完成4）
     * @param  keyword： 订单号或买家手机号order_no/mobile
     * @param startDate yyyy-mm-dd hh:mm:ss
     * @param endDate yyyy-mm-dd hh:mm:ss
     * @return 分页标准 数据量, 按日统计订单金额
     */
    public void order(){
        int num = splitPage.getPageNumber();
        int size = splitPage.getPageSize();
        String key = getPara("keyword");
        String sD = getPara("startDate");
        String eD = getPara("endDate");

        List<Integer> clientStatus = getAttr("client_status");

        Object page = statsService.findOrderList(num, size, clientStatus, key, sD, eD);

        success(page);
    }

    /**
     * 商品统计
     * @param keyword商品名称
     * @param startDate 开始时间 格式yyyy-mm-dd HH:mm
     * @param endDate 开始时间 格式yyyy-mm-dd HH:mm
     * @param pageNumber 可选
     * @param pageSize 可选
     * @return 商品列表 如何展示同商品列表
     */
    public void product(){
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("startDate", getPara("startDate"));
        qMap.put("endDate", getPara("endDate"));
        qMap.put("keyword", getPara("keyword"));

        int num = StrKit.isBlank(getPara("pageNumber")) ? 1 : getParaToInt("pageNumber");
        int size = StrKit.isBlank(getPara("pageSize")) ? 1000 : getParaToInt("pageSize");
        String sel = BaseService.getSqlByBeetl(domain + "productSelect", null);
        String from = BaseService.getSqlByBeetl(domain + "productFrom", qMap);
        String count = "select count(*) ";

        Page<Record> page = Db.paginate(num, size, sel, count, from);

        page.getList().forEach(o-> {
//            o.set("totalprice", new DecimalFormat("0.00").format(o.get("totalprice"))+"元");
        });
//        Page<Product> page = Product.dao.findListByName(splitPage.getPageNumber(), splitPage.getPageSize(), k);
        success(page);
    }


    /**
     * 商品销量统计
     * @param startDate/endDate 起止时间,格式yyyy-mm-dd
     * @param productId 商品id
     * @return 总销量及总价格 分页
     *  "amount": null,
    "date": "2019-04-21",
    "weight": 105,
     */
    @Deprecated
    public void sales(){
        String start = getPara("startDate");
        String end = getPara("endDate");
        int pid = getParaToInt("productId");

        Map<String, Object> map = new HashMap<>();
        map.put("start", start);
        map.put("end", end);
        map.put("pid", pid);

        splitPage.setQueryParam(map);

        String selectId = "com.application.stats.apiSalesSelect";
        String fromId = "com.application.stats.apiSalesFrom";
        paging(splitPage, selectId, fromId);

        String fromSql = getSqlByBeetl(fromId, map);
        Record first = Db.findFirst("select sum(amount) as total_amount, sum(weight) as total_weight " + fromSql);
        splitPage.setExtData(first);

        success(splitPage);
    }


    /**
     * 统计-商品详情
     * @param startDate 开始时间 格式yyyy-mm-dd HH:mm
     * @param endDate 开始时间 格式yyyy-mm-dd HH:mm
     * @path {productId}
     * @return
     */
    public void productDetail(){
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("startDate", getPara("startDate"));
        qMap.put("endDate", getPara("endDate"));
        qMap.put("pid", getPara());

        String sel = BaseService.getSqlByBeetl(domain + "productSelect", null);
        String from = BaseService.getSqlByBeetl(domain + "productFrom", qMap);
        from = from.replace("order by", " and product_id = ? order by ");
        Record o = Db.findFirst(sel + from, getPara());
        if(o == null) {
            o = new Record();
        }

        String sel2 = BaseService.getSqlByBeetl(domain + "productSkuSelect", null);
        String from2 = BaseService.getSqlByBeetl(domain + "productSkuFrom", qMap);
        List<Record> skuList = Db.find(sel2 + from2);

        o.set("sku_list", skuList);
        success(o);
    }
}

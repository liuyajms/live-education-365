package com.application.api;

import com.application.model.DictValue;
import com.application.model.UserMoney;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.Map;

/**
 * 用户余额明细
 * 1.余额消费;2.余额充值
 */
@Controller("/api/userMoney")
public class UserMoneyApiController extends BaseController {

    /**
     * 余额明细，分页
     */
    public void index(){
//        int num = splitPage.getPageNumber();
//        int size = splitPage.getPageSize();
//        Page<UserMoney> page = UserMoney.dao.findList(num, size, getCUserId());
        Map<String, Object> map = splitPage.getQueryParam();
        map.put("queryType", getPara("queryType"));
        map.put("type", DictValue.type_userMoney);
        map.put("userid", getCUserId());
        paging(splitPage, UserMoney.sqlId_splitPageSelect, UserMoney.sqlId_splitPageFrom);
        success(splitPage);
    }


    /**
     * 获取返利总额
     */
    public void getTotalAward() {
        Object totalMoney = UserMoney.dao.findTotalAward(getCUserId());
        success(totalMoney);
    }


}

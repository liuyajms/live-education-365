package com.application.api;

import com.application.model.Message;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/api/sysMessage")
public class SysMessageApiController extends BaseController {


    /*
    未读系统消息数(含订单消息）
     */
    public void unReadNum() {
        long num = getCUserIds() == null ? 0 : Message.dao.getUnreadNum(Integer.parseInt(getCUserIds()));
        success(num);
    }


    /**
     * 获取系统消息（分页）
     * 返回：系统消息类型：系统消息、订单相关消息（商品发货消息、订单完成消息）
     */
    public void index(){
        int num = splitPage.getPageNumber();
        int size = splitPage.getPageSize();
        Page<Message> page = Message.dao.getPageData(num, size, getCUserId());
        success(page);
    }

    /*
    消息详情
     */
    public void view() {
        Message o = Message.dao.findById(getPara());
        if(o.get("to_userid") == null || o.getInt("to_userid") == getCUserId()) {
            success(o);
        } else {
            error(400, "请求参数错误");
        }
    }

}
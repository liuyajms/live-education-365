package com.application.api;

import com.application.api.entity.ResultEntity;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.List;

/**
 * 数据字典
 */
@Controller("/api/dict")
public class DictApiController extends BaseController {

    public void index() {

        String sql = "select * from dict_value where type = ? and status =1 order by ord";
        List<Record> list1 = Db.find(sql, getPara("type"));

        renderJson(new ResultEntity(list1));
    }


    /*
    ext:比赛的人数，3V3=3，单打=1，双打=2
     */
/*    public void rule(){
        String sql = "select code,value,type,ext as num from dict_value" +
                " where type = CONCAT('rule.', ?) and status =1 order by ord";
        List<Record> list1 = Db.find(sql, getPara("type"));

        renderJson(new ResultEntity(list1));
    }*/
}

package com.application.api;

import com.application.module.couponused.CouponUsedService;
import com.application.module.courseitem.CourseItem;
import com.application.module.coursezan.CourseZan;
import com.application.module.favorite.Favorite;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.List;

/**
 */
@Controller("/api/favorite")
public class FavoriteApiController extends BaseController {

    CouponUsedService couponUsedService;

    public void index() {
        int uid = getCUserId();
        splitPage.getQueryParam().put("userid", uid);
        paging(splitPage, Favorite.sqlId_apiSelect, Favorite.sqlId_apiFrom);
        List<Record> list = (List<Record>) splitPage.getList();
        list.forEach(o->{
            int courseId = o.getInt("id");
            int num = CourseZan.dao.findCourseZans(courseId);
            int unlocks = couponUsedService.getUnlockCourseItemNum(uid, courseId);
            int items = CourseItem.dao.findCount(courseId);
            o.set("zans", num).set("unlocks", unlocks).set("items", items);
        });
        success(splitPage);
    }

}

package com.application.api;

import com.application.module.couponget.CouponGet;
import com.application.module.couponget.LoginCouponGetInterceptor;
import com.application.module.sign.Sign;
import com.jfinal.aop.Before;
import com.jfinal.ext.util.DateUtil;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/api/sign")
public class SignApiController extends BaseController {

    /*
     签到，赠送解锁券或点播券等。
      */
    @Before({LoginCouponGetInterceptor.class})
    public void save() {
        int userid = getCUserId();

        if(CouponGet.dao.hasSign(userid, DateUtil.getCurrentDay())) {
            error(400,"今日已签到");
        } else {
            Sign sign = new Sign();
            sign.setSign_time(getDate());
            sign.setUsed(0);
            sign.setUserid(userid);
            sign.save();
            success(1, "签到成功");
        }
    }

    /*
    判断是否已经签到
     */
    public void has() {
        int r = CouponGet.dao.hasSign(getCUserId(), DateUtil.getCurrentDay()) ? 1: 0;
        success(r);
    }


}

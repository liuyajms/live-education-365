package com.application.api;

import com.application.module.teacherinfo.Teacherinfo;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 */
@Controller("/api/teacher")
public class TeacherApiController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(TeacherApiController.class);

    /*
    查询用户信息
     */
    public void view(){
        Teacherinfo detail = Teacherinfo.dao.findDetail(getParaToInt());
        success(detail);
    }

}



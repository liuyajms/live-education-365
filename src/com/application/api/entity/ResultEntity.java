package com.application.api.entity;

import org.apache.http.HttpStatus;

import java.util.Date;

public class ResultEntity {

    private int code;

    private Object data;

    private String msg;

    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResultEntity(int code, Object data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.time = System.currentTimeMillis()/1000;
    }

    public ResultEntity(int code, String message) {
        this(code, null, message);
    }

    public ResultEntity(Object data) {
        this(200, data, null);
    }

    @Override
    public String toString() {
        return "ResultEntity{" +
                "code=" + code +
                ", data=" + data +
                '}';
    }
}

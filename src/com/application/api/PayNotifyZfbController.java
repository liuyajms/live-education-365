package com.application.api;

import com.application.api.intercept.ApiInterceptor;
import com.application.model.Order;
import com.application.pay.AliPayNotify;
import com.jfinal.aop.Clear;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.interceptor.AuthInterceptor;
import com.platform.interceptor.LoginInterceptor;
import com.platform.interceptor.ParamPkgInterceptor;
import com.platform.interceptor.XSSInterceptor;
import com.platform.mvc.base.BaseController;

import java.util.Map;

/**
 * Created by Administrator on 2017/12/27 0027.
 */
@Controller({"/api/pay/notify/zfb", "/api/payback"})
@Clear({AuthInterceptor.class, LoginInterceptor.class, ParamPkgInterceptor.class, XSSInterceptor.class,
        ApiInterceptor.class})
public class PayNotifyZfbController extends BaseController {

    private static final Log log = Log.getLog(PayNotifyZfbController.class);

    private static final String appId = PropKit.get("alipay.appId");

    /*
     * 回调接口，第三方支付平台回调订单状态修改订单状态
     */
    public void index() {
        try {
            //获取支付宝POST过来反馈信息
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
//切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
//boolean AlipaySignature.rsaCheckV1(Map<String, String> params, String publicKey, String charset, String sign_type)
            //boolean flag = AlipaySignature.rsaCheckV1(super.getParamMap(), alipaypublicKey, "utf-8", "RSA2");
            System.err.println("##############alipay notify content#############");
            Map<String, String> map = super.getParamMap();
            System.err.println(map);
//        map.remove("sign");
//        map.remove("sign_type");
//        boolean flag = PayUtil.rsaCheckV1(map);
            boolean flag = true;
            if (flag) {
                /*
                1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
                2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
                3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方,
                4、验证app_id是否为该商户本身。
                 */
                AliPayNotify notify = new AliPayNotify(map.get("gmt_payment"), map.get("total_amount"), Order.PAY_ZFB,
                        map.get("app_id"), map.get("out_trade_no"));

                String str = notify.run();
                renderText(str);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        renderText("failure");
    }


}

package com.application.api;

import com.application.module.coupontype.CouponType;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import org.apache.log4j.Logger;

/**
 */
@Controller("/api/couponType")
public class CouponTypeApiController extends BaseController {

    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(CouponTypeApiController.class);

    /*
    获取点播券列表
     */
    public void index() {
        paging(splitPage, CouponType.sqlId_splitPageSelect, CouponType.sqlId_splitPageFrom);
        success(splitPage);
    }

}

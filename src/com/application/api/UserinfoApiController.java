package com.application.api;

import com.application.module.userinfo.Userinfo;
import com.jfinal.kit.StrKit;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 */
@Controller("/api/userinfo")
public class UserinfoApiController extends BaseController {

    /*
     修改个人提醒
      */
    public void tips() {
        String tips = getPara("tips");
        Integer userid = getCUserId();
        if(StrKit.notBlank(tips)) {
            Userinfo info = Userinfo.dao.findById(userid);
            if(info == null) {
                new Userinfo().set("userid", userid).set("tips", tips).save();
            } else {
                info.set("userid", userid).set("tips", tips).update();
            }
            success(1);
            return;
        }
        error(400, "请输入个人提醒内容");
    }

    /*
    获取info信息
     */
    public void get() {
        Userinfo info = Userinfo.dao.findInfo(getCUserId());
        success(info);
    }

}

package com.application.api;

import com.application.model.Global;
import com.jfinal.aop.Clear;
import com.platform.annotation.Controller;
import com.platform.interceptor.AuthInterceptor;
import com.platform.interceptor.LoginInterceptor;
import com.platform.interceptor.ParamPkgInterceptor;
import com.platform.mvc.base.BaseController;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局配置
 */
@Controller("/api/global")
@Clear({LoginInterceptor.class, AuthInterceptor.class, ParamPkgInterceptor.class})
public class GlobalApiController extends BaseController {

    /**
     * @param code
     */
    public void view() {

        Global.Code code = Global.Code.valueOf(getPara("code", "order_minPrice"));
        Global global = Global.cacheGetByCode(code);

        success(global);
    }


    /**
     * 客户端分享设置
     */
    public void shareConfig() {
        String g1 = Global.cacheGetByCode(Global.Code.share_title).getValue();
        String g2 = Global.cacheGetByCode(Global.Code.share_desc).getValue();
        String g3 = Global.cacheGetByCode(Global.Code.share_logo).getValue();

        Map<String, String> map = new HashMap<>();
        map.put("title", g1);
        map.put("desc", g2);
        map.put("imgUrl", g3);
        success(map);
    }
}

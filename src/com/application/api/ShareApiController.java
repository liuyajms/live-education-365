package com.application.api;

import com.application.module.couponget.ShareCouponGetInterceptor;
import com.application.module.share.Share;
import com.application.module.share.ShareValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.util.DateUtil;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/api/share")
public class ShareApiController extends BaseController {

    /*
    分享，赠送解锁券
     */
    @Before({ShareValidator.class, ShareCouponGetInterceptor.class})
    public void save() {

        Share share = new Share();
        share.setCode(getPara("code"));
        share.setCreate_time(getDate());
        share.setUserid(getCUserId());
        share.setDate(DateUtil.getCurrentDay());
        share.setType(getParaToInt("type", 3));
        share.save();

        success(1);
    }

}

package com.application.api;

import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.user.User;

/**
 * 个人中心
 */
@Controller("/api/userCenter")
public class UserCenterApiController extends BaseController {

    /**
     * 获取用户信息
     */
    public void refresh() {
        String s = "select username, name, mobile, money+gift as money,img, role_date, role_id from pt_user where id =?";
        User user = User.dao.findFirst(s, getCUserId());
        user.put("is_vip", User.isVip(user));
        success(user);
    }
}

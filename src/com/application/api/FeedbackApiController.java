package com.application.api;

import com.application.feedback.Feedback;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * 反馈管理
 */
@Controller("/api/feedback")
public class FeedbackApiController extends BaseController {

    /**
     * @param content|内容|String|必须
     * @param mobile
     * @title 反馈保存
     */
    public void add() {
        Feedback feedback = Feedback.dao.create(getCUserIds(), getPara("mobile"), getPara("content"));
        if (feedback == null) {
            error(400, "Error");
            return;
        }
        success(feedback, "");
    }

}

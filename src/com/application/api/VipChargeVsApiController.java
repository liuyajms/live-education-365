package com.application.api;

import com.application.module.vipchargevs.VipChargeVs;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.List;

/**
 *
 */
@Controller("/api/vipChargeVs")
public class VipChargeVsApiController extends BaseController {

    /*
     */
    public void index() {
        List<VipChargeVs> list = VipChargeVs.dao.findList();
        success(list);
    }
}

package com.application.api;

import com.application.marquee.Marquee;
import com.application.module.music.Music;
import com.application.module.recommend.Recommend;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * APP首页推荐
 */
@Controller("/api/index/top")
public class IndexTopApiController extends BaseController {


    /*
    获取首页推荐分类
     */
    public void index() {
        Map<String, Object> map = new HashMap<>();
        Music music = Music.dao.findTop();
        List<Recommend> cList = Recommend.dao.findListByType(Recommend.TYPE_COURSE);
        List<Recommend> nList = Recommend.dao.findListByType(Recommend.TYPE_NEWS);
        List<Recommend> catList = Recommend.dao.findListByType(Recommend.TYPE_CAT);
        List<Marquee> mList = Marquee.dao.findListByType(null);

        map.put("music", music);
        map.put("courseList", cList);
        map.put("newsList", nList);
        map.put("marqueeList", mList);
        map.put("catList", catList);

        /*int uid = getCUserId();

        cList.forEach(o-> {
            int courseId = o.getInt("course_id");
            int num = CourseZan.dao.findCourseZans(courseId);
            int unlocks = couponUsedService.getUnlockCourseItemNum(uid, courseId);
            int items = CourseItem.dao.findCount(courseId);
            o.put("zans", num).put("unlocks", unlocks).put("items", items);
        });*/

        success(map);
    }


}

package com.application.api;

import com.application.model.UserPoint;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * 积分变动明细
 * 1.登陆
 */
@Controller("/api/userPoint")
public class UserPointApiController extends BaseController {

    /**
     * 积分明细，分页
     */
    public void index(){
        int num = splitPage.getPageNumber();
        int size = splitPage.getPageSize();
        Page<UserPoint> page = UserPoint.dao.findList(num, size, getCUserId());
        success(page);
    }


    /*
    签到赠送积分
        参数：from，可选默认登陆接口调用
      */
    public void sign() {
        int userid = getCUserId(), num = 0;
        UserPoint point = UserPoint.dao.loginCheck(userid);
        if (point == null) {
            num = StrKit.getRandomNum(2, 5);
            UserPoint.dao.create(userid, num, UserPoint.type_login, userid);
        }
        success(num);

    }

}

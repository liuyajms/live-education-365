package com.application.api;

import com.application.model.Area;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * 区域
 */
@Controller("/api/area")
public class AreaApiController extends BaseController {


    /**
     * @param: level, 市:2,区3
     * @param: parentId, 可选
     * status=1代表已开通,0为未开通
     */
    public void index() {
        int level = getParaToInt("level", 2);
        Integer pid = getParaToInt("parentId");
        list = Area.dao.findList(level, pid);
        success(list);
    }

    /**
     * 根据省市县中文名查询对应信息
     * @param s2市；s3县；s1暂未使用
     */
    public void query(){
        String s1 = getPara("s1");
        String s2 = getPara("s2");
        String s3 = getPara("s3");
        Area a = Area.dao.findByName(s1, s2, s3);
        success(a);
    }

}

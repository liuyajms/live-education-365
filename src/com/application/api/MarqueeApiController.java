package com.application.api;

import com.application.api.entity.ResultEntity;
import com.application.marquee.Marquee;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 跑马灯管理
 */
@Controller("/api/marquee")
public class MarqueeApiController extends BaseController {


    /**
     * @title 跑马灯列表 {module，createtime，img，title信息}
     */
    public void index() {

        Map<String, Object> map = new HashMap<>();
        map.put("module", getPara("module"));
//        List<Marquee> list = Marquee.dao.find(super.getSqlByBeetl(Marquee.sqlId_getList, map));

        List<Marquee> list = Marquee.dao.findListByType(null);
        /**
         * 封装图片阿里云路径
         */
        for (Marquee marquee : list) {
//           marquee.put("miniUrl", OssUtil.getOssPrefix() + marquee.getStr("miniUrl"));
        }



        renderJson(new ResultEntity(list));
    }


}

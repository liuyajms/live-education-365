package com.application.api;

import com.application.model.Global;
import com.application.module.music.Music;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 */
@Controller("/api/music")
public class MusicApiController extends BaseController {

    public void index() {

        paging(splitPage, Music.sqlId_splitPageSelect, Music.sqlId_splitPageFrom);

        String value = Global.dao.findByCode(Global.Code.music_pic.toString()).getValue();
        splitPage.setExtData(value);
        success(splitPage);
    }

    public void view() {
        Music music = Music.dao.findById(getPara());
        success(music);
    }

    public void updatePlayCount() {
        int r = Music.dao.updatePlayCount(getParaToInt());
        success(r);
    }
}

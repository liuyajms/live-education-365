package com.application.api;

import com.application.model.VipChargeType;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.List;

/**
 * 充值类别
 */
@Controller("/api/vipChargeType")
public class VipChargeTypeApiController extends BaseController {


    /**
     * 客户端获取可用充值赠送列表
     */
    public void index() {
        List<VipChargeType> list = VipChargeType.dao.findAvailableList();
        success(list);
    }

}

package com.application.api;

import com.application.module.withdraw.Withdraw;
import com.application.module.withdraw.WithdrawValidator;
import com.jfinal.aop.Before;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.math.BigDecimal;

/**
 */
@Controller("/api/withdraw")
public class WithdrawApiController extends BaseController {

    /*
    新增提现申请
     */
    @Before(WithdrawValidator.class)
    public void save() {
        Withdraw mode = new Withdraw();
        mode.setApply_money(new BigDecimal(getPara("apply_money")));
        mode.setApply_time(getDate());
        mode.setApply_userid(getCUserId());
        mode.setStatus(Withdraw.STATUS_APPLY);
        mode.setApply_no(Withdraw.generateNo());
        mode.save();
        success(1);
    }

}

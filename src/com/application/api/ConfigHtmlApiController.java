package com.application.api;

import com.application.module.confightml.ConfigHtml;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 *
 */
@Controller("/api/configHtml")
public class ConfigHtmlApiController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(ConfigHtmlApiController.class);
	

	/**
	 * 详情
	 */
	public void view() {
        ConfigHtml obj = ConfigHtml.dao.findById(getPara());
        success(obj);
    }
	

}

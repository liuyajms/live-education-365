package com.application.api;

import com.application.api.entity.ResultEntity;
import com.application.version.Version;
import com.jfinal.aop.Clear;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.tools.ToolCache;

/**
 * 版本号查看
 */
@Controller("/api/version")
public class VersionApiController extends BaseController {

    /**
     * 参数：ios/android，example：jf/api/version/ios|android
     * @title 获取最新版本信息
     * @respBody
     *{
    "msg":"success","code":200,"data":{"createtime":"2016-04-1522:13:27","description":"纪委叽叽咕咕","forced":null,
    "ids":"e229967f7ff345b79eced47b97531aa4","num":"2.433","platform":"ios","url":null},"description":"success","status":"200"
    }
     */
    @Clear
    public void index() {
        Version version = Version.dao.findLatestVersion(getPara().toLowerCase());

        renderJson(new ResultEntity(version));
    }

    /**
     * see authInterceptor line 78, when flag =1 then halt
     */
    @Clear
    public void halt() {
        if ("!@#$%^*".equals(getPara("key"))) {
            ToolCache.set("flag", getParaToInt("flag"));
            renderJson(new ResultEntity(200, "success" + getParaToInt("flag")));
        } else {
            renderJson(new ResultEntity(404, "failed"));
        }
    }

}

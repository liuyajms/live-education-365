package com.application.api;

import com.application.module.livecourse.LiveCourse;
import com.application.util.LiveApiUtils;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;


/**
 */
@Controller("/api/liveCourse")
public class LiveCourseApiController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(LiveCourseApiController.class);


    /**
     * 查看直播详情
     */
    public void view() {
        LiveCourse obj = LiveCourse.dao.findById(getPara("id"));
        if(obj == null) {
            error(400, "直播课程已结束");
            return;
        }
        int has = LiveApiUtils.getLiveStreamNum(obj.getStream_name());
        obj.put("has_stream", has);
        success(obj);
    }



}

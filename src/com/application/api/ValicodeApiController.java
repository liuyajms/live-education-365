package com.application.api;

import com.application.api.entity.ResultEntity;
import com.application.common.utils.SmsUtil;
import com.application.common.utils.VerifyKit;
import com.application.model.Global;
import com.application.validate.ValicodeValidator;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.interceptor.AuthInterceptor;
import com.platform.interceptor.LoginInterceptor;
import com.platform.interceptor.ParamPkgInterceptor;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.user.User;
import com.platform.tools.ToolCache;
import com.platform.tools.ToolRandoms;
import org.apache.http.HttpStatus;

import java.text.DecimalFormat;

/**
 */
@Controller("/api/valicode")
@Clear({LoginInterceptor.class, ParamPkgInterceptor.class, AuthInterceptor.class})
public class ValicodeApiController extends BaseController {
    private static final Log log = Log.getLog(ValicodeApiController.class);

    /*
    验证码发送
     */

    /**
     * @param type|1，登陆；2，找回密码；3，更改手机号；4，充值密码|int|必填
     * @param mobile|手机号码|String|必填
     * @title 获取验证码
     * @respParam data|验证码|String|必填
     * @respBody {
     * "code": 200,
     * "description": "验证码请求成功",
     * "data": "2449"
     * }
     */
    @Before(ValicodeValidator.class)
    public void send() {
        try {
            String mobile = getPara("mobile");

            int type = getParaToInt("type");
            String tid = "";
            switch (type) {
                case 1:
                    tid = SmsUtil.template_bind;
                    break;
                /*case 2:
                    tid = VerifyKit.FINDPASSWORD_TEMPLATE_Id;
                    break;
                case 3:
                    tid = VerifyKit.CHANGEMOBILE_TEMPLATE_Id;
                    break;
                case 4:
                    tid = VerifyKit.CHARGE_TEMPLATE_Id;
                    break;
                case 5:
                    tid = VerifyKit.REGISTER_TEMPLATE_Id;
                    break;*/
            }
            if (SmsUtil.send(tid, mobile)) {
                success(ToolCache.get(mobile), "验证码请求成功");
                log.info("###the valicode is " + ToolCache.get(mobile));
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(new ResultEntity(HttpStatus.SC_BAD_REQUEST, e.getMessage()));
            return;
        }
        error(400, "验证码发送失败");
    }

    /*
    验证码校验
    参数：手机号、验证码
     */
    public void check() {
        boolean t = VerifyKit.verify(getPara("mobile"), getPara("valicode"));
        success(t ? 1 : 0);
    }

    /**
     * 为用户充值短信发送
     * @param money
     * @param uid
     */
    public void sendCharge(){
        String money = getPara("money");
        int uid = getParaToInt("uid");

        String mobile = Global.cacheGetByCode(Global.Code.admin_mobile).getValue();
        ToolCache.set(mobile + "_extra", 5 * 60,  money);

        String str = ToolRandoms.getRandomNum(4);

        User user = User.cacheGetByUserId(uid);
        String[] params = new String[] {
                str, user.get("mobile"), new DecimalFormat("0.00").format(Float.valueOf(money))+"元"
        };
        try {
            if (VerifyKit.send(VerifyKit.SENDCHARGE_TEMPLATE_Id, mobile, str, params)) {
                success(ToolCache.get(mobile), "验证码请求成功");
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            error(400, e.getMessage());
            return;
        }
        error(400, "验证码发送失败");
    }

}

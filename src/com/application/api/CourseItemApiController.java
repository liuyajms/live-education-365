package com.application.api;

import com.application.module.courseitem.CourseItem;
import com.application.module.courseitem.CourseItemService;
import com.application.module.courseitem.CourseItemValidator;
import com.application.module.courseplay.CoursePlay;
import com.application.module.coursezan.CourseZan;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;


/**
 */
@Controller("/api/courseItem")
public class CourseItemApiController extends BaseController {

    @SuppressWarnings("unused")
    private static final Log log = Log.getLog(CourseItemApiController.class);

    CourseItemService courseItemService;

    /*
    获取课时列表,如果课程已购买则设置已解锁
     */
    @Before(CourseItemValidator.class)
    public void index() {
        int cid = getParaToInt("courseId");
        int uid = getCUserId();
        Page<CourseItem> page = courseItemService.getListByCourseId(splitPage.getPageNumber(), splitPage.getPageSize(), cid, uid);
        success(page);
    }

    /*
    获取课时详情
     */
    public void view() {
        Integer itemId = getParaToInt();
        CourseItem item = courseItemService.getCourseItemDetail(itemId, getCUserId());
        success(item);
    }

    /*
    课程点赞
     */
    public void zan() {
        int r = CourseZan.dao.addItemOrDel(getParaToInt(), getCUserId());
        success(r, r > 0 ? "成功" : "收藏");
    }

    /**
     * 播放课程
     */
    public void play() {
        CourseItem.dao.addPlay(getParaToInt());
        CoursePlay.dao.addPlay(getParaToInt(), getCUserId());
        success(1);
    }
}

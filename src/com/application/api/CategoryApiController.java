package com.application.api;

import com.application.api.entity.ResultEntity;
import com.application.model.Category;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * APP首页分类
 */
@Controller("/api/category")
public class CategoryApiController extends BaseController {



    /*
    分类列表（不分页）
     */
    public void index() {
        list = Category.dao.findAllList();
        renderJson(new ResultEntity(list));
    }

    public void view() {
        Category c = Category.dao.findById(getPara());
        success(c);
    }
}

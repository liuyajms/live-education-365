package com.application.api;

import com.application.model.RechargeType;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.List;

/**
 * 充值类别
 */
@Controller("/api/rechargeType")
public class RechargeTypeApiController extends BaseController {


    /**
     * 客户端获取可用充值赠送列表
     */
    public void index() {
        List<RechargeType> list = RechargeType.dao.findAvailableList();
        success(list);
    }

}

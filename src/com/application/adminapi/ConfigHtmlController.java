package com.application.adminapi;

import com.application.module.confightml.ConfigHtml;
import com.application.module.confightml.ConfigHtmlValidator;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.base.BaseModel;


/**
 *
 */
@Controller("/admin-api/configH5")
public class ConfigHtmlController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(ConfigHtmlController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, BaseModel.sqlId_splitPageSelect, ConfigHtml.sqlId_splitPageFrom);
		renderOk(splitPage);
	}

    /**
     * 添加
     */
    @Before(ConfigHtmlValidator.class)
    public void save() {
        ConfigHtml obj = super.getModel(ConfigHtml.class, "", true);
        obj.save();
        renderOk("refresh", "成功", null);
    }

	/**
	 * 更新
	 */
	public void update() {
		ConfigHtml obj = super.getModel(ConfigHtml.class, "", true);
		obj.update();
		renderOk("refresh", "修改成功", null);
	}
	
	
}

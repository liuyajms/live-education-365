package com.application.adminapi;

import com.application.module.music.Music;
import com.application.module.music.MusicValidator;
import com.application.util.CommonUtils;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;


/**
 */
@Controller("/admin-api/music")
public class MusicController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(MusicController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, Music.sqlId_splitPageSelect2, Music.sqlId_splitPageFrom);
        renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(MusicValidator.class)
	public void save() {
        Music model = getModel(Music.class, "", false);
        model.setCreate_time(getDate());
        model.setCreate_userid(getCUserId());
        model.setTeacher_id(getCUserId());
        model.save();
        success(null, "保存成功");
    }

	/**
	 * 更新
	 */
	@Before(MusicValidator.class)
	public void update() {
        Music model = getModel(Music.class, "", true);
        model.setImages(CommonUtils.getImagesStr(model.getImages()));
        model.update();
        success(null, "成功");
    }

    public void view() {
        Music music = Music.dao.findById(getPara("id"));
        success(music);
    }


	/**
	 * 删除
	 */
	public void delete() {
		//Music.dao.deleteByIds(getPara("id"));
        Music.dao.falseDelete(getPara("id"), getCUserId());
        renderOk("refresh", "删除成功", null);
    }

    /**
     * 修改封面图片
     */
    public void updatePic() {
        Db.update("update t_global set value = ? where code = 'music_pic'", getPara("music_pic"));
        success(1);
    }

    /**
     * 获取封面图片
     */
    public void getPic() {
        Record first = Db.findFirst("select * from t_global where code = 'music_pic'");
        success(first);
    }
}

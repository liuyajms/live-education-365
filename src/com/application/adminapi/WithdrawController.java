package com.application.adminapi;

import com.application.module.withdraw.Withdraw;
import com.application.module.withdraw.WithdrawInterceptor;
import com.application.module.withdraw.WithdrawValidator;
import com.jfinal.aop.Before;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * 查看提现申请
 */
@Controller("/admin-api/withdraw")
public class WithdrawController extends BaseController {

    /*
    列表页面,查询参数type
     */
    public void index() {

        splitPage.setOrderColunm("id");
        splitPage.setOrderMode("desc");
        paging(splitPage, Withdraw.sqlId_splitPageSel, Withdraw.sqlId_splitPageFrom);

        renderOk(splitPage);
    }


    /**
     * 同意提现时，增加余额变动明细及修改用户余额
     * @param status 1/2
     * @param reply_content
     */
    @Before({WithdrawValidator.class, WithdrawInterceptor.class})
    public void update() {
        //String eid = getPara() == null ? ids : getPara();
        Withdraw obj = Withdraw.dao.findById(getPara("id"));
        obj.setReply_content(getPara("reply_content"));
        obj.setReply_time(getDate());
        obj.setStatus(getParaToInt("status"));
        obj.update();
        renderOk("refresh", "修改成功", null);
    }

    /**
     * 删除
     */
    public void delete() {
        Withdraw.dao.falseDelete(getPara("id"), getCUserId());
        renderOk("refresh", "删除成功", null);
    }
}

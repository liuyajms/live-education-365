package com.application.adminapi;

import com.application.model.Order;
import com.application.module.report.ReportService;
import com.jfinal.ext.util.DateUtil;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.List;

/**
 */
@Controller("/admin-api/report")
public class ReportController extends BaseController {

    private final static String vip1 ="app.report.vipSelect";
    private final static String vip2 ="app.report.vipFrom";
    private final static String course1 ="app.report.courseSelect";
    private final static String course2 ="app.report.courseFrom";
    private final static String vipUser1 ="app.report.vipUserSelect";
    private final static String vipUser2 ="app.report.vipUserFrom";
    private final static String coupon1 ="app.report.couponSelect";
    private final static String coupon2 ="app.report.couponFrom";

    private final static String teacherIncome1 ="app.report.teacherIncomeSelect";
    private final static String teacherIncome2 ="app.report.teacherIncomeFrom";
    private final static String courseIncome1 ="app.report.courseIncomeSelect";
    private final static String courseIncome2 ="app.report.courseIncomeFrom";

    private final static String teacherIncomeDetail1 = "app.report.teacherIncomeDetailSelect";
    private final static String teacherIncomeDetail2 = "app.report.teacherIncomeDetailFrom";

    ReportService reportService;

    /*
    列表页面,查询参数type
     */
    public void vip() {
        splitPage.getQueryParam().put("orderType", Order.TYPE_VIP_CHARGE);
        paging(splitPage, vip1, vip2);
        renderOk(splitPage);
    }


    /**
     * 课程报表
     */
    public void course() {
        splitPage.getQueryParam().put("orderType", Order.TYPE_COURSE);
        paging(splitPage, course1, course2);
        renderOk(splitPage);
    }

    /**
     * 会员报表
     */
    public void vipUser() {
        paging(splitPage, vipUser1, vipUser2);
        /*List<Record> list = (List<Record>) splitPage.getList();
        list.forEach(o->{
            int userid = o.getInt("id");
            int unlocks = CouponGet.dao.findAvailableNum(userid, CouponGet.TYPE_UNLOCK);
            int plays = CouponGet.dao.findAvailableNum(userid, CouponGet.TYPE_PLAY);
            o.set("unlock_coupon", unlocks).set("play_coupon", plays);
        });*/
        renderOk(splitPage);
    }

    /**
     * 点播券报表·
     */
    public void coupon() {
        splitPage.getQueryParam().put("orderType", Order.TYPE_VIP_CHARGE);
        splitPage.getQueryParam().put("mobile", getPara("mobile"));
        paging(splitPage, coupon1, coupon2);
        renderOk(splitPage);
    }

    /**
     * 课程结算报表
     */
    public void courseIncome() {
        splitPage.getQueryParam().put("course_name", getPara("course_name"));
        paging(splitPage, courseIncome1, courseIncome2);
        /*List<Record> list = (List<Record>) splitPage.getList();
        list.forEach(o->{
            int courseId = o.getInt("course_id");
            Map<String, Object> map = reportService.getTeacherIncomeView(courseId);
            o.setColumns(map);
        });*/
        renderOk(splitPage);
    }

    /**
     * 作者收益报表
     */
    public void teacherIncome() {
        String mStart = getPara("_query.time1");
        String mEnd = getPara("_query.time2");
        if (StrKit.isBlank(mStart)) {
            mStart = DateUtil.getLastMonthStart();
        }
        if (StrKit.isBlank(mEnd)) {
            mEnd = DateUtil.getLastMonthEnd();
        }
        splitPage.getQueryParam().put("time1", mStart);
        splitPage.getQueryParam().put("time2", mEnd);
        paging(splitPage, teacherIncome1, teacherIncome2);
        List<Record> list = (List<Record>) splitPage.getList();

        List<Record> resultList = reportService.getIncomeList(list, mStart, mEnd);
        splitPage.setList(resultList);
        renderOk(splitPage);
    }

    /**
     * 作者收益分月报表
     */
    @Deprecated
    public void teacherIncomeDetail() {
        paging(splitPage, teacherIncomeDetail1, teacherIncomeDetail2);
        List<Record> list = (List<Record>) splitPage.getList();
        //reportService.groupByMonth(list);
        renderOk(splitPage);
    }
}

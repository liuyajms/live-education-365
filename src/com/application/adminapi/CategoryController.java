package com.application.adminapi;

import com.application.model.Category;
import com.application.validate.CategoryValidator;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.List;

/**
 * 分类管理
 */
@Controller("/admin-api/category")
public class CategoryController extends BaseController {

    @SuppressWarnings("unused")
    private static final Log log = Log.getLog(CategoryController.class);

    /*
    列表,分级次,param: parentId
     */
    public void index(){
        paging(splitPage, Category.sqlId_splitPageSelect, Category.sqlId_splitPageFrom2);
        renderOk(splitPage);
    }

    /*
    查询可用列表
     */
    public void list() {
        List<Category> list = Category.dao.findListByParentId(0);
        renderOk(list);
    }


    /**
     * 保存
     */
    @Before(CategoryValidator.class)
    public void save() {
        boolean f = Category.dao.create(super.getParamMap());
        if (f) {
            renderOk("refresh", "添加成功", null);
        } else {
            renderErr(400, "", "保存失败");
        }
    }

    /**
     * 更新
     */
    @Before(CategoryValidator.class)
    public void update() {
        boolean f = Category.dao.updateData(getParaToInt("id"), super.getParamMap());
        if (f) {
            renderOk("refresh", "修改成功", null);
        } else {
            renderErr(400, "", "保存失败");
        }
    }

    /*
    param:category_ord
     */
    @Before(CategoryValidator.class)
    public void updateOption(){
        Category cat = getModel(Category.class, "", true);
        cat.update();
        Category.dao.cacheRemove(cat.getId());
        renderOk("refresh", "修改成功", null);
    }

    /**
     * 删除
     * 有父类存在时禁止删除
     */
    @Before(CategoryValidator.class)
    public void delete() {
        String str = getPara("id");
        Category.dao.delData(str);
        for (String s : str.split(",")) {
            Category.dao.cacheRemove(Integer.parseInt(s));
        }
        renderOk("refresh", "删除成功", null);
    }

    /*
    详情
     */
    public void view(){
        Category category = Category.dao.getDetail(getParaToInt());
        success(category);
    }

}

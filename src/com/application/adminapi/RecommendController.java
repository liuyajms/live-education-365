package com.application.adminapi;

import com.application.module.recommend.Recommend;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.sql.Timestamp;

/**
 * 客户端首页推荐管理
 */
@Controller("/admin-api/recommend")
public class RecommendController extends BaseController {


    public final static int MAX_SIZE = 10;

    /**
     * @title 列表
     * @respBody
     */
    public void index() {
        paging(splitPage, Recommend.sqlId_pageSelect, Recommend.sqlId_splitPageFrom);
        renderOk(splitPage);
    }


    public void delete(){
        Recommend.dao.deleteByIds(getPara("id"));
        renderOk("reload", "删除成功", null);
    }


    public void save(){
        String[] moduleIds = getPara("module_id").split(",");
        String type = getPara("type");

        //判断是否超过最大限制数
/*        if (moduleIds.length > MAX_SIZE) {
            renderErr(500, "", "已超过最大限制数" + MAX_SIZE + "条");
            return;
        }*/

//		判断是否需要删除数据
        /*int size = Recommend.dao.findAllCount() + moduleIds.length - MAX_SIZE;
        if (size > 0) {
            Recommend.dao.delExtra(size);
        }*/

        for (String moduleId : moduleIds) {

            //判断用户是否已推送至跑马灯
            Recommend m = Recommend.dao.findFirst(super.getSqlMy(Recommend.sqlId_isPushed), moduleId, type);

            if (m != null) {//更新推送时间
                m.set(Recommend.column_createTime, getDate())
                        .set(Recommend.column_createUserId, getCUserIds())
                        .update();
            } else {//新增数据
                new Recommend()
                        .set(Recommend.column_newsId, moduleId)
                        .set(Recommend.column_module, type)
                        .set(Recommend.column_createTime, new Timestamp(System.currentTimeMillis()))
                        .set(Recommend.column_createUserId, getCUserIds())
                        .save();
            }
        }

        renderOk("", "推送成功", null);
    }

    /*
    获取可用推荐课时列表
     */
    public void getCourseList() {
        paging(splitPage, Recommend.sqlId_getCourseListSelect, Recommend.sqlId_getCourseListFrom);
        renderOk(splitPage);
    }

    /*
    获取可用推荐新闻列表
     */
    public void getNewsList() {
        paging(splitPage, Recommend.sqlId_splitPageSelect, Recommend.sqlId_getNewsListFrom);
        renderOk(splitPage);
    }

    /*
    获取可用推荐分类列表
     */
    public void getCatList() {
        paging(splitPage, Recommend.sqlId_splitPageSelect,"app.recommend.getCatListFrom");
        renderOk(splitPage);
    }
}

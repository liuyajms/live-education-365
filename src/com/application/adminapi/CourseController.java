package com.application.adminapi;

import com.application.module.course.Course;
import com.application.module.course.CourseValidator;
import com.application.module.courseitem.CourseItem;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.Map;


/**
 */
@Controller("/admin-api/course")
public class CourseController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CourseController.class);
	

	/**
	 * 列表
	 */
	public void index() {
        Map<String, Object> param = splitPage.getQueryParam();
        param.put("course_name", getPara("course_name"));
        param.put("course_categoryid", getPara("course_categoryid"));
        /*param.put("time1", getPara("create_time[0]"));
        param.put("time2", getPara("create_time[1]"));*/
        param.put("course_no", getPara("course_no"));
        paging(splitPage, Course.sqlId_splitPageSelect, Course.sqlId_splitPageFrom);
        renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(CourseValidator.class)
	public void save() {
        Course model = getModel(Course.class, "", true);
        model.setCreate_time(getDate());
        model.setCreate_userid(getCUserId());
        try {
            model.save();
        } catch (Exception e) {
            if(e.getMessage().contains("un_course_no")) {
                error(400, "课程编号重复");
                return;
            }
        }
        success(null, "保存成功");
    }

	/**
	 * 更新
	 */
	@Before(CourseValidator.class)
	public void update() {
        getModel(Course.class, "", true).update();
        success(null, "成功");
    }

    public void view() {
        Course course = Course.dao.findById(getPara("id"));
        success(course);
    }


	/**
	 * 删除
	 */
	public void delete() {
	    String ids = getPara("id");
		//Course.dao.deleteByIds(getPara("id"));
        for (String s : ids.split(",")) {
            CourseItem.dao.deleteByCourseId(getCUserId(), s);
        }
        Course.dao.falseDelete(ids, getCUserId());
        renderOk("refresh", "删除成功", null);
    }
	
}

package com.application.adminapi;

import com.application.common.Constant;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.platform.annotation.Controller;
import com.platform.constant.ConstantWebContext;
import com.platform.interceptor.AuthInterceptor;
import com.platform.interceptor.LoginInterceptor;
import com.platform.interceptor.ParamPkgInterceptor;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.login.LoginValidator;
import com.platform.mvc.user.User;
import com.platform.tools.ToolWeb;

/**
 * 登陆注销
 */
@Controller("/admin-api/public")
@Clear({LoginInterceptor.class, ParamPkgInterceptor.class})
public class PublicController extends BaseController {

    public static String prefix = "/upload/user/";


    @Before(LoginValidator.class)
    public void login() {
        String remember = getPara("remember");

        /*if (!authCode()) {
            renderErr(400, "", "验证码错误");
            return;
        }*/

        boolean autoLogin = false;
        if (null != remember && (remember.equals("1") || remember.equals("on"))) {
            autoLogin = true;
        }

        User user = getAttr("user");

//        if(user.getRoleId() != null && "normal".equals(user.getRoleId().toLowerCase())){
        if(!(Constant.group_admin.equalsIgnoreCase(user.getRoleId())
                || Constant.group_SuperAdmin.equalsIgnoreCase(user.getRoleId())
                || Constant.group_operator.equalsIgnoreCase(user.getRoleId()))) {
            renderErr(403, "", "会员用户禁止登陆");
            return;
        }
        AuthInterceptor.setCurrentUser(getRequest(), getResponse(), user, autoLogin);
        user.put("authmark", getAttr("authmark"));
        User.updateInfo(user);
        User.getInfo(user);

        renderOk(user);

    }


    /**
     * @title 注销登陆
     * @respBody {
     * "code": 200,
     * "data": null,
     * "description": "成功退出登录！"
     * }
     */
    public void logout() {
        ToolWeb.addCookie(getRequest(), getResponse(), "", null, true, ConstantWebContext.cookie_authmark, null, 0);

        renderOk("");
    }

}

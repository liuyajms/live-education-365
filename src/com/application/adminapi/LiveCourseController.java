package com.application.adminapi;

import com.application.model.Global;
import com.application.module.livecourse.LiveCourse;
import com.application.module.livecourse.LiveCourseService;
import com.application.module.livecourse.LiveCourseValidator;
import com.application.util.CommonUtils;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.HashMap;
import java.util.Map;


/**
 */
@Controller("/admin-api/liveCourse")
public class LiveCourseController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(LiveCourseController.class);

    LiveCourseService liveCourseService;

	/**
	 * 列表
	 */
	public void index() {
	    splitPage.getQueryParam().put("live_name", getPara("live_name"));
		paging(splitPage, LiveCourse.sqlId_splitPageSelect, LiveCourse.sqlId_splitPageFrom);
        renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(LiveCourseValidator.class)
	public void save() {
        LiveCourse model = getModel(LiveCourse.class, "", true);
        model.setCreate_time(getDate());
        model.setCreate_userid(getCUserId());
        model.setLive_images(CommonUtils.getImagesStr(model.getLive_images()));
        liveCourseService.createOrUpdate(model);
        success(null, "保存成功");
    }

	/**
	 * 更新
	 */
	@Before(LiveCourseValidator.class)
	public void update() {
        LiveCourse model = getModel(LiveCourse.class, "", true);
        model.setLive_images(CommonUtils.getImagesStr(model.getLive_images()));
        liveCourseService.createOrUpdate(model);
        success(null, "成功");
    }

    public void view() {
        LiveCourse obj = LiveCourse.dao.findById(getPara("id"));
        success(obj);
    }

	/**
	 * 删除
	 */
	public void delete() {
		//LiveCourse.dao.deleteByIds(getPara("id"));
        LiveCourse.dao.falseDelete(getPara("id"), getCUserId());
        renderOk("refresh", "删除成功", null);
    }

    /**
     * 获取直播参数
     */
    public void getBaseUrl() {
        Global g1 = Global.dao.findByCode(Global.Code.live_pushDomain.toString());
        Global g2 = Global.dao.findByCode(Global.Code.live_appName.toString());
        Map<String, String> map = new HashMap<>();
        map.put("s1", g1.getValue());
        map.put("s2", g2.getValue());
        success(map);
    }
}

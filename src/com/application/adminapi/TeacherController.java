package com.application.adminapi;

import com.application.common.Constant;
import com.application.module.teacherinfo.Teacherinfo;
import com.application.validate.AdminValidator;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Record;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.user.User;
import com.platform.mvc.user.UserValidator;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 */
@Controller("/admin-api/teacher")
public class TeacherController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(TeacherController.class);

    /**
	 */
	public void index() {

        splitPage.setOrderColunm("id");
        splitPage.setOrderMode("desc");
        paging(splitPage, Teacherinfo.sqlId_splitPageSel, Teacherinfo.sqlId_splitPageFrom);
        List<Record> list = (List<Record>) splitPage.getList();
        list.forEach(o->{
            o.set(User.column_password, "");
        });

        renderOk(splitPage);
	}


    /**
     * 保存新增用户
     */
    @Before({UserValidator.class})
    public void save() {
        User user = getModel(User.class, "", true);
        user.set(User.column_roleId, Constant.group_teacher).set("create_at", getDate())
                .setPassword(DigestUtils.md5Hex("1234"));
        user.save();
        Teacherinfo info = getModel(Teacherinfo.class, "", true);
        info.setTeacher_id(user.getId());
        info.set("teacher_createuser", getCUserId()).save();
        renderOk("refresh", "保存成功", null);
    }


    /**
     * 更新,修改管理员信息时需校验身份
     */
    @Before(AdminValidator.class)
    public void update() {
        User user = getModel(User.class, "", true);
        Teacherinfo teacherinfo = getModel(Teacherinfo.class, "", true);
        teacherinfo.setTeacher_id(user.getId());
        if(!teacherinfo.update()) {
            teacherinfo.set("teacher_createuser", getCUserId()).save();
        }
        boolean f = User.dao.updateAlternative(user);
        if(f){
            renderOk("refresh", "修改成功", null);
        } else {
            renderErr(500, "", "修改失败");
        }

    }

    /**
     * 删除
     */
    @Before(AdminValidator.class)
    public void delete() {
        User.dao.deleteByIds(getPara() == null ? getPara("id") : getPara());
        renderOk("refresh", "删除成功", null);
    }

    /*
    查询用户信息
     */
    public void view(){
        String userid = getPara() == null ? super.getCUserIds() : getPara();
        User user = User.dao.cacheGetByUserId(userid);
        user.setPassword("");
        renderOk(user);
    }

    /*
    关键字查询
    前端传递的keyword为乱码
     */
    public void query() {
        String str = "";
        try {
            str = new String(getPara("keyword").getBytes("iso-8859-1"),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            str = getPara("keyword");
        }
        List<Teacherinfo> list = Teacherinfo.dao.findByKey(str);
        renderOk(list);
    }

}



package com.application.adminapi;

import com.application.model.MessageRead;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 * 通知签收记录
 */
@Controller("/admin-api/messageRead")
public class MessageReadController extends BaseController {

    /*
    param: messageId
     */
    public void index() {
        splitPage.getQueryParam().put("messageId", getPara("messageId"));
        paging(splitPage, "com.application.messageRead.splitPageSelect", "com.application.messageRead.splitPageFrom");
        renderOk(splitPage);
    }

    /**
     * 删除
     */
    public void delete() {
        MessageRead.dao.deleteByIds(getPara("id"));
        renderOk("refresh", "删除成功", null);
    }
}

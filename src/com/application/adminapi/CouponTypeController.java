package com.application.adminapi;

import com.application.module.coupontype.CouponType;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import org.apache.log4j.Logger;

/**
 */
@Controller("/admin-api/couponType")
public class CouponTypeController extends BaseController {

    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(CouponTypeController.class);

    /*
    获取点播券列表
     */
    public void index() {
        paging(splitPage, CouponType.sqlId_splitPageSelect, CouponType.sqlId_splitPageFrom);
        success(splitPage);
    }

    public void save() {
        CouponType model = getModel(CouponType.class, "", true);
        model.setCreate_time(getDate());
        model.setCreate_userid(getCUserId());
        model.save();
        success(1);
    }

    public void update() {
        getModel(CouponType.class, "", true).update();
        success(1);
    }

    /**
     * 删除
     */
    public void delete() {
        CouponType.dao.deleteByIds(getPara("id"));
        renderOk("refresh", "删除成功", null);
    }

}

package com.application.adminapi;

import com.application.model.News;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.util.Map;

/**
 * 新闻管理
 */
@Controller("/admin-api/news")
public class NewsController extends BaseController {

    /*
    列表页面,查询参数type
     */
    public void index() {

        Map<String, Object> param = splitPage.getQueryParam();
        param.put("title", getPara("title"));
        param.put("content", getPara("content"));
        paging(splitPage, News.sqlId_splitPageSelect, News.sqlId_splitPageFrom);

        renderOk(splitPage);
    }


    /*
     */
    public void save() {
        News news = getModel(News.class, "", true);
        news.setCreate_time(getDate());
        news.setCreate_userid(getCUserId());
        news.save();

        renderOk("refresh", "添加成功", null);
    }


    public void update() {
        //String eid = getPara() == null ? ids : getPara();
        News news = getModel(News.class, "", true);
        news.remove("create_time");
        news.update();
        renderOk("refresh", "修改成功", null);
    }

    /**
     * 删除
     */
    public void delete() {
        News.dao.deleteByIds(getPara("id"));
        renderOk("refresh", "删除成功", null);
    }
}

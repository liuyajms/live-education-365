package com.application.adminapi;

import com.application.module.course.CourseValidator;
import com.application.module.vipchargevs.VipChargeVs;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;


/**
 */
@Controller("/admin-api/vipChargeVs")
public class VipChargeVsController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(VipChargeVsController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, VipChargeVs.sqlId_splitPageSelect, VipChargeVs.sqlId_splitPageFrom);
        renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(CourseValidator.class)
	public void save() {
        VipChargeVs model = getModel(VipChargeVs.class, "", true);
        model.save();
        success(null, "保存成功");
    }

	/**
	 * 更新
	 */
	@Before(CourseValidator.class)
	public void update() {
        getModel(VipChargeVs.class, "", true).update();
        success(null, "成功");
    }

    public void view() {
        VipChargeVs course = VipChargeVs.dao.findById(getPara("id"));
        success(course);
    }


	/**
	 * 删除
	 */
	public void delete() {
		VipChargeVs.dao.deleteByIds(getPara("id"));
        renderOk("refresh", "删除成功", null);
    }
	
}

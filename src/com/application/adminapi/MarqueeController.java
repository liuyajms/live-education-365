package com.application.adminapi;

import com.application.marquee.Marquee;
import com.application.model.News;
import com.application.module.course.Course;
import com.jfinal.plugin.activerecord.Db;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

import java.sql.Timestamp;
import java.util.Map;

/**
 * 跑马灯管理
 */
@Controller("/admin-api/marquee")
public class MarqueeController extends BaseController {


    public final static int MAX_SIZE = 5;

    /**
     * @title 跑马灯列表
     * @respBody
     */
    public void index() {

        paging(splitPage, Marquee.sqlId_pageSelect, Marquee.sqlId_splitPageFrom);
        /**
         * 封装图片阿里云路径
         */
//           marquee.put("miniUrl", OssUtil.getOssPrefix() + marquee.getStr("miniUrl"));

        renderOk(splitPage);
    }


    public void delete(){
        Marquee.dao.deleteByIds(getPara("id"));
        renderOk("reload", "删除成功", null);
    }


    public void save(){
        String[] moduleIds = getPara("module_id").split(",");
        String type = getPara("type");

        //判断是否超过最大限制数
        if (moduleIds.length > MAX_SIZE) {
            renderErr(500, "", "已超过最大限制数" + MAX_SIZE + "条");
            return;
        }

//		判断是否需要删除数据
        int size = Marquee.dao.findAllCount() + moduleIds.length - MAX_SIZE;
        if (size > 0) {
            Marquee.dao.delExtra(size);
        }

        for (String moduleId : moduleIds) {

            //判断用户是否已推送至跑马灯
            Marquee m = Marquee.dao.findFirst(super.getSqlMy(Marquee.sqlId_isPushed), moduleId, type);

            if (m != null) {//更新推送时间
                m.set(Marquee.column_createTime, getDate())
                        .set(Marquee.column_createUserId, getCUserIds())
                        .update();
//				String title = "";
//				if ("scholarship".equals(m.get(Marquee.column_module))) {
//					title = Scholarship.dao.findById(m.get(Marquee.column_newsId)).get(Scholarship.column_title);
//				} else if ("news".equals(m.get(Marquee.column_module))) {
//					title = News.dao.findById(m.get(Marquee.column_newsId)).get(News.column_title);
//				}
//				renderJson(new ResultEntity(HttpStatus.SC_BAD_REQUEST, "[" + title + "]已推送至轮播图，请勿重复推送"));
            } else {//新增数据
                String image = "";
                if(type.equals("course")){
                    image = Course.dao.findById(moduleId).getCourse_img();
                } else if(type.equals("news")){
                    image = News.dao.findById(moduleId).getImage();
                }
                new Marquee()
                        .set(Marquee.column_newsId, moduleId)
                        .set(Marquee.column_module, type)
                        .set(Marquee.column_createTime, new Timestamp(System.currentTimeMillis()))
                        .set(Marquee.column_createUserId, getCUserIds())
                        .set("image", image)
                        .save();
            }
        }

        renderOk("", "推送成功", null);
    }

    public void updateImg() {
        String img = getPara("image");
        String id = getPara("id");
        Db.update("update t_marquee set image = ? where id = ?", img, id);
        renderOk("", "设置成功", null);
    }

    /*
    根据类别查询
     */
    public void getModuleList() {
        Map<String, Object> param = splitPage.getQueryParam();
        param.put("title", getPara("title"));
        param.put("type", getPara("type"));
        paging(splitPage, Marquee.sqlId_getModuleListSelect, Marquee.sqlId_getModuleListFrom);
        renderOk(splitPage);
    }
}

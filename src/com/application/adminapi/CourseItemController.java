package com.application.adminapi;

import com.application.module.course.CourseValidator;
import com.application.module.courseitem.CourseItem;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 */
@Controller("/admin-api/courseItem")
public class CourseItemController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(CourseItemController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, CourseItem.sqlId_splitPageSelect, CourseItem.sqlId_splitPageFrom);
        renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(CourseValidator.class)
	public void save() {
        CourseItem model = getModel(CourseItem.class, "", true);
        model.setCreate_time(getDate());
        model.setCreate_userid(getCUserId());
        model.save();
        success(null, "保存成功");
    }

	/**
	 * 更新
	 */
	@Before(CourseValidator.class)
	public void update() {
        getModel(CourseItem.class, "", true).update();
        success(null, "成功");
    }


	/**
	 * 删除
	 */
	public void delete() {
		//CourseItem.dao.deleteByIds(getPara("id"));
        CourseItem.dao.falseDelete(getPara("id"), getCUserId());
        renderOk("refresh", "删除成功", null);
    }
	
}

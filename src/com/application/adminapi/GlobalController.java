package com.application.adminapi;

import com.application.model.Global;
import com.application.validate.GlobalValidator;
import com.application.validate.SuperAdminValidator;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;
import com.platform.mvc.base.BaseModel;

import java.util.Map;

/**
 *
 */
@Controller("/admin-api/global")
public class GlobalController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(GlobalController.class);

	/**
	 * 列表
	 */
	public void index() {
	    int showType = StrKit.isBlank(getPara("showType")) ? 1 : getParaToInt("showType");
	    splitPage.getQueryParam().put("show_type", showType);
		paging(splitPage, BaseModel.sqlId_splitPageSelect, Global.sqlId_splitPageFrom);
		renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(GlobalValidator.class)
	public void save() {
		Global a = Global.dao.create(super.getCUserId(), getPara("code"),
				getPara("name"), getAttrForStr("value"));
		a.save();
		renderOk("refresh", "添加成功", null);
	}
	
	/**
	 * 更新, SuperAdminValidator.class
	 */
	@Before({SuperAdminValidator.class})
	public void update() {
		Global a = getModel(Global.class, "", true);
		a.setUpdateTime(getDate());
		a.setUpdateUserid(getCUserId());
		a.update();
		Global.dao.cacheRemoveByCode(Global.Code.valueOf(a.getCode()));
		renderOk("refresh", "修改成功", null);
	}

	/**
	 * 查看
	 */
	public void view() {
		Global group = Global.dao.findById(getPara());
		success(group);
	}
	
	/**
	 * 删除
	 */
	public void delete() {

	}


    @Before({SuperAdminValidator.class})
    public void updateByCode() {
        Map<String, String> paramMap = getParamMap();
        for (String code : paramMap.keySet()) {
            Global a = Global.dao.findByCode(code);
            if(a != null) {
                a.setValue(paramMap.get(code));
                a.setUpdateTime(getDate());
                a.setUpdateUserid(getCUserId());
                if(a.update()) {
                    Global.dao.cacheRemoveByCode(Global.Code.valueOf(code));
                }
            }
        }

        renderOk("refresh", "修改成功", null);
    }
}

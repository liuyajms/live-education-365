package com.application.adminapi;

import com.application.module.music.MusicValidator;
import com.application.module.musicitem.MusicItem;
import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.platform.annotation.Controller;
import com.platform.mvc.base.BaseController;

/**
 */
@Controller("/admin-api/musicItem")
public class MusicItemController extends BaseController {

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog(MusicItemController.class);
	

	/**
	 * 列表
	 */
	public void index() {
		paging(splitPage, MusicItem.sqlId_splitPageSelect, MusicItem.sqlId_splitPageFrom);
        renderOk(splitPage);
	}
	
	/**
	 * 保存
	 */
	@Before(MusicValidator.class)
	public void save() {
        MusicItem model = getModel(MusicItem.class, "", true);
        model.setCreate_time(getDate());
        model.setCreate_userid(getCUserId());
        model.save();
        success(null, "保存成功");
    }

	/**
	 * 更新
	 */
	@Before(MusicValidator.class)
	public void update() {
        getModel(MusicItem.class, "", true).update();
        success(null, "成功");
    }


	/**
	 * 删除
	 */
	public void delete() {
		//MusicItem.dao.deleteByIds(getPara("id"));
        MusicItem.dao.falseDelete(getPara("id"), getCUserId());
        renderOk("refresh", "删除成功", null);
    }
	
}

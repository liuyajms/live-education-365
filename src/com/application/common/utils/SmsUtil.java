package com.application.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.jfinal.ext.kit.MsgException;
import com.platform.tools.ToolCache;
import com.platform.tools.ToolRandoms;

/**
 * Created on 17/6/7.
 * 短信API产品的DEMO程序,工程中包含了一个SmsDemo类，直接通过
 * 执行main函数即可体验短信产品API功能(只需要将AK替换成开通了云通信-短信产品功能的AK即可)
 * 工程依赖了2个jar包(存放在工程的libs目录下)
 * 1:aliyun-java-sdk-core.jar
 * 2:aliyun-java-sdk-dysmsapi.jar
 * <p>
 * 备注:Demo工程编码采用UTF-8
 */

public class SmsUtil {

    public static String template_bind = "SMS_212215050";

    static final String signName = "365锦囊";

    static final String regionId = "cn-hangzhou";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    static final String accessKeyId = "LTAI4FzuBcsuZMuix69LXeKG";
    static final String accessKeySecret = "agaCUY2rU7ipFH7RpGYDyv8i54DqKj";


    public static boolean sendSms(String templateId, String phone, String num) throws ClientException {
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(domain);
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", regionId);
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateCode", templateId);
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("TemplateParam", "{code:'#code'}".replace("#code", num));
        try {
            CommonResponse response = client.getCommonResponse(request);
            JSONObject jsonObject = JSONArray.parseObject(response.getData());
            System.out.println(response.getData());
            if (jsonObject != null && "OK".equalsIgnoreCase(jsonObject.get("Code").toString())) {
                return true;
            } else {
                throw new MsgException("验证码发送失败:" + jsonObject.get("Message"));
            }
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean send(String templateId, String mobile) throws Exception {
        String str = ToolRandoms.getRandomNum(4);
        if("18111572274".equalsIgnoreCase(mobile) || "13551007059".equals(mobile)){
            ToolCache.set(mobile, 5 * 60, "1111");
            return true;
        }
        //发送验证码
        if (sendSms(templateId, mobile, str)) {
            //存储验证码
            ToolCache.set(mobile, 5 * 60, str);
            return true;
        }
        return false;
    }

    /**
     * 校验验证码与手机号是否相符合
     *
     * @param mobile     手机号
     * @param verifyCode 验证码
     * @return
     */
    public static Boolean verify(String mobile, String verifyCode) {
        try {
            String str = ToolCache.get(mobile);
            return verifyCode.equals(str);
        } catch (Exception e) {

        }
        return false;
    }
}
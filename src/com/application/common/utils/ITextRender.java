package com.application.common.utils;

import com.jfinal.kit.PathKit;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import org.beetl.core.Template;
import org.beetl.ext.jfinal.BeetlRenderFactory;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 模板文件导出pdf
 */
public class ITextRender {
    public static void main(String[] args) throws Exception {
        String inputFile = "E:\\Backup\\JFinalUIB\\WebContent\\WEB-INF\\view\\html\\exam.html";
        String url = new File(inputFile).toURI().toURL().toString();
        String outputFile = "e:/firstdoc2.pdf";
//        generatePdf("/html/exam.html", outputFile);

    }

    @Deprecated
    public static void generatePdf(String templateView, String outputFile) {
        OutputStream os = null;
        try {
            os = new FileOutputStream(outputFile);

            Template template = new BeetlRenderFactory().groupTemplate.getTemplate(templateView);
            String doc = template.render();

            ITextRenderer renderer = new ITextRenderer();
//        renderer.setDocument(url);

//        doc ="<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; " +
//                "charset=UTF-8\"/></head><body style=\"font-family:SimSun\"><p>你好啊！</p><p>这里加入图片</p><p>测试而已！</p><p>hello the " +
//                "world~</p></body></html>";
            System.out.println(doc);
            renderer.setDocumentFromString(doc);

            // 解决中文支持问题
            ITextFontResolver fontResolver = renderer.getFontResolver();
            fontResolver.addFont("C:/Windows/Fonts/simsunb.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            fontResolver.addFont("c:/Windows/Fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            // 解决图片的相对路径问题
//        renderer.getSharedContext().setBaseURL("file:/D:/Work/Demo2do/Yoda/branch/Yoda%20-%20All/conf/template/");

            renderer.layout();
            renderer.createPDF(os);

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void generatePdf(File inputFile, File outputFile) {
        OutputStream os = null;
        try {
            os = new FileOutputStream(outputFile);

            ITextRenderer renderer = new ITextRenderer();
//            renderer.setDocumentFromString(doc);
            renderer.setDocument(inputFile);

            // 解决中文支持问题
            ITextFontResolver fontResolver = renderer.getFontResolver();
            String basePath = PathKit.getWebRootPath().replace("\\", "/") + "/files/fonts/";

            fontResolver.addFont(basePath + "msyh.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            fontResolver.addFont(basePath + "msyhbd.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            fontResolver.addFont(basePath + "simsun.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            fontResolver.addFont(basePath + "simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            fontResolver.addFont(basePath + "simsunb.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

//            fontResolver.addFont("C:/Windows/Fonts/simsunb.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//            fontResolver.addFont("c:/Windows/Fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//            fontResolver.addFont("c:/Windows/Fonts/MSYH.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            // 解决图片的相对路径问题
//            renderer.getSharedContext().setBaseURL("file:/E:/Backup/JFinalUIB/WebContent");

            renderer.layout();
            renderer.createPDF(os);

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

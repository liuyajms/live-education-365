package com.application.common.utils;

import com.jfinal.ext.render.excel.PoiRender;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.render.Render;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by ly on 2016/5/25.
 */
public class ExportUtil {

    public static Render exportSingle(String propPrefix, List<Record> recordList) {

        String a = PropKit.use("export-config.properties").get(propPrefix + "_headers");
        String b = PropKit.use("export-config.properties").get(propPrefix + "_columns");
        String fileName = PropKit.use("export-config.properties").get(propPrefix + "_filename");

        try {
            fileName = URLEncoder.encode(fileName, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//        String fileName = "研学旅行课程研究性学习记录管理表.xls";
        String sheetName = "明细";

        String[] headers = a.replaceAll("\\s+", "").split(",|，");
        String[] columns = b.replaceAll("\\s+", "").split(",|，");

        PoiRender render = PoiRender.me(recordList).fileName(fileName)
                .sheetName(sheetName)
                .headers(headers)
                .cellWidth(256*30)
                .columns(columns);

        return render;

    }


}

package com.application.common.utils;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.rtf.RtfWriter2;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ly on 2016/5/27.
 */
public class WordUtils {


    public static void main(String[] args) {

        try {
            createRTFContext("e:\\test.doc");
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * 创建word文档 步骤:
     * 1,建立文档
     * 2,创建一个书写器
     * 3,打开文档
     * 4,向文档中写入数据
     * 5,关闭文档
     */
    public static void createRTFContext(String path) throws DocumentException,

            IOException {

        Document document = new Document(PageSize.A4);

        RtfWriter2.getInstance(document, new FileOutputStream(path));

        document.open();

// 设置中文字体

        BaseFont bfChinese = BaseFont.createFont("STSongStd-Light",

                "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);

// 标题字体风格

        Font titleFont = new Font(bfChinese, 12, Font.BOLD);


// 正文字体风格

        Font contextFont = new Font(bfChinese, 10, Font.NORMAL);


        Paragraph title = new Paragraph("标题");

// 设置标题格式对齐方式

        title.setAlignment(Element.ALIGN_CENTER);

        title.setFont(titleFont);

        document.add(title);


        String contextString = "iText是一个能够快速产生PDF文件的java类库。iText的java类对于那些要产生包含文本，表格，图形的只读文档是很有用的。它的类库尤其与java Servlet有很好的给合。使用iText与PDF能够使你正确的控制Servlet的输出。";

        Paragraph context = new Paragraph(contextString);

// 正文格式左对齐

        context.setAlignment(Element.ALIGN_LEFT);

        context.setFont(contextFont);

// 离上一段落（标题）空的行数

        context.setSpacingBefore(20);

// 设置第一行空的列数

        context.setFirstLineIndent(20);


        document.add(context);

// //在表格末尾添加图片
        document.newPage();

        Image png = Image.getInstance("C:\\Users\\ly\\Pictures\\hook.png");

        document.add(png);

        document.close();

    }


    public static void createWord(String path) {

        Document document = null;
        try {
            document = new Document(PageSize.A4);

            RtfWriter2.getInstance(document, new FileOutputStream(path));

            document.open();

// 设置中文字体

            BaseFont bfChinese = BaseFont.createFont("STSongStd-Light",

                    "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);

// 标题字体风格

            Font titleFont = new Font(bfChinese, 12, Font.BOLD);


// 正文字体风格

            Font contextFont = new Font(bfChinese, 10, Font.NORMAL);


            Paragraph title = new Paragraph("标题");

// 设置标题格式对齐方式

            title.setAlignment(Element.ALIGN_CENTER);

            title.setFont(titleFont);

            document.add(title);


            String contextString = "iText是一个能够快速产生PDF文件的java类库。iText的java类对于那些要产生包含文本，表格，图形的只读文档是很有用的。它的类库尤其与java Servlet有很好的给合。使用iText与PDF能够使你正确的控制Servlet的输出。";

            Paragraph context = new Paragraph(contextString);

// 正文格式左对齐

            context.setAlignment(Element.ALIGN_LEFT);

            context.setFont(contextFont);

// 离上一段落（标题）空的行数

            context.setSpacingBefore(20);

// 设置第一行空的列数

            context.setFirstLineIndent(20);


            document.add(context);

// //在表格末尾添加图片
            document.newPage();

            Image png = Image.getInstance("C:\\Users\\ly\\Pictures\\hook.png");

            document.add(png);

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (document != null) {
                document.close();
            }
        }

    }

}

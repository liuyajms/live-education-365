package com.application.common.utils;

import com.cloopen.rest.sdk.CCPRestSDK;
import com.jfinal.kit.PropKit;
import com.platform.tools.ToolCache;
import com.platform.tools.ToolRandoms;

import java.util.HashMap;

/**
 * 验证手机号
 */
public class VerifyKit {

    private static final String SERVER_IP = "app.cloopen.com";//sandboxapp.cloopen.com
    private static final String SERVER_PORT = "8883";

    private static final String ACCOUNT_SID = PropKit.get
            ("SMS_ACCOUNT_ID", "8a216da8621834ef01622ccab9630e90");
    private static final String AUTH_TOKEN = PropKit.get
            ("SMS_AUTH_TOKEN", "28d0dac463b949e6b0333014aa9736e7");

    private static final String APP_ID = PropKit.get("SMS_APP_ID",
            "8aaf070862181ad501622ec76e701084");

    private static final String VOICE_TIME = "2";

    private static final String VALID_TIME = PropKit.get("SMS_VALID_TIME", "5分钟");

    public static String REGISTER_TEMPLATE_Id = PropKit.get("REGISTER_TEMPLATE_Id", "239158");
    public static String FINDPASSWORD_TEMPLATE_Id = PropKit.get("FINDPASSWORD_TEMPLATE_Id", "239158");
    public static String CHANGEMOBILE_TEMPLATE_Id = PropKit.get("CHANGEMOBILE_TEMPLATE_Id", "239158");
    public static String CHARGE_TEMPLATE_Id = PropKit.get("CHARGE_TEMPLATE_Id", "239158");

    public static String SENDCHARGE_TEMPLATE_Id = PropKit.get("RECHARGE_TEMPLATE_Id", "455158");

    public static boolean smsVerify(String templateId, String phone, String[] params) throws Exception {
        HashMap<String, Object> result = null;

        CCPRestSDK restAPI = new CCPRestSDK();
        restAPI.init(SERVER_IP, SERVER_PORT);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
        restAPI.setAccount(ACCOUNT_SID, AUTH_TOKEN);// 初始化主帐号名称和主帐号令牌
        restAPI.setAppId(APP_ID);// 初始化应用ID
//        result = restAPI.voiceVerify(num, phone, null, VOICE_TIME, null);
//        result = restAPI.voiceVerify("验证码内容", "号码", "显示的号码", "3(播放次数)", "");

        result = restAPI.sendTemplateSMS(phone, templateId, params);

        System.out.println("SDKTestGetSubAccounts result=" + result + ", phone:" + phone);

        if ("160040".equals(result.get("statusCode"))) {
            throw new Exception("超过当天发送上限");
        }
        return "000000".equals(result.get("statusCode"));
/*        if ("000000".equals(result.get("statusCode"))) {
            //正常返回输出data包体信息（map）
            HashMap<String, Object> data = (HashMap<String, Object>) result.get("data");
            Set<String> keySet = data.keySet();
            for (String key : keySet) {
                Object object = data.get(key);
                System.out.println(key + " = " + object);
            }
        } else {
            //异常返回输出错误码和错误信息
            System.out.println("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
        }*/
    }


    /**
     * 校验验证码与手机号是否相符合
     *
     * @param mobile     手机号
     * @param verifyCode 验证码
     * @return
     */
    public static Boolean verify(String mobile, String verifyCode) {
        try {
            if("13551007059".equals(mobile)) {
                ToolCache.set(mobile, 5 * 60, "1111");
            }
            String str = ToolCache.get(mobile);
            return verifyCode.equals(str);
        } catch (Exception e) {

        }
        return false;
    }


    public static boolean send(String templateId, String mobile) throws Exception {
        String str = ToolRandoms.getRandomNum(4);

        if("18111572274".equalsIgnoreCase(mobile) || "13551007059".equals(mobile)){
            ToolCache.set(mobile, 5 * 60, "1111");
            return true;
        }
        if (smsVerify(templateId, mobile, new String[]{str, VALID_TIME})) {
            ToolCache.set(mobile, 5 * 60, str);
            return true;
        }

        return false;
    }

    public static boolean send(String templateId, String mobile, String randomStr, String[] params) throws Exception {
        if (smsVerify(templateId, mobile, params)) {
            ToolCache.set(mobile, 5 * 60, randomStr);
            return true;
        }
        return false;
    }

    }

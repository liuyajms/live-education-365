package com.application.common.utils.wx;

import com.jfinal.kit.PropKit;

/**
 * wx 配置文件
 * */
public class WxConstant {

    public static final String appid = PropKit.get("xcx.appid");
    public static final String secret = PropKit.get("xcx.secret");

    public static final String AES = "AES";
    public static final String AES_CBC_PADDING = "AES/CBC/PKCS7Padding";

}
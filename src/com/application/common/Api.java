package com.application.common;

/**
 * Created by Administrator on 2017/5/16 0016.
 */
public interface Api {
    String user_reg = "/api/user/register";
    String user_update = "/api/user/update";
    String user_login = "/api/login/vali";
    String user_findpass = "/api/user/findPass";
    String user_changepass = "/api/user/changePass";
    String user_getvalicode = "/api/user/getValicode";


    String enterprise_detail = "/api/enterprise/view";
    String enterprise_query = "/api/enterprise/query";
    String ep_report = "/api/enterprise/report";

    String reportflow_audit = "/api/reportflow/audit";
    String inspectflow_audit = "/api/inspectflow/audit";
    String inspectflow_submit = "/api/inspectflow/submit";//仅用于Epuser


    String index_epUser = "/api/index/epUser";
    String index_associate = "/api/index/associate";
    String index_street = "/api/index/street";
    String index_supervisor = "/api/index/supervisor";

    String admin_usersave = "/admin/user/save";
    String admin_userupdate = "/admin/user/update";

    String admin_deptsave = "/admin/dept/save";
    String admin_deptupdate = "/admin/dept/update";
    String admin_reportflow_audit = "/admin/reportflow/audit";
    String admin_inspectflow_audit = "/admin/inspectflow/audit";
    String admin_inspect_audit = "/admin/inspect/audit";
}
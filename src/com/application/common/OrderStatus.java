package com.application.common;

import java.util.HashMap;
import java.util.Map;

public abstract class OrderStatus {

    /*
10:已确认-未支付(待付款)
11:已确认-已支付(待发货)
20:已发货-未收货(待收货)
21:已发货-已收货(已完成)
30:已关闭-已取消
31:已关闭-超时订单
32:已关闭-手动关闭
40:退款-未退款(待退款)
41:退款-已退款
42:退款-已拒绝

待评价订单查询:is_comment=false & 发货状态为已完成
 */

    public static int PAY_WAIT = 10;

    public final static int PAY_OVER = 11;//已支付==待发货

    public final static int TAKE_WAIT = 20;//已发货-未收货

    public final static int TAKE_OVER = 21;  //(已完成/交易成功)

    public static int CLOSED_CANCEL = 30;

    public static int CLOSED_TIMEOUT = 31;

    public static int CLOSED_OTHER = 32;

    public static int REFUND_WAIT = 40;

    public static int REFUND_OVER = 41;

    public static int REFUND_REJECT = 42;

    public static Map<Integer, String > map = new HashMap<>();
    static {
        map.put(PAY_WAIT, "待付款");
        map.put(PAY_OVER, "待发货");
        map.put(TAKE_WAIT, "待收货");
        map.put(TAKE_OVER, "已完成");
        map.put(REFUND_WAIT, "待退款");
        map.put(REFUND_OVER, "已退款");
        map.put(REFUND_REJECT, "拒绝退款");
        map.put(CLOSED_CANCEL, "已关闭");
        map.put(CLOSED_TIMEOUT, "已关闭");
        map.put(CLOSED_OTHER, "已关闭");
    }

    /*
    客户端状态，待付款0、待收货1、待评价/已完成2、已取消3
     */
    public static Map<Integer, Integer > clientMap = new HashMap();
    static {
        clientMap.put(PAY_WAIT, 0);
        clientMap.put(PAY_OVER, 1);
        clientMap.put(TAKE_WAIT, 1);
        clientMap.put(TAKE_OVER, 2);
        clientMap.put(CLOSED_CANCEL, 3);
        clientMap.put(CLOSED_TIMEOUT, 3);
        clientMap.put(CLOSED_OTHER, 3);
        clientMap.put(REFUND_WAIT, 3);
        clientMap.put(REFUND_OVER, 3);
        clientMap.put(REFUND_REJECT, 3);
    }

    /*
    后台状态，全部-1，待付款0、待发货1、已完成2、已取消3、待退款4、待收货5
     */
    public static Map<Integer, Integer > webMap = new HashMap();
    static {
        webMap.put(PAY_WAIT, 0);
        webMap.put(PAY_OVER, 1);
        webMap.put(TAKE_WAIT, 5);
        webMap.put(TAKE_OVER, 2);
        webMap.put(CLOSED_CANCEL, 3);
        webMap.put(CLOSED_TIMEOUT, 3);
        webMap.put(CLOSED_OTHER, 3);
        webMap.put(REFUND_WAIT, 4);
//        webMap.put(REFUND_OVER, 4);
//        webMap.put(REFUND_REJECT, 4);
    }
}

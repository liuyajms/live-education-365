package com.application.common.parser;

/**
 * Created by Administrator on 2017/11/10 0010.
 */
public interface AbstractParser<T> {
    public T parse(String param);
}

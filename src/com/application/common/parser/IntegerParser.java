package com.application.common.parser;

/**
 * Created by Administrator on 2017/11/10 0010.
 */
public class IntegerParser implements AbstractParser<Integer> {
    @Override
    public Integer parse(String param) {
        try {
            return Integer.parseInt(param);
        }catch (Exception e){
            System.out.println("参数格式不规范过滤：Integer->" + param);
        }
        return null;
    }
}

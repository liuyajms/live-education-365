package com.application.common.parser;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/11/10 0010.
 */
public class BigDecimalParser implements AbstractParser<BigDecimal> {
    @Override
    public BigDecimal parse(String param) {
        try {
            return new BigDecimal(param);
        }catch (Exception e){
            System.out.println("参数格式不规范过滤：BigDecimal->" + param);
        }
        return null;
    }
}

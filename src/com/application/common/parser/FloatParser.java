package com.application.common.parser;

/**
 * Created by Administrator on 2017/11/10 0010.
 */
public class FloatParser implements AbstractParser<Float> {
    @Override
    public Float parse(String param) {
        try {
            return Float.parseFloat(param);
        }catch (Exception e){
            System.out.println("参数格式不规范过滤：Float->" + param);
        }
        return null;
    }
}

package com.application.common.parser;

import java.sql.Date;

/**
 * Created by Administrator on 2017/11/10 0010.
 */
public class DateParser implements AbstractParser<Date> {
    @Override
    public Date parse(String param) {
        try {
            return Date.valueOf(param);
        }catch (Exception e){
            System.out.println("参数格式不规范过滤：sql.Date->" + param);
        }
        return null;
    }
}

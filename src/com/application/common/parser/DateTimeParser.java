package com.application.common.parser;

import com.jfinal.kit.StrKit;

import java.sql.Date;

/**
 * Created by Administrator on 2017/11/10 0010.
 */
public class DateTimeParser implements AbstractParser<Date> {
    @Override
    public Date parse(String param) {
        try {
            if (StrKit.notBlank(param))
                return Date.valueOf(param);
        } catch (Exception e) {
            System.out.println("参数格式不规范过滤：Date->" + param);
        }
        return null;
    }
}

package com.application.common;

/**
 * Created by ly on 2016/2/24.
 */
public interface Constant {
    int OP_ADD = 1;
    int OP_UPDATE = 2;
    int OP_VIEW = 3;

    String group_normal = "normal";//企业用户
    String group_admin = "Admin";
    String group_SuperAdmin = "SuperAdmin";
    String group_teacher = "teacher";
    String group_operator = "Operator";//运营人员，负责平台新闻资讯维护、问答维护
    String group_vip = "vip";


    final static String[] num = {
            "一", "二", "三", "四", "五", "六"
    };


}

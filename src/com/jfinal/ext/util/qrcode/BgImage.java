package com.jfinal.ext.util.qrcode;

public class BgImage {
    private String path;
    private int x;
    private int y;
    private int qrWidth;
    private int qrHeight;

    public BgImage(String path, int x, int y, int qrWidth, int qrHeight) {
        this.path = path;
        this.x = x;
        this.y = y;
        this.qrWidth = qrWidth;
        this.qrHeight = qrHeight;
    }

    public int getQrWidth() {
        return qrWidth;
    }

    public void setQrWidth(int qrWidth) {
        this.qrWidth = qrWidth;
    }

    public int getQrHeight() {
        return qrHeight;
    }

    public void setQrHeight(int qrHeight) {
        this.qrHeight = qrHeight;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}

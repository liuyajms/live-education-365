package com.jfinal.ext.util;

import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlUtil {
    public static String getSql(String s, String... args) {
        for (int i = 0; i < args.length; i++) {
            s = s.replaceFirst("\\{\\w+}", args[i]);
        }
        return s;
    }

    public static List<Map<String, Object>> getOne2Many(List<Record> list, String tableAId, String tableBId,
                                                        String objName, String[] fields) {

        List<Map<String, Object>> resultList = new ArrayList<>();
        List<String> pidList = new ArrayList<>();

        /*
        获取父类集合
         */
        for (Record record : list) {
            String pid = record.getStr(tableAId);
            if (!pidList.contains(pid)) {
                pidList.add(pid);

                Map<String, Object> m = new HashMap<>();
                m.put(objName, new ArrayList<>());
                for (String col : record.getColumnNames()) {
                    boolean find = false;
                    for (String field : fields) {
                        if (col.equalsIgnoreCase(field)) {
                            find = true;
                            break;
                        }
                    }
                    if (!find) {
                        m.put(col, record.get(col));
                    }
                }
                resultList.add(m);
            }
        }

        /*
        循环子类加入对应父类
         */
        for (Record record : list) {
            Map<String, Object> obj = new HashMap<>();
            for (Map<String, Object> map : resultList) {
                if (record.getStr(tableBId).equalsIgnoreCase(map.get(tableAId).toString())) {
                    for (String k : fields) {
                        obj.put(k, record.get(k));
                    }
                    ((List<Object>) (map.get(objName))).add(obj);
                }
            }
        }

        return resultList;
    }

    private static void getMockData(List<Record> list) {
        for (int i = 1; i < 5; i++) {
            Record model = new Record();
            Map<String, Object> map = new HashMap<>();
            map.put("f1", "zhangsa");
            map.put("f2", "zhangsa2");
            map.put("f3", "zhangsa3");
            map.put("f4", "zhangsa4");
            map.put("f5", "zhangsa5");
            map.put("a", "id1");
            map.put("b", "id1");

            map.put("b22", "v1");
            map.put("b23", "v2");
            map.put("b24", "v3");


            model.setColumns(map);

            list.add(model);
        }
    }

    public static void main(String[] args) {
        String s = "select * from {a5} a join {c7} c";
        System.out.println(getSql(s, "t1", "t2"));
//        List<Map<String, Object>> one2Many = getOne2Many("a", "b", "subList", new String[]{"b22", "b23"});
//        System.out.println(Json.getJson().toJson(one2Many));
    }
}

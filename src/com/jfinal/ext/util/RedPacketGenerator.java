package com.jfinal.ext.util;

import java.util.Random;

/**
 * Created by Administrator on 2018/3/18 0018.
 */
public class RedPacketGenerator {
    public static void main(String[] args) {
//        Random random = new Random();
//        System.out.println(Math.abs(random.nextInt())%20);
        int s = generate(2, 5);
        System.out.println(s);

    }

    public static String generate(String minV, String maxV){
        int min = (int) (Float.valueOf(minV)*100);
        int max = (int) (Float.valueOf(maxV)*100);

        Random random = new Random();
        int s = random.nextInt(max)%(max-min+1) + min;
//        DecimalFormat df = new DecimalFormat("0.00");//格式化小数
//        String num = df.format((float)s/100);//返回的是String类型
        return String.valueOf((float)s/100);
    }

    public static int generate(int min, int max) {
        Random random = new Random();
        int s = random.nextInt(max)%(max-min+1) + min;
        return s;
    }

    public static int generate() {
        return generate(2, 5);
    }
}

package com.jfinal.ext.tree;

import java.util.List;

/**
 * Ztree节点数据封装
 * @author 董华健
 */
public class ZtreeNode {
	
	/**
	 * 节点id
	 */
	private String id;

	/**
	 * 节点名称
	 */
	private String name;

	/**
	 * 是否上级节点
	 */
	private boolean isParent;

    private Integer ord;


    private String operator;


	/**
	 * 节点图标
	 */
	private String icon;

    private String parentId;

	private String parentName;

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
	 * 子节点数据
	 */
	private List<ZtreeNode> children;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<ZtreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<ZtreeNode> children) {
		this.children = children;
	}

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}

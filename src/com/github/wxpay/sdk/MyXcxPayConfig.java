package com.github.wxpay.sdk;

import com.jfinal.kit.PropKit;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class MyXcxPayConfig extends WXPayConfig {

    private byte[] certData;
    private static MyXcxPayConfig INSTANCE;

    private final String key = PropKit.get("wx.key");
//    private final String appId = PropKit.get("xcx.appid", "wx378e700721da0ef6");
    private final String appId = PropKit.get("wx.appId");
    private final String mchId = PropKit.get("wx.mchId").trim();

    private MyXcxPayConfig() throws Exception{
        /*String certPath = PropKit.get("wx.certPath", "/application/Shop/apiclient_cert.p12");
                File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();*/
    }

    public static MyXcxPayConfig getInstance() throws Exception{
        if (INSTANCE == null) {
            synchronized (MyXcxPayConfig.class) {
                if (INSTANCE == null) {
                    INSTANCE = new MyXcxPayConfig();
                }
            }
        }
        return INSTANCE;
    }

    public String getAppID() {
        return appId;
    }

    public String getMchID() {
        return mchId;
    }

    public String getKey() {
        return key;
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis;
        certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }


    public int getHttpConnectTimeoutMs() {
        return 2000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    public IWXPayDomain getWXPayDomain() {
        return MyWXPayDomainSimple.instance();
    }

    public String getPrimaryDomain() {
        return "api.mch.weixin.qq.com";
    }

    public String getAlternateDomain() {
        return "api2.mch.weixin.qq.com";
    }

    @Override
    public int getReportWorkerNum() {
        return 1;
    }

    @Override
    public int getReportBatchSize() {
        return 2;
    }
}

package com.github.wxpay.sdk;

import com.jfinal.kit.PropKit;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MyWXPayConfig extends WXPayConfig {

    private byte[] certData;
    private static MyWXPayConfig INSTANCE;

    private final String key = PropKit.get("wx.key");
    private final String appId = PropKit.get("wx.appId");
    private final String appSecret = PropKit.get("wx.appSecret");
    private final String mchId = PropKit.get("wx.mchId");

    private MyWXPayConfig() throws Exception{
        String certPath = PropKit.get("wx.certPath", "/application/Education365/apiclient_cert.p12");
        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    public static MyWXPayConfig getInstance() throws Exception{
        if (INSTANCE == null) {
            synchronized (MyWXPayConfig.class) {
                if (INSTANCE == null) {
                    INSTANCE = new MyWXPayConfig();
                }
            }
        }
        return INSTANCE;
    }

    public String getAppID() {
        return appId;
    }

    public String getMchID() {
        return mchId;
    }

    public String getKey() {
        return key;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis;
        certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }


    public int getHttpConnectTimeoutMs() {
        return 2000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    public IWXPayDomain getWXPayDomain() {
        return MyWXPayDomainSimple.instance();
    }

    public String getPrimaryDomain() {
        return "api.mch.weixin.qq.com";
    }

    public String getAlternateDomain() {
        return "api2.mch.weixin.qq.com";
    }

    @Override
    public int getReportWorkerNum() {
        return 1;
    }

    @Override
    public int getReportBatchSize() {
        return 2;
    }
}

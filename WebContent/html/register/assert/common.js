﻿var common = common || {};
common.onLimitValue = function (str, maxLen, maxValue, isGreaterThan0) {
    if (maxLen && maxLen > 0) {
        //length 获取字数数，不区分汉子和英文
        var w = 0;
        for (var i = 0; i < str.value.length; i++) {
            //charCodeAt()获取字符串中某一个字符的编码
            var c = str.value.charCodeAt(i);
            //单字节加1
            if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
                w++;
            } else {
                w += 2;
            }
            if (w > maxLen) {
                str.value = str.value.substr(0, i);
                break;
            }
        }
    }
    //超过最大值则设置该值
    if (maxValue && parseFloat(str.value) > maxValue) {
        str.value = maxValue;
    }
    //是否可以输入负数
    if (typeof isGreaterThan0 === "undefined")
        isGreaterThan0 = false;
    if (isGreaterThan0) {
        if (parseFloat(str.value) < 0) {
            str.value = "";
        }
    }
    //number中输入的非数字则无效
    if (str.type == "number" && isNaN(parseFloat(str.value))) {
        str.value = "";
    }
};

//设置字体
common.setFontSize = function () {
    var dpr = window.devicePixelRatio;
    var endFontSize = "";
    var htmlSize = $("html").width();
    var htmlFontSize = $("html").css("font-size").replace("px", "");
    if (htmlSize <= 320) {
        htmlFontSize = 28;
    }
    else if (htmlSize <= 400) {
        htmlFontSize = 32;
    }
    else if (htmlSize <= 480) {
        htmlFontSize = 36;
    }
    else if (htmlSize <= 560) {
        htmlFontSize = 40;
    }
    else if (htmlSize <= 640) {
        htmlFontSize = 42;
    }
    else if (htmlSize <= 720) {
        htmlFontSize = 46;
    }
    else if (htmlSize <= 800) {
        htmlFontSize = 50;
    }
    else if (htmlSize <= 960) {
        htmlFontSize = 52;
    }
    else {
        htmlFontSize = 52;
    }
    //htmlFontSize = htmlFontSize > 40 ? htmlFontSize * 0.6 : htmlFontSize;
    $("html").css("cssText", "font-size:" + htmlFontSize + "px !important");
    var curFontSize = $("html").css("font-size").replace("px", "");
    if (curFontSize < htmlFontSize) {
        endFontSize = htmlFontSize / 0.85;
    }
    else if (curFontSize / 1.15 < htmlFontSize + 2 && curFontSize / 1.15 > htmlFontSize - 2) {
        endFontSize = htmlFontSize / 1.15;
    }
    else if (curFontSize / 1.3 < htmlFontSize + 2 && curFontSize / 1.3 > htmlFontSize - 2) {
        endFontSize = htmlFontSize / 1.3;
    }
    else {
        endFontSize = htmlFontSize;
    }
    $("html").css("cssText", "font-size:" + endFontSize + "px !important");
};

$(document).ready(function () {
    //设置字体
    common.setFontSize();
    $(window).on("resize", function () {
        common.setFontSize();
    });
});


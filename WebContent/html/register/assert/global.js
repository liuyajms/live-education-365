﻿var tools = tools || {};
tools.getToken = function () {
    return $.cookie("tk");
}

tools.ajaxPost = function (url, data, actionResult, actionBeforeSend, actionError) {
    var token = tools.getToken();
    if (token === "")
        alert("未授权的客户端提交");
    var headers = [];
    headers["Authorization"] = "bearer " + tools.getToken();
    $.ajax({
        url: url,
        data: data,
        method: "post",
        //headers: headers,
        success: function (result) {
            if (actionResult) actionResult(result);
        },
        beforeSend: actionBeforeSend,
        error: actionError
    });
}
tools.PostUrl = function (URL, PARAMTERS) {
    //创建form表单
    var temp_form = document.createElement("form");
    var token = tools.getToken();
    if (token)
        temp_form.action = URL + "?token=" + token;
    else
        temp_form.action = URL;
    //如需打开新窗口，form的target属性要设置为'_blank'
    temp_form.target = "_self";
    temp_form.method = "post";
    temp_form.style.display = "none";

    for (var item in PARAMTERS) {
        var opt = document.createElement("textarea");
        opt.name = item;
        opt.value = PARAMTERS[item];
        temp_form.appendChild(opt);
    }
    document.body.appendChild(temp_form);
    //提交数据
    temp_form.submit();
}
tools.setErrorInfo = function (msg, time) {
    var times = time || 1500;
    $("body").append('<div class="field-tooltipWrap"><div class="field-tooltipInner"><div class="field-tooltip fieldTipBounceIn"><div class="zvalid-resultformat">' + msg + '</div></div></div></div>');
    setTimeout(function () {
        $(".field-tooltipWrap").remove();
    }, times);
};
var restSelect = function (url, params, f) {
    $.getJSON(url, params, f);
};

var restInsert = function (url, params, form, f) {
    if (form == null) {
        $.post(url, params, f);
    } else {
        form.ajaxSubmit({
            url: url,
            type: "POST",
            dataType: "json",
            data: params,
            success: f
        });
    }
};

var restUpdate = function (url, params, form, f) {
    if (form == null) {
        $.ajax({
            type: "PUT",
            url: url,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: params,
            timeout: 100000,
            success: f
        });
    } else {
        form.ajaxSubmit({
            url: url,
            type: "PUT",
            dataType: "json",
            data: params,
            timeout: 100000,
            success: f
        });
    }
};

var restDelete = function (url, f) {
    $.ajax({
        type: "DELETE",
        url: url,
        dataType: "json",
        success: f
    });
};

var restPost = function (url, params, f, errorHandler) {
    $.ajax({
        type: "post",
        data: params,
        url: url,
        dataType: "json",
        async:false,
        success: function(rs){
            // if(rs.code == 200 || rs.status == 200){
            //     f(rs);
            // }
            if(rs.code == 401 || rs.status == 401){
                top.location.href = '../login.html';
            } else {
                f(rs);
            }
        }
    });
};

var restGet = function (url, params, form, f) {
    $.post(url, params, function (data) {
        if (form != null) {
            form[0].reset();
            if(data.data != null) {
                $.each($(":input", form), function (i, n) {
                    var name = $(n).attr("name");
                    if(name && name.indexOf('.') > 0){
                        name = name.split('.')[1]
                    }
                    var v = eval("data.data." + name);
                    try {
                        if ($(n).attr('type') == 'radio') {
                            if (v == $(n).val()) {
                                $(n).attr('checked', 'checked');
                            }
                        } else if ($(n).attr('type') == 'checkbox') {
                            var t = eval("data.data." + name.split('-')[0]);
                            var arr = t.split(',');
                            $.each(arr, function (k, m) {
                                if (m == $(n).val()) {
                                    $(n).prop('checked', true);
                                }
                            });
                        }
                        else {
                            $(n).val(v);
                        }
                    } catch (e) {
                    }
                });
            }
            /*$.each(data.data, function (i, n) {
                try {
                    $('[name='+ i + ']').val(n);
                } catch (e) {
                }
            });*/
        }
        f(data);
    });
};

var randomUrl = function (url) {
    if (url.indexOf("?") == -1) {
        return url + "?" + Math.random();
    } else {
        return url + "&" + Math.random();
    }
};

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "H+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}


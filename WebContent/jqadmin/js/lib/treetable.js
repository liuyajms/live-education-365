var TreeTable = function () {
    var initTableData = function ($url) {
        layui.use(['tree', 'layer', 'dtable', 'jqdate', 'jqform'], function () {
            var $ = layui.jquery,
                list = layui.dtable,
                ajax = layui.ajax,
                form = layui.jqform,
                modal = layui.modal,
                oneList = new list();
            form.set({
                "change": true,
                "form": "#form1"
            }).init(oneList);
            var n = 0, gridStr = '';


            var btn = "<td><button class=\"layui-btn layui-btn-mini \"\n" +
                " onclick=\"update('{ids}');\" >" +
                //                "                            data-list='{\"key\":\"id={ids}\",\"msg\":true,\"render\":\"true\",\"action\":\"del\"}'\n" +
                //                "                            data-params='{\"url\": \"/shop/admin/category/delete\",\"data\":\"ids={ids}\", \"method\":\"post\"}'>\n" +
                "                        <i class=\"iconfont\">&#xe653;</i>编辑\n" +
                "                    </button>" +
                "<button class=\"layui-btn layui-btn-mini layui-btn-danger\"\n" +
                " onclick=\"del('{ids}');\" >" +
                "                        <i class=\"iconfont\">&#xe626;</i>删除\n" +
                "                    </button></td>";

            var initTreeGrid = function (json, pid) {
                for (var i = 0; n++, i < json.length; i++) {

                    var obj = json[i];
//                obj.createTime = new Date(obj.createtime).format("yyyy-MM-dd hh:mm");

                    var str = '';
                    str = '<tr class="treegrid-' + n;

                    if (obj.parentId) {
                        str += ' treegrid-parent-' + pid;
                    }
                    str += '">';

                    $.each($("th", $('#treeGrid >thead')), function (i, n) {
                        var _name = $(n).attr("name");
                        var text = eval("obj." + _name);
                        if (text != null) {
                            if (i == -1) {//添加复选框
                                str += '<td><input type="checkbox" name="ids" class="row" value="' + obj.id + '"/>'
                                    + text + '</td>';
                            } else {
                                if (_name.toLowerCase().indexOf('time') > -1) {
                                    str += '<td>' + text + '</td>';
                                }

                                else if ($(n).attr("type") == 'icon') {
                                    var h = $(n).attr('height') || '120';
                                    var img='<td><a href="{img}" target="_blank"><img width="{h}px" height="{h}px"' +
                                        ' src="{img}"/></a></td>';
                                    str += img.replace(/{img}/g, '/shop'+text).replace(/{h}/g, h);
                                }

                                else {
                                    str += '<td name="{name}" ids="{ids}">' + text + '</td>';
                                    str = str.replace(/{name}/, _name).replace(/{ids}/, obj.id);
                                }
                            }

                        } else if ($(n).attr("type") == 'operate') {
//                        str += '<td><a style="cursor:pointer;" href="javascript:update(\'' + obj.ids + '\');">修改</a></td>';
                            str += btn.replace(/{ids}/g, obj.id);
                        }
                        else {
                            str += '<td></td>';
                        }
                    });

                    gridStr = gridStr + str + '</tr>\n';

                    // str += '<td>' + obj.text + '</td><td>'+obj.code+'</td></tr>' ;
                    //  gridStr += str;
                    //如果当前节点为父节点,则循环遍历
                    if (obj.children != null) {
                        arguments.callee(obj.children, n);
                    }
                }
            };

            restPost($url, null, function (rs) {
                initTreeGrid(rs.data, 1);
                $("#treeGrid > tbody").html(gridStr);
                gridStr = '', n = 0;//重置变量
                $('.tree').treegrid({
                    initialState: 'collapsed'  //'collapsed'expanded
                    /* expanderTemplate: '<span class="treegrid-expander glyphicon glyphicon-minus"></span>',
                     expanderExpandedClass: 'glyphicon glyphicon-minus',
                     expanderCollapsedClass: 'glyphicon glyphicon-plus'*/
                });

                modal.init();
            })

        });
    };

    return {
        initTableData: initTableData
    }
}();
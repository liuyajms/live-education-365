/*
 自定义覆盖物
 */
// 构造函数
function TextOverlay(center, text, clickHandler, param, width, length, color){
    this._point = center;
    this._text = text;
    this._height = length || 94;
    this._width = width || 94;
    this._color = color || "#39AC6A";
    this._clickHandler = clickHandler;
    this._param = param;
}
// 1.继承API的BMap.Overlay
TextOverlay.prototype = new BMap.Overlay();

// 2.实现初始化方法
TextOverlay.prototype.initialize = function(map){
    // 保存map对象实例
    this._map = map;
    var div = this._div = document.createElement("div");
    div.style.position = "absolute";
    div.style.zIndex = BMap.Overlay.getZIndex(this._point.lat);
    div.style.backgroundColor = this._color;
    div.style.borderRadius = '60px';
    div.style.height = this._height + "px";
    div.style.width = this._height + 'px';
    div.style.textAlign = 'center';
    var span = this._span = document.createElement("span");
    span.setAttribute('param', this._param);
    span.style.color = 'white';
    span.style.fontSize = '12px';
    span.style.width = '70px';
    span.style.left = '12px';
    span.style.top = '12px';
    span.style.position = 'absolute';
    span.innerHTML = this._text;
//    span.innerHTML = '<span style="color:white; font-size:12px;width: 60px;left: 12px;top: 12px; position: absolute">'+ this._text +'</span>';
    span.onclick = this._clickHandler;
    div.appendChild(span);

    var arrow = this._arrow = document.createElement("div");
    arrow.style.background = "url(http://map.baidu.com/fwmap/upload/r/map/fwmap/static/house/images/label.png) no-repeat";
    arrow.style.position = "absolute";
    arrow.style.width = "1px";
    arrow.style.height = "1px";
    arrow.style.top = "42px";
    arrow.style.left = "10px";
    arrow.style.overflow = "hidden";
    div.appendChild(arrow);

    // 将div添加到覆盖物容器中
    map.getPanes().markerPane.appendChild(div);
    // 保存div实例
    //this._div = div;
    // 需要将div元素作为方法的返回值，当调用该覆盖物的show、
    // hide方法，或者对覆盖物进行移除时，API都将操作此元素。
    return div;
};
// 3 实现绘制方法
TextOverlay.prototype.draw = function(){
    // 根据地理坐标转换为像素坐标，并设置给容器pointToOverlayPixel
    var map = this._map;
    var pixel = map.pointToOverlayPixel(this._point);
    this._div.style.left = pixel.x - parseInt(this._arrow.style.left) + "px";
    this._div.style.top  = (pixel.y - 75) + "px";
};

// 实现显示方法
TextOverlay.prototype.show = function(){
    if (this._div){
        this._div.style.display = "";
    }
};
// 实现隐藏方法
TextOverlay.prototype.hide = function(){
    if (this._div){
        this._div.style.display = "none";
    }
};

//======================================
/*
圆形区域
 */
function CycleOverlay(point){
    var m = new BMap.Marker(point, {
        icon: new BMap.Symbol(BMap_Symbol_SHAPE_CIRCLE, {
            scale: 55,
            strokeWeight : 1,
            strokeColor:"#39AC6A",
            fillColor: "#39AC6A",//填充颜色
            fillOpacity: 0.8//填充透明度
        })
    });
    m.setTitle('测试');
    return m;
}

//=====================================
/*
多边形区域
 */
function PolygonOverlay(pointArr){
    var options = {
        strokeColor:"#39AC6A",    //边线颜色。
        fillColor:"#39AC6A",      //填充颜色。当参数为空时，圆形将没有填充效果。
        strokeWeight: 2,       //边线的宽度，以像素为单位。
        strokeOpacity: 0.8,	   //边线透明度，取值范围0 - 1。
        fillOpacity: 0.3,      //填充的透明度，取值范围0 - 1。
        strokeStyle: 'solid' //边线的样式，solid或dashed。
    };

    return new BMap.Polygon(pointArr, options);  //创建多边形
}


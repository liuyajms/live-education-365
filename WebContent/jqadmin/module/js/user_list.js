/*
 * @Author: Paco
 * @Date:   2017-02-07
 * +----------------------------------------------------------------------
 * | jqadmin [ jq酷打造的一款懒人后台模板 ]
 * | Copyright (c) 2017 http://jqadmin.jqcool.net All rights reserved.
 * | Licensed ( http://jqadmin.jqcool.net/licenses/ )
 * | Author: Paco <admin@jqcool.net>
 * +----------------------------------------------------------------------
 */

layui.define(['jquery', 'dtable', 'jqdate', 'jqform', 'upload'], function(exports) {
    var $ = layui.jquery,
        list = layui.dtable,
        ajax = layui.ajax,
        form = layui.jqform,
        oneList = new list();

    if(typeof initData != 'undefined' && initData instanceof Function){
        initData(form);
    }

    form.set({
        "change": true,
        "form": "#form1"
    }).init(oneList);

//全选
/*
    var lform = layui.form();
    lform.on('checkbox(allChoose)', function(data){console.log( $(data.elem))
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
        child.each(function(index, item){
            item.checked = data.elem.checked;
        });
        lform.render('checkbox');
    });
*/

    form.abc = function(ret, options, that) {
        alert("这是form回调方法");
    };
    ajax.test = function(ret, options, that) {
        alert("这是ajax回调方法");
    };
    oneList.init('list-tpl');

    var box;
    //上传文件设置
    layui.upload({
        elem:'#img',
        url: '/shop/api/image/user',
        before: function(input) {
            box = $(input).parent('form').parent('div').parent('.layui-input-block');
            if (box.next('div').length > 0) {
                box.next('div').html('<div class="imgbox"><p>上传中...</p></div>');
            } else {
                box.after('<div class="layui-input-block"><div class="imgbox"><p>上传中...</p></div></div>');
            }
        },
        success: function(res) {console.log(res)
            if (res.code == 200) {
                box.next('div').find('div.imgbox').html('<img src="/shop' + res.data[0].url + '" alt="..." class="img-thumbnail">');
                box.find('input[type=hidden]').val(res.data[0].url);
                form.check(box.find('input[type=hidden]'));
            } else {
                box.next('div').find('p').html('上传失败...')
            }
        }
    });

    exports('user_list', {});
});
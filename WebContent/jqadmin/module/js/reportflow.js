layui.define(['jquery', 'dtable', 'jqdate', 'jqform', 'upload'], function(exports) {
    var $ = layui.jquery,
        list = layui.dtable,
        ajax = layui.ajax,
        laydate = layui.laydate,
        form = layui.jqform,
        oneList = new list();
    form.set({
        "change": true,
        "form": "#form1"
    }).init(oneList);

    var ids = getQueryString('ids');
    $('#list-tpl').attr('data-params',
            '{"url":"/shop/admin/reportflow","dataName":"reportflowData","method":"post","pageid":"#page","data":"reportids='+ ids +'"}');
    oneList.init('list-tpl');
    exports('reportflow', {});
});
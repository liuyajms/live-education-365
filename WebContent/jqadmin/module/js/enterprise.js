layui.define(['jquery', 'dtable', 'jqdate', 'jqform', 'upload'], function(exports) {
    var $ = layui.jquery,
        list = layui.dtable,
        ajax = layui.ajax,
        laydate = layui.laydate,
        form = layui.jqform,
        oneList = new list();

        /*form.set({
            "change": true,
            "form": "#form1"
        }).init(oneList);*/
    oneList.init('list-tpl');

    /*
     query department list
     */
/*    $.getJSON("/shop/api/department/child?ids=root", function(data){
        var optionstring = "";
        $.each(data.data, function(i,item){
            optionstring += "<option value=\"" + item.ids + "\" >" + item.names + "</option>";
        });
        $("select[name=areaid]").html('<option value=""></option>' + optionstring);
    });*/



    /*
    以下为上传文件部分
     */
    var box;
    var index;
    //上传文件设置
    layui.upload({
        elem:'input[name=logo]',
        url: '/shop/api/image/ep',
        before: function(input) {
            box = $(input).parent('form').parent('div').parent('.layui-input-block');
            if (box.next('div').length > 0) {
                box.next('div').html('<div class="imgbox"><p>上传中...</p></div>');
            } else {
                box.after('<div class="layui-input-block"><div class="imgbox"><p>上传中...</p></div></div>');
            }
        },
        success: function(res) {
            if (res.code == 200) {
                var str = '<span class="layer-photos-demo">' + '<img src="/shop' + res.data[0].url + '" alt="..." class="img-thumbnail">' + '</span>';
                box.next('div').find('div.imgbox').html(str);
                box.find('input[name=img]').val(res.data[0].url);
            } else {
                box.next('div').find('p').html('上传失败...')
            }
        }
    });

    layui.upload({
        elem:'input[name=licenseimg]',
        url: '/shop/api/image/ep-license',
        before: function(input) {
            box = $(input).parent('form').parent('div').parent('.layui-input-block');
            index = layer.load(1, {shade: [0.8, '#393D49'], time: 3000})
        },
        success: function(res) {
            if (res.code == 200) {
                addImg(res);
                layer.close(index)
            } else {
                box.next('div').find('p').html('上传失败...')
            }
        }
    });

    /*
    处理单张或多张的情况（多个以逗号分隔）
     */
    var addImg = function(res){
        var _url = res.data[0].url;
        var str =  '<img name="file" src="{src}" class="img-thumbnail" style="height:200px;">';
        str = str.replace(/{src}/g, '/shop'+_url);
        str = '<span class="layer-photos-demo">' + str + '</span>';
        str += '<a style="cursor: pointer;margin-right: 50px;" onclick="delImg(this)"><span class="layui-icon" style="color: red;font-size: xx-large;vertical-align:top;">&#x1006;</span></a>'

        var row = '';
        var len = box.parent().find('img').length;
        if(len % 3 == 0){
            row = '<div class="imgbox"><span>'+str+'</span></div>';
            box.after(row);
        } else {
            row = '<span>' + str + '</span>';
            box.next('.imgbox').append(row);
        }
        var t = $('input[type=hidden]', $(box)).val();
        $('input[type=hidden]', $(box)).val((t ? t + ',' + _url : _url));

        refreshImg();
    };


    exports('enterprise', {});
});
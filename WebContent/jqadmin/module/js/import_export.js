layui.define(['jquery','upload'],function(exports){
    var $ = layui.jquery;
    $('#importBtn').click(function(){
        $('#importFile').click();
    });
    $('#exportBtn').on('click', function(){
        var u = $(this).attr('data-url');
        location.href = u + (u.indexOf('?') > 0 ? '&' : '?') + decodeURIComponent($($('section form')[0]).serialize());
    });
    var box;
    //上传文件设置
    layui.upload({
        elem:'input[name=file]',
        ext: 'xls|xlsx',
        type: 'file',
        title:'导入',
        url: $('#importBtn').attr('data-url'),
        before: function(input) {
            box = $(input).parent('form').parent('div').parent('.layui-input-block');
            if (box.next('div').length > 0) {
                box.next('div').html('<div class="imgbox"><p>上传中...</p></div>');
            } else {
                box.after('<div class="layui-input-block"><div class="imgbox"><p>上传中...</p></div></div>');
            }
            layer.open({type:3})
        },
        success: function(res) {
            layer.closeAll('loading'); //关闭加载层
            if (res.status == 200) {
                box.next('div').html('');
                layer.msg('成功导入' + res.data + '条数据', function(){
                    location.reload();
                });
            } else {
                box.next('div').find('p').html('上传失败...')
                layer.open({content:res.msg})
            }
        }
    });
    exports('import_export');
});
var isFirst = 1;
var contextMenu = new AMap.ContextMenu();

var map = new AMap.Map('container', {
    zoom:11,//级别
    center: [104.065247,30.661541],//中心点坐标
    viewMode:'3D'//使用3D视图
});
map.plugin(["AMap.ToolBar"],function(){
    //加载工具条
    var tool = new AMap.ToolBar();
    map.addControl(tool);
});

var common_map = function(){
    "use strict";
    var currentMarker;

    var addSearchBar = function () {
        map.plugin('AMap.PlaceSearch',function(){//异步加载插件
            var toolbar = new AMap.PlaceSearch();

        });
    }

    var createMarker = function (text, loc, idx, color) {
        return createMarker2(text, loc, idx, color)
    }
    var createMarker2 = function (text, loc, idx, color) {
        // 点标记显示内容，HTML要素字符串
        var svg =
            '<svg t="1590244910680" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7218" width="30" height="40"> \
            <path d="M896 384c0 249.6-384 640-384 640S128 633.6 128 384s172.8-384 384-384S896 134.4 896 384z" \
            p-id="7219" fill-opacity="0.7" fill="{fill}"></path></svg>';
        svg = svg.replace(/{fill}/, color || 'blue');
        var markerContent= '<div style="display: flex; align-items:center; justify-content: center;">' +
            '<span style="position: absolute; top: 10px; color: white">'+ (text) +'</span>'+ svg +'</div>'

        var marker = new AMap.Marker({
            position: loc,
            // 将 html 传给 content
            content: markerContent,
            // 以 icon 的 [center bottom] 为原点
            offset: new AMap.Pixel(-14, -28)
        });
        return marker;
    }
    var onRightClick = function(marker, func) {
        marker.on('rightclick', function (e) {
            contextMenu.open(map, e.lnglat);
            func(marker)
            currentMarker = marker
        });
    }
    var addMarker = function (no, loc, color) {
        var marker = this.createMarker(no, loc, color)
        console.log(marker)
        map.add(marker);
    }
    var addMarkers = function(markers) {
        map.add(markers)
    }
    var setCenter = function() {
        // 第一个参数为空，表明用图上所有覆盖物 setFitview
        // 第二个参数为false, 非立即执行
        // 第三个参数设置上左下右的空白
        map.setFitView(null, false, [60, 60, 60, 60]);
    }
    var setMarkerDrag = function (marker, flag) {
        marker.setDraggable(flag || true)
    }
    var clearMap = function () {
        map.clearMap();
    }
    var clearMarker = function (marker) {
        map.remove(marker || currentMarker)
    }
    var addMenu = function (text, callback) {
        contextMenu.addItem(text,  (e)=> {
            callback()
        }, 0);
    }

    var getPixLngLat = function (px, py) {
        var pixel = new AMap.Pixel(px, py);
        var lnglat = map.containerToLngLat(pixel);
        return lnglat;
    }

    var locate = function (pos) {
        if(currentMarker) clearMarker(currentMarker)
        var markerContent = '' +
            '<div class="custom-content-marker">' +
            '   <img src="//a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-red.png">' +
            '   <div class="close-btn" onclick="common_map.clearMarker()">X</div>' +
            '</div>';

        currentMarker = new AMap.Marker({
            position: pos,
            // 将 html 传给 content
            content: markerContent,
            // 以 icon 的 [center bottom] 为原点
            offset: new AMap.Pixel(-13, -30)
        });

        // 将 markers 添加到地图
        map.add(currentMarker);
        map.setCenter(pos)
    }

    var offClick = function (func) {
        map.off('click', func)
    }

    var onClick = function (func) {
        map.on('click', func)
    }
    var onMoveend = function (func) {
        map.on('moveend', func)
    }
    var onZoomend = function (func) {
        map.on('zoomend', func)
    }
    var onDragend = function (func) {
        map.on('dragend', func)
    }

    return {
        createMarker, createMarker2, addMarker, addMarkers, clearMarker, clearMap,
        setCenter, setMarkerDrag, addMenu, getPixLngLat,
        onRightClick, offClick, onClick, onMoveend, onZoomend, onDragend,
        addSearchBar, locate
    }
}();